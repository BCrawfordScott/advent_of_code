defmodule EAC18.InputsTest do
  use ExUnit.Case, async: true

  setup do
    {:ok, bucket} = EAC18.Inputs.start_link([])
    %{bucket: bucket}
  end

  test "stores values by key", %{bucket: bucket} do
    assert EAC18.Inputs.get(bucket, "milk") == nil

    EAC18.Inputs.put(bucket, "milk", 3)
    assert EAC18.Inputs.get(bucket, "milk") == 3
  end

  test "deletes values by key", %{bucket: bucket} do
    EAC18.Inputs.put(bucket, "milk", 3)
    EAC18.Inputs.delete(bucket, 3)

    assert EAC18.Inputs.get(bucket, 3) == nil
  end
end
