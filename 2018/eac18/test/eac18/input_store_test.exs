defmodule EAC18.InputStoreTest do
  use ExUnit.Case, async: true

  setup do
    registry = start_supervised!(EAC18.InputStore)
    %{registry: registry}
  end

  test "spawns buckets", %{registry: registry} do
    assert EAC18.InputStore.lookup(registry, "shopping") == :error

    EAC18.InputStore.create(registry, "shopping")
    assert {:ok, bucket} = EAC18.InputStore.lookup(registry, "shopping")

    EAC18.Inputs.put(bucket, "milk", 1)
    assert EAC18.Inputs.get(bucket, "milk") == 1
  end

  test "removes buckets on exit", %{registry: registry} do
    EAC18.InputStore.create(registry, "shopping")
    {:ok, bucket} = EAC18.InputStore.lookup(registry, "shopping")
    Agent.stop(bucket)
    assert EAC18.InputStore.lookup(registry, "shopping") == :error
  end
end
