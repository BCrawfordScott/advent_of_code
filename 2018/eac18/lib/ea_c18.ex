defmodule EAC18 do
  use Application
  @moduledoc """
  Documentation for `EAC18`.
  """

  def start(_type, _args) do
    EAC18.Supervisor.start_link(name: EAC18.Supervisor)
  end
end
