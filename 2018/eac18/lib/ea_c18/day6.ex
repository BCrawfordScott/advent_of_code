defmodule EAC18.Day6 do

  def run do
    IO.puts("Part 1 answer: #{process1()}")
    IO.puts("Part 2 answer: #{process2()}")
  end

  def get_input do
    string_mod = __MODULE__ |> Atom.to_string()
    [_string | [file_name | _tail ]] = Regex.run(~r/.*\.(D\w*)/, string_mod)

    EAC18.Inputs.format_input(String.downcase(file_name))
  end

  def process1 do
    "Not Implemented"
  end

  def process2 do
    "Not Implemented"
  end
end
