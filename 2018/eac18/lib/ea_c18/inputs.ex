defmodule EAC18.Inputs do
  use Agent

  @doc """
    Start a new inputs map
  """
  def start_link(_opts) do
    Agent.start_link(fn -> %{} end)
  end

  @doc """
    Get a value at the given key from the inputs map
  """
  def get(bucket, key) do
    Agent.get(bucket, &Map.get(&1, key))
  end

  @doc """
    Put a value into the inputs map
  """
  def put(bucket, key, value) do
    Agent.update(bucket, &Map.put(&1, key, value))
  end

  @doc """
    Removes an item from the inputs map
  """
  def  delete(bucket, key) do
    Agent.get_and_update(bucket, &Map.pop(&1, key))
  end

  def build_file_path(file_name) do
    "./inputs/" <> file_name <> ".txt"
  end

  def read(path) do
    {:ok, input} = File.read(path)
    input
  end

  def to_list(content) do
    content
    |> String.replace("\r", "")
    |> String.split("\n", trim: true)
  end

  def to_ints(string_list) do
    string_list
    |> Enum.map(&(String.to_integer(&1)))
  end

  def format_input(name) do
    case name do
      "day1" ->
        name |> build_file_path() |> read() |> to_list() |> to_ints()
      "day2" ->
        name |> build_file_path() |> read() |> to_list()
      "day3" ->
        name |> build_file_path() |> read() |> to_list()
      "day4" ->
        name |> build_file_path() |> read() |> to_list()
      "day5" ->
        name |> build_file_path() |> read() |> String.replace(["\r","\n"], "")
      "default" ->
        name |> build_file_path() |> read() |> to_list()
      _ ->
        {:error, "Cannot process that input"}
    end
  end
end
