defmodule EAC18.Day2 do
  def run do
    IO.puts("Part 1 answer: #{process1()}")
    IO.puts("Part 2 answer: #{process2()}")
  end

  def get_input do
    EAC18.Inputs.format_input("day2")
  end

  def process1 do
    get_input()
    |> Enum.map(&analyze_id(&1))
    |> Enum.reduce({0, 0}, &sum_counts(&1, &2))
    |> checksum()
  end

  def process2 do
    get_input()
    |> find_match()
    |> Enum.join("")
  end

  def analyze_id(box_id) do
    split_id = box_id |> String.split("", trim: true)

    split_id
    |> MapSet.new()
    |> MapSet.to_list()
    |> Enum.reduce(%{}, &count_letters(&1, &2, split_id))
  end

  def count_letters(letter, letter_counts, split_id) do
    Map.put(
      letter_counts,
      Enum.count(split_id, fn x -> x == letter end) |> Integer.to_string(),
      letter
    )
  end

  def sum_counts(letter_counts, {y1, y2}) do
    x1 = if Map.has_key?(letter_counts, "2"), do: 1, else: 0
    x2 = if Map.has_key?(letter_counts, "3"), do: 1, else: 0

    {x1 + y1, x2 + y2}
  end

  def find_match([]), do: {:error, "Something messed up, bro"}
  def find_match([head | tail]) do
    case check_diff(String.split(head, "", trim: true), tail) do
      :no_match -> find_match(tail)
      val -> val
    end
  end

  def check_diff(_box_id, []), do: :no_match
  def check_diff(box_id, [head | tail]) do
    checking = String.split(head, "", trim: true)
    case diff_count(box_id, checking) do
      1 -> remove_diff(box_id, checking)
      _ -> check_diff(box_id, tail)
    end
  end

  def diff_count(list1, list2), do: diff_count(list1, list2, 0)
  def diff_count([], _list2, count), do: count
  def diff_count(_list1, [], count), do: count
  def diff_count([head1 | tail1], [head2 | tail2], count) do
    cond do
      head1 == head2 -> diff_count(tail1, tail2, count)
      true -> diff_count(tail1, tail2, count + 1)
    end
  end

  def remove_diff([head1 | tail1], [head2 | tail2]) do
    cond do
      head1 != head2 -> tail1
      true -> [head1 | remove_diff(tail1, tail2)]
    end
  end

  def checksum({x, y}) do
    x * y
  end
end
