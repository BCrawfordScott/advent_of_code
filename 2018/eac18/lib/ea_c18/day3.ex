defmodule EAC18.Day3 do
  @input_capture ~r/#(\d*) @ (\d*)\,(\d*): (\d*)x(\d*)/

  def run do
    IO.puts("Part 1 answer: #{process1()}")
    IO.puts("Part 2 answer: #{process2()}")
  end

  def get_input do
    EAC18.Inputs.format_input("day3")
  end

  def process1 do
    get_input()
    |> Enum.map(&parse_inputs(&1))
    |> Enum.map(&to_ranges(&1))
    |> Enum.map(&build_coordinate_list(&1))
    |> Enum.reduce({%MapSet{}, %MapSet{}}, fn coord_list, {all_positions, doubled_positions} ->
      current_set = MapSet.new(coord_list)
      overlap = MapSet.intersection(current_set, all_positions)
      {MapSet.union(all_positions, current_set), MapSet.union(doubled_positions, overlap)}
    end)
    |> (fn {_all_positions, doubled_positions} -> MapSet.size(doubled_positions) end).()
  end

  def process2 do
    parsed_inputs = get_input()
    |> Enum.map(&parse_inputs(&1))

    doubled_positions = parsed_inputs
    |> Enum.map(&to_ranges(&1))
    |> Enum.map(&build_coordinate_list(&1))
    |> Enum.reduce({%MapSet{}, %MapSet{}}, fn coord_list, {all_positions, doubled_positions} ->
      current_set = MapSet.new(coord_list)
      overlap = MapSet.intersection(current_set, all_positions)
      {MapSet.union(all_positions, current_set), MapSet.union(doubled_positions, overlap)}
    end)
    |> (fn {_all_positions, doubled_positions} -> doubled_positions end).()

    parsed_inputs
    |> Enum.find(fn input ->
      coord_list = input
      |> to_ranges()
      |> build_coordinate_list()
      |> MapSet.new()

      overlap = MapSet.intersection(coord_list, doubled_positions)
      MapSet.size(overlap) == 0
    end)
    |> (fn {id, _, _, _, _} -> id end).()
  end

  def parse_inputs(input) do
    [_input_string | [id | [start_x | [start_y | [width | [height | _tail]]]]]] = Regex.run(@input_capture, input)

    {
      String.to_integer(id),
      String.to_integer(start_x),
      String.to_integer(start_y),
      String.to_integer(width),
      String.to_integer(height)
    }
  end

  def to_ranges({_id, start_x, start_y, width, height }) do
    x_range = (start_x + 1)..(start_x + width)
    y_range = (start_y + 1)..(start_y + height)
    {x_range, y_range}
  end

  def coordinate_range(x, y_coords) do
    Enum.map(y_coords, fn y -> {x, y} end)
  end

  def build_coordinate_list({x_coords, y_coords}) do
    x_coords
    |> Enum.reduce([], fn x, acc -> acc ++ coordinate_range(x, y_coords) end)
  end
end
