defmodule EAC18.Day4 do
  @log_regex ~r/\[(\d*\-\d*\-\d*) (\d{2}:\d{2})\] (Guard|falls|wakes) (#\d*|\w*)/

  def run do
    IO.puts("Part 1 answer: #{process1()}")
    IO.puts("Part 2 answer: #{process2()}")
  end

  def get_input do
    string_mod = __MODULE__ |> Atom.to_string()
    [_string | [file_name | _tail ]] = Regex.run(~r/.*\.(D\w*)/, string_mod)

    EAC18.Inputs.format_input(String.downcase(file_name))
  end

  def process1 do
    guard_sleep_times = parse_input()
    |> Enum.sort_by(&capture_date_time/1, DateTime)
    |> build_guard_sleep_times()

    guard = longest_sleeper(guard_sleep_times)
    minute = sleepiest_minute(Map.get(guard_sleep_times, guard))

    guard * minute
  end

  def process2 do
    guard_sleep_times = parse_input()
    |> Enum.sort_by(&capture_date_time/1, DateTime)
    |> build_guard_sleep_times()

    {sleeper, {minutes, _}} = most_consistent_sleeper(guard_sleep_times)

    sleeper * minutes
  end

  def parse_input() do
    get_input()
    |> Enum.map(fn log ->
      [_match | [date | [time | [action | [id_or_description | _tail]]]]] = parse_log(log)

      { date, time, action, parse_id(id_or_description) }
    end)
  end

  def parse_log(log) do
    Regex.run(@log_regex, log)
  end

  def parse_id(id_or_description) do
    if String.match?(id_or_description, ~r/#\d*/) do
      String.replace(id_or_description, "#", "") |> String.to_integer()
    else
      nil
    end
  end

  def capture_date_time({string_date, string_time, _action, _id}) do
    {:ok, date_time} = DateTime.new(capture_date(string_date), capture_time(string_time))
    date_time
  end

  def capture_date(string_date) do
    date_values = String.split(string_date, "-", trim: true) |> Enum.map(&String.to_integer/1)
    {:ok, date} = apply(Date, :new, date_values)
    date
  end

  def capture_time(string_time) do
    time_values = String.split(string_time, ":", trim: true) |> Enum.map(&String.to_integer/1)
    {:ok, time} = apply(Time, :new, time_values ++ [0])
    time
  end

  def capture_log({_date, _time, "Guard", id}, {guard_logs, _last_guard_info}) do
    {guard_logs, {id, nil}}
  end
  def capture_log({_date, time, "falls", _id}, {guard_logs, {current_guard, _last_sleep_time}}) do
    {guard_logs, {current_guard, time}}
  end
  def capture_log({_date, wake_time, "wakes", _id}, {guard_logs, {current_guard, last_sleep_time}}) do
    current_guard_log = Map.get(guard_logs, current_guard, %{})
    minute_range = build_sleep_minute_range(last_sleep_time, wake_time)
    new_guard_log = Enum.reduce(minute_range, current_guard_log, &record_sleep_minute/2)

    {Map.merge(guard_logs, %{current_guard => new_guard_log}), {current_guard, nil}}
  end

  def build_sleep_minute_range(sleep_time, wake_time) do
    sleep = capture_time(sleep_time).minute
    wake = capture_time(wake_time).minute
    sleep..wake-1
  end

  def record_sleep_minute(minute, log) do
    {_, new_log} = Map.get_and_update(log, minute, fn
      nil -> {nil, 1}
      prev -> {prev, prev + 1}
    end)

    new_log
  end

  def build_guard_sleep_times(log) do
    {sleep_log, _} = Enum.reduce(log, {%{}, {nil, nil}}, &capture_log/2)
    sleep_log
  end

  def longest_sleeper(sleep_log) do
    {guard, _time_sleeping} = Map.keys(sleep_log)
    |> Enum.reduce({nil, 0}, fn
      guard_id, {nil, _ } -> {guard_id, calc_sleep(Map.get(sleep_log, guard_id))}
      guard_id, {longest_sleeping_guard, sleep_time}
        -> current_sleep_time = calc_sleep(Map.get(sleep_log, guard_id)); if current_sleep_time > sleep_time, do: {guard_id, current_sleep_time}, else: {longest_sleeping_guard, sleep_time }
    end)

    guard
  end

  def calc_sleep(sleep_times) do
    Map.keys(sleep_times)
    |> Enum.reduce(0, fn minute, total -> Map.get(sleep_times, minute) + total end)
  end

  def sleepiest_minute(sleep_times) do
    Map.keys(sleep_times)
    |> Enum.reduce(nil, fn
      minute, nil -> minute
      minute, longest -> if Map.get(sleep_times, minute) > Map.get(sleep_times, longest), do: minute, else: longest
    end)
  end

  def most_consistent_sleeper(guard_sleep_logs) do
    Map.keys(guard_sleep_logs)
    |> Enum.reduce({nil, {nil, 0}}, fn id, {running_guard, {min, times}} ->
      current_guard_log = Map.get(guard_sleep_logs, id)
      current_guard_minute = sleepiest_minute(current_guard_log)
      current_times_sleeping = Map.get(current_guard_log, current_guard_minute)

      if current_times_sleeping > times do
        {id, {current_guard_minute, current_times_sleeping}}
      else
        {running_guard, {min, times}}
      end
    end)
  end
end
