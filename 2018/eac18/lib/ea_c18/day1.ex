defmodule EAC18.Day1 do
  def run do
    IO.puts("Part 1 answer: #{process1()}")
    IO.puts("Part 2 answer: #{process2()}")
  end

  def get_input do
    EAC18.Inputs.format_input("day1")
  end

  def process1 do
    get_input()
    |> Enum.reduce(&add(&1, &2))
  end

  def process2 do
    get_input()
    |> continuous_process()
  end

  def continuous_process(input), do: continuous_process(input, {0, MapSet.new([])})
  def continuous_process(input, accumulator) do
    case Enum.reduce_while(input, accumulator, &memo_add(&1, &2)) do
      {sum, set} -> continuous_process(input, {sum, set})
      val -> val
    end
  end

  def add(x, y) do
    x + y
  end

  def memo_add(x, {sum, set}) do
    new_sum = add(x, sum)
    if MapSet.member?(set, new_sum) do
      {:halt, new_sum}
    else
      {:cont, {new_sum, MapSet.put(set, new_sum)}}
    end
  end
end
