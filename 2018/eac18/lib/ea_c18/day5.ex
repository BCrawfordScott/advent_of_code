defmodule EAC18.Day5 do
  @types 'abcdefghijklmnopqrstuvwxyz'

  def run do
    IO.puts("Part 1 answer: #{process1()}")
    IO.puts("Part 2 answer: #{process2()}")
  end

  def get_input do
    string_mod = __MODULE__ |> Atom.to_string()
    [_string | [file_name | _tail ]] = Regex.run(~r/.*\.(D\w*)/, string_mod)

    EAC18.Inputs.format_input(String.downcase(file_name))
  end

  def process1 do
    get_input()
    |> String.to_charlist()
    |> start_reaction()
    |> length()
  end

  def process2 do
    # "dabAcCaCBAcCcaDA"
    get_input()
    |> String.to_charlist()
    |> find_shortest_length()
  end

  def react(polymer) when is_list(polymer), do: react({:finished, polymer})
  def react({rerun, []}), do: {rerun, []}
  def react({rerun, [char1 | []]}), do: {rerun, [char1]}
  def react({_rerun, [char1 | [char2 | []]]}) when abs(char1 - char2) == 32, do: {:rerun, []}
  def react({rerun, [char1 | [char2 | []]]}), do: {rerun, [char1, char2]}
  def react({_rerun, [char1 | [char2 | [char3 | tail]]]}) when abs(char2 - char3) == 32, do: react({:rerun, [char1 | tail]})
  def react({_rerun, [char1 | [char2 | [char3 | tail]]]}) when abs(char1 - char2) == 32, do: react({:rerun, [char3 | tail]})
  def react({rerun, [char1 | [char2 | [char3 | tail]]]}) do
    {should_rerun, polymer} = react({rerun, [char2 | [char3 | tail]]})
    {should_rerun, [char1 | polymer]}
  end

  def start_reaction(polymer) do
    case react(polymer) do
      {:rerun, new_polymer} -> start_reaction(new_polymer)
      {:finished, new_polymer} -> new_polymer
    end
  end

  def remove_type(polymer, char) do
    polymer
    |> Enum.reject(fn p -> p == char || p == char - 32 end)
  end

  def length_wihout_type(polymer, type) do
    polymer
    |> remove_type(type)
    |> start_reaction()
    |> length
  end

  def find_shortest_length(polymer) do
    @types
    |> Enum.reduce(nil, fn
      type, nil -> length_wihout_type(polymer, type)
      type, shortest ->
        current_length = length_wihout_type(polymer, type)
        if current_length < shortest, do: current_length, else: shortest
    end)
  end
end
