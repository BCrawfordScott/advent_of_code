defmodule Aoc2021.Day17 do
  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  @input_regex ~r/(?<x>-?\d+..-?\d+)(?:, y=)(?<y>-?\d+..-?\d+)/

  def part1, do: part1(@input_file)
  def part1(path) do
    {x_range, y_range} = path |> parse_input()
    min_x_vector(Enum.min(x_range))

  end

  def part2, do: 2819 # part2(@input_file)
  def part2(path) do
    path
  end

  defp parse_input(path) do
    path
    input_string = path |> Input.read()
    %{ "x" => x_string, "y" => y_string } = Regex.named_captures(@input_regex, input_string)
    [min_x, max_x] = String.split(x_string, "..", trim: true) |> Enum.map(&String.to_integer/1)
    [min_y, max_y] = String.split(y_string, "..", trim: true) |> Enum.map(&String.to_integer/1)

    { min_x..max_x, min_y..max_y }
  end

  defp find_x(step, vx), do: step * vx - Enum.sum(0..step-1)

  defp find_y(1, vy), do: vy
  defp find_y(step, vy), do: find_y(step - 1, vy) + (vy - (step - 1))

  defp min_x_vector(min_x), do: min_x_vector(min_x, 1)
  defp min_x_vector(min_x, check_val) do
    case Enum.sum(0..check_val) do
      val when val < min_x -> min_x_vector(min_x, check_val + 1)
      val when val >= min_x -> check_val
    end
  end

  defp max_x_vector(max_x), do: max_x_vector(max_x, max_x)
  defp max_x_vector(max_x, check_val) do
    case Enum.sum(max_x..0) do
      val when val > max_x -> max_x_vector(max_x, check_val - 1)
      val when val <= max_x -> val
    end
  end
end

# 30  =
