Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Fuel do
    def sum_needed(masses) do
        masses
        |> mass_to_fuel
        |> Enum.reduce(&(&1 + &2))
    end

    def sum_total(masses) do
        masses
        |> mass_to_total_fuel
        |> Enum.reduce(&(&1 + &2))
    end

    def calc_fuel(mass) do
        div(mass, 3) - 2
    end

    def calc_all_fuel(mass) when mass <= 0, do: 0;
    def calc_all_fuel(mass) do
        new_mass = div(mass, 3) - 2

        new_mass + calc_all_fuel(new_mass)
    end

    def mass_to_fuel(masses) do
        masses
        |> Enum.map(&(calc_fuel(&1)))
    end

    def mass_to_total_fuel(masses) do
        masses
        |> Enum.map(&(calc_all_fuel(&1)))
    end
end

input_path = Path.dirname(__ENV__.file) <> "/input.txt"

input_path
|> Input.to_int_array
|> Fuel.sum_needed
|> IO.puts

input_path
|> Input.to_int_array
|> Fuel.sum_total
|> IO.puts
