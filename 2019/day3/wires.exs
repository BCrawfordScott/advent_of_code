Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Wires do
    def closest_cross(input_path) do
        Input.to_array(input_path)
        # calculate the positions and steps for both paths
        |> Enum.map(fn path -> calc_path(String.split(path, ",", trim: true)) end)
        |> cross_points
        # |> IO.inspect
        # compare the two lists and find matches
        |> sort_points
        # find the lowest match
        |> List.first
        |> IO.inspect
    end

    def calc_path(steps, pos \\ {1, 1}, idx \\ 1, path \\ %{})
    def calc_path([], _pos, _idx, path), do: path
    def calc_path([head | tail], pos, idx, path) do
        step = next_step(pos, head)
        new_path = Map.put(path, step, idx)

        calc_path(tail, step, idx + 1, new_path)
    end

    def next_step(pos, step) do
        {x, y} = pos
        case parse_step(step) do
            {"R", val} -> {x + val, y}
            {"L", val} -> {x - val, y}
            {"D", val} -> {x, y - val}
            {"U", val} -> {x, y + val}
        end
    end

    def parse_step(step) do
        {d, s} = String.split_at(step, 1)
        {d, String.to_integer(s)}
    end

    def cross_points([map1, map2]) do
        IO.inspect(map1)
        IO.inspect(map2)
        Map.keys(map1)
        |> Enum.reject(fn pos -> 
            # IO.inspect(Map.get(map2, pos))
            Map.get(map2, pos) == nil 
        end)
    end

    def sort_points(points) do
        Enum.sort(points, fn p1, p2 -> 
            manhattan(p1) < manhattan(p2)
        end)
    end

    def manhattan(pos, start \\ {1, 1})
    def manhattan(pos, start) do
        {x1, y1} = pos
        {x2, y2} = start

        abs(x1 - x2) + abs(y1 - y2)
    end
end

test_path = Path.dirname(__ENV__.file) <> "/test.txt"
Wires.closest_cross(test_path)

# input_path = Path.dirname(__ENV__.file) <> "/input.txt"

# Wires.closest_cross(input_path)