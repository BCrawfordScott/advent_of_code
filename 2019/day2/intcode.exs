Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Intcode do

    def prep_memory(memory, noun \\ 12, verb \\ 2)
    def prep_memory(memory, noun, verb) do 
        memory
        |> List.replace_at(1, noun)
        |> List.replace_at(2, verb)
    end

    def run(memory), do: run(memory, 0)
    def run(memory, pointer) do
        [instruction, p1, p2, p3] =  Enum.slice(memory, pointer..pointer + 3)
        v1 = Enum.at(memory, p1)
        v2 = Enum.at(memory, p2)

        case process(instruction, v1, v2) do
            {:halt, _}-> List.first(memory)
            {val, mv} -> run(List.replace_at(memory, p3, val), pointer + mv)
        end
    end

    def process(instruction, v1, v2) do
        case instruction do
            1 -> {v1 + v2, 4}
            2 -> {v1 * v2, 4}
            99 -> {:halt, 1}
        end
    end

    def find(memory, val) do
        for noun <- 0..99 do
            for verb <- 0..99 do
                test = run(prep_memory(memory, noun, verb))
                if test == val, do: IO.puts(100 * noun + verb)
            end
        end
    end
end

input_path1 = Path.dirname(__ENV__.file) <> "/input1.txt"
input_path2 = Path.dirname(__ENV__.file) <> "/input2.txt"

input_path2
|> Input.intcode_array
|> Intcode.prep_memory(12, 2)
|> Intcode.run
|> IO.inspect

input_path2
|> Input.intcode_array
# |> Intcode.prep_memory(32, 5)
|> Intcode.find(19690720)
