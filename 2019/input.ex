defmodule Input do
    def read(filename) do
        {:ok, input} = File.read(filename)
        input
    end

    def to_array(filename) do
        filename
        |> read
        |> String.replace("\r", "")
        |> String.split("\n", trim: true)
    end

    def to_array(filename, ",") do
        filename
        |> read
        |> String.replace("\r", "")
        |> String.split(",", trim: true)
    end

    def to_int_array(filename) do
        filename
        |> to_array
        |> Enum.map(&(String.to_integer(&1)))
    end

    def intcode_array(filename) do
        filename
        |> to_array(",")
        |> Enum.map(&(String.to_integer(&1)))
    end
end