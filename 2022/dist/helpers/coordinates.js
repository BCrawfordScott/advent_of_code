"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.processCoor = exports.parseCoor = void 0;
const parseCoor = (coor) => coor.split(',').map(c => parseInt(c));
exports.parseCoor = parseCoor;
const processCoor = (coorArr) => coorArr.join(',');
exports.processCoor = processCoor;
