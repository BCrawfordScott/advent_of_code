"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.max = exports.min = void 0;
const min = (array) => array.reduce((least, current) => current < least ? current : least);
exports.min = min;
const max = (array) => array.reduce((least, current) => current > least ? current : least);
exports.max = max;
