"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const toRanges = (stringRange) => {
    const [sRange1, sRange2] = stringRange.split(',');
    return [
        sRange1.split('-').map((int) => parseInt(int)),
        sRange2.split('-').map((int) => parseInt(int)),
    ];
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day4.txt', '\n');
    const ranges = input.map(toRanges);
    const overlapCount = ranges.reduce((total, currentRanges) => {
        const [range1, range2] = currentRanges;
        const [r1start, r1end] = range1;
        const [r2start, r2end] = range2;
        if (r1start <= r2start && r1end >= r2end)
            return total + 1;
        if (r1start >= r2start && r1end <= r2end)
            return total + 1;
        return total;
    }, 0);
    return overlapCount;
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day4.txt', '\n');
    const ranges = input.map(toRanges);
    const overlapCount = ranges.reduce((total, currentRanges) => {
        const [range1, range2] = currentRanges;
        const [r1start, r1end] = range1;
        const [r2start, r2end] = range2;
        if (r1end < r2start || r2end < r1start)
            return total;
        return total + 1;
    }, 0);
    return overlapCount;
});
exports.part2 = part2;
