"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const SAND_DELTAS = [
    [0, 1],
    [-1, 1],
    [1, 1],
];
const buildRockPositions = (input) => {
    const positions = new Set();
    let maxY = -Infinity;
    input.forEach((instruction) => {
        const points = instruction.split(' -> ');
        let last = null;
        points.forEach((point) => {
            if (last === null) {
                last = point;
            }
            else {
                const [cx, cy] = last.split(',').map((str) => parseInt(str));
                const [nx, ny] = point.split(',').map((str) => parseInt(str));
                if (cy > maxY)
                    maxY = cy;
                if (ny > maxY)
                    maxY = ny;
                if (cx === nx) {
                    const [s, e] = [cy, ny].sort((a, b) => a - b);
                    for (let y = s; y <= e; y++) {
                        positions.add(`${cx},${y}`);
                    }
                }
                if (cy === ny) {
                    const [s, e] = [cx, nx].sort();
                    for (let x = s; x <= e; x++) {
                        positions.add(`${x},${cy}`);
                    }
                }
                last = point;
            }
        });
    });
    return [positions, maxY];
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day14.txt', '\n');
    const [rockPositions, maxY] = buildRockPositions(input);
    const sandPositions = new Set();
    let full = false;
    while (!full) {
        let sand = [500, 0];
        while (!sandPositions.has(sand.join(','))) {
            const [sx, sy] = sand;
            if (sy > maxY) {
                sandPositions.add(sand.join(','));
                full = true;
            }
            else {
                const cannotMove = SAND_DELTAS.map((delta) => {
                    const [dx, dy] = delta;
                    const nx = sx + dx;
                    const ny = sy + dy;
                    return rockPositions.has(`${nx},${ny}`) || sandPositions.has(`${nx},${ny}`);
                });
                if (cannotMove.every((check) => check)) {
                    sandPositions.add(sand.join(','));
                }
                else {
                    const delta = SAND_DELTAS[(cannotMove.findIndex((check) => !check))];
                    const [dx, dy] = delta;
                    const nx = sx + dx;
                    const ny = sy + dy;
                    sand = [nx, ny];
                }
            }
        }
    }
    return sandPositions.size - 1;
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day14.txt', '\n');
    const [rockPositions, maxY] = buildRockPositions(input);
    const sandPositions = new Set();
    while (!sandPositions.has('500,0')) {
        let sand = [500, 0];
        while (!sandPositions.has(sand.join(','))) {
            const [sx, sy] = sand;
            if (sy === maxY + 1) {
                sandPositions.add(sand.join(','));
            }
            else {
                const cannotMove = SAND_DELTAS.map((delta) => {
                    const [dx, dy] = delta;
                    const nx = sx + dx;
                    const ny = sy + dy;
                    return rockPositions.has(`${nx},${ny}`) || sandPositions.has(`${nx},${ny}`);
                });
                if (cannotMove.every((check) => check)) {
                    sandPositions.add(sand.join(','));
                }
                else {
                    const delta = SAND_DELTAS[(cannotMove.findIndex((check) => !check))];
                    const [dx, dy] = delta;
                    const nx = sx + dx;
                    const ny = sy + dy;
                    sand = [nx, ny];
                }
            }
        }
    }
    return sandPositions.size;
});
exports.part2 = part2;
