"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
class Node {
    constructor(pos, next, id) {
        this.pos = pos;
        this.next = next;
        this.id = id;
    }
    stringPos() {
        return this.pos.join(',');
    }
    posDiff(checkNode) {
        const [px, py] = this.pos;
        const [cx, cy] = checkNode.pos;
        return [px - cx, py - cy];
    }
    ;
}
const print = (headNode, maximum) => {
    const positions = new Map();
    let current = headNode;
    while (current) {
        positions.set(current.pos, current.id);
        current = current.next;
    }
    const rows = [];
    for (let index = 0; index < maximum * 2; index++) {
        const periods = '.'.repeat(maximum * 2).split('');
        rows.push(periods);
    }
    positions.forEach((value, key) => {
        const [x, y] = key;
        if (rows[x][y] === '.')
            rows[x][y] = value.toString();
    });
    if (rows[maximum][maximum] === '.')
        rows[maximum][maximum] = 's';
    console.log(rows.map((row) => row.join(' ')).join('\n'));
    console.log('\n');
};
const DELTA_MAP = {
    U: [-1, 0],
    D: [1, 0],
    L: [0, -1],
    R: [0, 1],
};
const ADJACENT_DELTAS = [
    [1, 0],
    [-1, 0],
    [0, -1],
    [0, 1],
    [-1, -1],
    [1, -1],
    [-1, 1],
    [1, 1],
];
const COMMAND_REGEX = /([UDLR]) (\d+)/;
const isAdjacent = (head, tail) => {
    const [hx, hy] = head;
    const [tx, ty] = tail;
    if (hx === tx && hy === ty)
        return true;
    return ADJACENT_DELTAS.some((delta) => {
        const [dx, dy] = delta;
        const testX = dx + hx;
        const testY = dy + hy;
        return (tx === testX && ty === testY);
    });
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day9.txt', '\n');
    let head = [0, 0];
    let tail = [0, 0];
    const tailVisited = new Set();
    tailVisited.add(tail.join(','));
    input.forEach((command) => {
        const match = command.match(COMMAND_REGEX);
        if (!match)
            return;
        const [_, direction, steps] = match;
        const [dx, dy] = DELTA_MAP[direction];
        for (let step = 0; step < parseInt(steps); step++) {
            const prevHead = head;
            head = [head[0] + dx, head[1] + dy];
            const adjacent = isAdjacent(head, tail);
            if (!adjacent) {
                tail = prevHead;
                tailVisited.add(tail.join(','));
            }
        }
    });
    return tailVisited.size;
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day9.txt', '\n');
    const maximum = (0, helpers_1.max)(input.map((command) => parseInt(command.split(' ')[1])));
    const head = new Node([maximum, maximum], null, 'H');
    const tail = new Node([maximum, maximum], null, 9);
    let current = head;
    for (let num = 0; num < 8; num++) {
        const nextNode = new Node([maximum, maximum], null, num + 1);
        current.next = nextNode;
        current = nextNode;
    }
    current.next = tail;
    const tailVisited = new Set();
    tailVisited.add(tail.stringPos());
    input.forEach((command) => {
        const match = command.match(COMMAND_REGEX);
        if (!match)
            return;
        const [_, direction, steps] = match;
        const [dx, dy] = DELTA_MAP[direction];
        for (let step = 0; step < parseInt(steps); step++) {
            head.pos = [head.pos[0] + dx, head.pos[1] + dy];
            let current = head;
            while (current.next) {
                const node1 = current;
                const node2 = current.next;
                const adjacent = isAdjacent(node1.pos, node2.pos);
                if (!adjacent) {
                    let [sx, sy] = node1.posDiff(node2);
                    if (sx > 1 || sx < -1)
                        sx = sx / 2;
                    if (sy > 1 || sy < -1)
                        sy = sy / 2;
                    const [px, py] = node2.pos;
                    node2.pos = [px + sx, py + sy];
                }
                current = current.next;
            }
            tailVisited.add(tail.stringPos());
            // print(head, maximum)
        }
    });
    return tailVisited.size;
});
exports.part2 = part2;
