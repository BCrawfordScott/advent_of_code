"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const ADJ_DELTAS = [
    [1, 0],
    [-1, 0],
    [0, 1],
    [0, -1],
];
const buildMap = (lines) => {
    const charLines = lines.map((line) => line.split(''));
    let start = [-1, -1];
    let end = [-1, -1];
    const numMap = new Map();
    charLines.forEach((line, i) => {
        line.forEach((char, j) => {
            const pos = `${i},${j}`;
            if (char === 'S') {
                start = [i, j];
                const value = 'a'.charCodeAt(0);
                numMap.set(pos, { pos: [i, j], value, shortest: Infinity });
            }
            else if (char === 'E') {
                end = [i, j];
                const value = 'z'.charCodeAt(0);
                numMap.set(pos, { pos: [i, j], value, shortest: Infinity });
            }
            else {
                const value = char.charCodeAt(0);
                numMap.set(pos, { pos: [i, j], value, shortest: Infinity });
            }
        });
    });
    return [start, end, numMap];
};
const toKey = (pos) => pos.join(',');
const getAdjacentPathNodes = (currentNode, map, visited) => {
    const { pos } = currentNode;
    const allAdjacents = ADJ_DELTAS.map((delta) => {
        const [dx, dy] = delta;
        const [cx, cy] = pos;
        const key = toKey([cx + dx, cy + dy]);
        return map.get(key);
    });
    return allAdjacents.filter((node) => {
        if (node === undefined)
            return false;
        if (visited.has(node))
            return false;
        const { value } = node;
        return currentNode.value >= value || currentNode.value === value - 1;
    });
};
const getAdjacentPathNodesPart2 = (currentNode, map, visited) => {
    const { pos } = currentNode;
    const allAdjacents = ADJ_DELTAS.map((delta) => {
        const [dx, dy] = delta;
        const [cx, cy] = pos;
        const key = toKey([cx + dx, cy + dy]);
        return map.get(key);
    });
    return allAdjacents.filter((node) => {
        if (node === undefined)
            return false;
        if (visited.has(node))
            return false;
        const { value } = node;
        return currentNode.value <= value || currentNode.value === value + 1;
    });
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const lines = yield (0, helpers_1.toArray)('day12.txt', '\n');
    const [start, end, map] = buildMap(lines);
    const visited = new Set();
    const unvisited = new Set(map.values());
    const startNode = map.get(toKey(start));
    const endNode = map.get(toKey(end));
    if (!startNode)
        throw 'Broken from the start';
    if (!endNode)
        throw 'Broken from the end';
    startNode.shortest = 0;
    const startAdjacents = getAdjacentPathNodes(startNode, map, visited);
    startAdjacents.forEach((node) => {
        node.shortest = startNode.shortest + 1;
    });
    unvisited.delete(startNode);
    visited.add(startNode);
    let currentNode = Array.from(unvisited).sort((a, b) => a.shortest - b.shortest)[0];
    while (currentNode) {
        const adjacentNodes = getAdjacentPathNodes(currentNode, map, visited);
        adjacentNodes.forEach((node) => {
            const currentDistance = currentNode ? currentNode.shortest + 1 : -1;
            if (node.shortest > currentDistance)
                node.shortest = currentDistance;
        });
        unvisited.delete(currentNode);
        visited.add(currentNode);
        currentNode = Array.from(unvisited).sort((a, b) => a.shortest - b.shortest)[0];
        if (visited.has(endNode)) {
            currentNode = null;
        }
    }
    return (_a = map.get(toKey(end))) === null || _a === void 0 ? void 0 : _a.shortest;
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const lines = yield (0, helpers_1.toArray)('day12.txt', '\n');
    const [start, end, map] = buildMap(lines);
    const visited = new Set();
    const unvisited = new Set(map.values());
    const startNode = map.get(toKey(end));
    const endNode = map.get(toKey(start));
    if (!startNode)
        throw 'Broken from the start';
    if (!endNode)
        throw 'Broken from the end';
    startNode.shortest = 0;
    const startAdjacents = getAdjacentPathNodesPart2(startNode, map, visited);
    startAdjacents.forEach((node) => {
        node.shortest = startNode.shortest + 1;
    });
    unvisited.delete(startNode);
    visited.add(startNode);
    let currentNode = Array.from(unvisited).sort((a, b) => a.shortest - b.shortest)[0];
    while (currentNode) {
        const adjacentNodes = getAdjacentPathNodesPart2(currentNode, map, visited);
        adjacentNodes.forEach((node) => {
            const currentDistance = currentNode ? currentNode.shortest + 1 : -1;
            if (node.shortest > currentDistance)
                node.shortest = currentDistance;
        });
        unvisited.delete(currentNode);
        visited.add(currentNode);
        currentNode = Array.from(unvisited).sort((a, b) => a.shortest - b.shortest)[0];
        // if (visited.has(endNode)) {
        //   currentNode = null;
        // }
    }
    const aCode = 'a'.charCodeAt(0);
    const shortestA = Array.from(map.values())
        .filter((node) => node.value === aCode)
        .sort((a, b) => a.shortest - b.shortest)[0];
    return shortestA.shortest;
});
exports.part2 = part2;
