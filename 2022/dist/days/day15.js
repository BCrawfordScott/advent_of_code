"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const manahattanDistance = (point1, point2) => {
    const [x1, y1] = point1;
    const [x2, y2] = point2;
    return Math.abs(x1 - x2) + Math.abs(y1 - y2);
};
const INPUT_PATTERN = /Sensor at x=(.+), y=(.+): closest beacon is at x=(.+), y=(.+)/;
class Sensor {
    constructor(sensor, beacon) {
        this.sensor = sensor;
        this.beacon = beacon;
        this.maxDistance = manahattanDistance(sensor, beacon);
        this.minX = sensor[0] - this.maxDistance;
        this.maxX = sensor[0] + this.maxDistance;
        this.minY = sensor[1] - this.maxDistance;
        this.maxY = sensor[1] + this.maxDistance;
    }
    mDistanceFrom(point) {
        return manahattanDistance(this.sensor, point);
    }
}
;
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day15.txt', '\n');
    const sensors = input.map((line) => {
        const match = line.match(INPUT_PATTERN);
        if (!match)
            throw 'REGEX didnt work';
        const [_line, sx, sy, bx, by] = match;
        return new Sensor([parseInt(sx), parseInt(sy)], [parseInt(bx), parseInt(by)]);
    });
    const beaconPositions = new Set(sensors.map((sensor) => sensor.beacon.join(',')));
    const y2 = 2000000;
    const impossiblePositions = new Set();
    sensors.forEach((sensor) => {
        if (y2 >= sensor.minY && y2 <= sensor.maxY) {
            const { maxDistance, sensor: pos } = sensor;
            const [sx, sy] = pos;
            const x2op1 = maxDistance - Math.abs(sy - y2) + sx;
            const x2op2 = (maxDistance - Math.abs(sy - y2) - sx) * -1;
            const [start, end] = [x2op1, x2op2].sort((a, b) => a - b);
            for (let x = start; x <= end; x++) {
                if (!beaconPositions.has(`${x},${y2}`))
                    impossiblePositions.add(x);
            }
        }
    });
    return impossiblePositions.size;
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day15.txt', '\n');
    const sensors = input.map((line) => {
        const match = line.match(INPUT_PATTERN);
        if (!match)
            throw 'REGEX didnt work';
        const [_line, sx, sy, bx, by] = match;
        return new Sensor([parseInt(sx), parseInt(sy)], [parseInt(bx), parseInt(by)]);
    });
    let adjacentSensors = null;
    for (let i = 0; i < sensors.length - 1; i++) {
        for (let j = i + 1; j < sensors.length; j++) {
            if (!adjacentSensors) {
                const sensor1 = sensors[i];
                const sensor2 = sensors[j];
                if ((sensor1.mDistanceFrom(sensor2.sensor) - sensor1.maxDistance - sensor2.maxDistance) == 2) {
                    adjacentSensors = [sensor1, sensor2];
                }
            }
        }
    }
    if (!adjacentSensors)
        throw 'Couldnt find adjacent sensors';
    const [sensor1, sensor2] = adjacentSensors;
    const { sensor: [s1x, s1y] } = sensor1;
    const { sensor: [s2x, s2y] } = sensor2;
    const possiblePositions = new Set();
    const found = [];
    let minY = sensor1.minY < 0 ? 0 : sensor1.minY;
    let maxY = sensor1.maxY > 4000000 ? 4000000 : sensor1.maxY;
    for (let y = minY; y <= maxY; y++) {
        const x2op1 = (sensor1.maxDistance + 1) - Math.abs(s1y - y) + s1x;
        const x2op2 = ((sensor1.maxDistance + 1) - Math.abs(s1y - y) - s1x) * -1;
        possiblePositions.add(`${x2op1},${y}`);
        possiblePositions.add(`${x2op2},${y}`);
    }
    minY = sensor2.minY < 0 ? 0 : sensor2.minY;
    maxY = sensor2.maxY > 4000000 ? 4000000 : sensor2.maxY;
    for (let y = minY; y <= maxY; y++) {
        const x2op1 = (sensor2.maxDistance + 1) - Math.abs(s2y - y) + s2x;
        const x2op2 = ((sensor2.maxDistance + 1) - Math.abs(s2y - y) - s2x) * -1;
        if (possiblePositions.has(`${x2op1},${y}`))
            found.push([x2op1, y]);
        if (possiblePositions.has(`${x2op2},${y}`))
            found.push([x2op2, y]);
    }
    const final = found.find((potential) => {
        return sensors.every(sensor => sensor.mDistanceFrom(potential) > sensor.maxDistance);
    });
    if (!final)
        throw 'Couldnt find final';
    const [fx, fy] = final;
    return BigInt(fx) * BigInt(4000000) + BigInt(fy);
});
exports.part2 = part2;
