"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const COMMAND_REGEX = /move (\d+) from (\d+) to (\d+)/;
const toCommandTuple = (command) => {
    const intCommand = command.match(COMMAND_REGEX);
    return intCommand ? intCommand.slice(1) : [];
};
const processInput = () => __awaiter(void 0, void 0, void 0, function* () {
    const [startStacks, commandList] = yield (0, helpers_1.toArray)('day5.txt', '\n\n');
    const stacks = startStacks.split('\n')
        .reverse()
        .slice(1)
        .reduce((stcks, row) => {
        row.split('').forEach((crate, idx) => {
            const address = Math.floor(idx / 4);
            if (!stcks[address])
                stcks[address] = [];
            if (crate.match(/[A-Z]/))
                stcks[address].push(crate);
        });
        return stcks;
    }, []);
    const commands = commandList.split('\n').map(toCommandTuple);
    return [stacks, commands];
});
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const [stacks, commands] = yield processInput();
    commands.forEach((command) => {
        const [strAmmount, fromPlus, toPlus] = command;
        const ammount = parseInt(strAmmount);
        const from = parseInt(fromPlus) - 1;
        const to = parseInt(toPlus) - 1;
        for (let times = ammount || 0; times > 0; times--) {
            const current = stacks[from].pop();
            if (current)
                stacks[to].push(current);
        }
    });
    return stacks.reduce((endString, stack) => `${endString}${stack.pop()}`, '');
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const [stacks, commands] = yield processInput();
    commands.forEach((command) => {
        const [strAmmount, fromPlus, toPlus] = command;
        const ammount = parseInt(strAmmount);
        const from = parseInt(fromPlus) - 1;
        const to = parseInt(toPlus) - 1;
        const carrier = [];
        for (let times = ammount || 0; times > 0; times--) {
            const current = stacks[from].pop();
            if (current)
                carrier.push(current);
        }
        while (carrier.length > 0) {
            const current = carrier.pop();
            if (current)
                stacks[to].push(current);
        }
    });
    return stacks.reduce((endString, stack) => `${endString}${stack.pop()}`, '');
});
exports.part2 = part2;
