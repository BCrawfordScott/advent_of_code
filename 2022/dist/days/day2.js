"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const SHAPE_POINTS = {
    'X': 1,
    'Y': 2,
    'Z': 3
};
const SHAPE_POINTS2 = {
    'A': 1,
    'B': 2,
    'C': 3
};
const determineOutcome = (opponent, player) => {
    switch (opponent) {
        case 'A':
            if (player === 'X')
                return 3;
            if (player === 'Y')
                return 6;
            if (player === 'Z')
                return 0;
            return null;
        case 'B':
            if (player === 'X')
                return 0;
            if (player === 'Y')
                return 3;
            if (player === 'Z')
                return 6;
            return null;
        case 'C':
            if (player === 'X')
                return 6;
            if (player === 'Y')
                return 0;
            if (player === 'Z')
                return 3;
            return null;
        default:
            return null;
    }
};
const determineOutcome2 = (opponent, player) => {
    switch (opponent) {
        case 'A':
            if (player === 'X')
                return [0, 'C'];
            if (player === 'Y')
                return [3, 'A'];
            if (player === 'Z')
                return [6, 'B'];
            return null;
        case 'B':
            if (player === 'X')
                return [0, 'A'];
            if (player === 'Y')
                return [3, 'B'];
            if (player === 'Z')
                return [6, 'C'];
            return null;
        case 'C':
            if (player === 'X')
                return [0, 'B'];
            if (player === 'Y')
                return [3, 'C'];
            if (player === 'Z')
                return [6, 'A'];
            return null;
        default:
            return null;
    }
};
const tallyCommand = (command) => {
    const [opponent, player] = command.split(' ');
    const outcomePoints = determineOutcome(opponent, player);
    if (outcomePoints === null)
        throw "Womp womp";
    return outcomePoints + SHAPE_POINTS[player];
};
const tallyCommand2 = (command) => {
    const [opponent, player] = command.split(' ');
    const outcomePoints = determineOutcome2(opponent, player);
    if (outcomePoints === null)
        throw "Womp womp";
    const [outcome, shape] = outcomePoints;
    return outcome + SHAPE_POINTS2[shape];
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const commands = yield (0, helpers_1.toArray)('day2.txt', '\n');
    return commands.map(tallyCommand).reduce((total, current) => total + current);
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const commands = yield (0, helpers_1.toArray)('day2.txt', '\n');
    return commands.map(tallyCommand2).reduce((total, current) => total + current);
});
exports.part2 = part2;
