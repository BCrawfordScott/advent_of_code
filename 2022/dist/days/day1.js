"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const toCalTotal = (calList) => {
    const calories = calList.split('\n').map((stringNum) => parseInt(stringNum));
    return calories.reduce((total, cal) => total + cal, 0);
};
const ascending = (a, b) => a - b;
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const calorieLists = yield (0, helpers_1.toArray)('day1.txt', '\n\n');
    const calorieTotals = calorieLists.map(toCalTotal);
    return (0, helpers_1.max)(calorieTotals);
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const calorieLists = yield (0, helpers_1.toArray)('day1.txt', '\n\n');
    const calorieTotals = calorieLists.map(toCalTotal);
    const sortedTotals = calorieTotals.sort(ascending);
    const sortedLength = sortedTotals.length;
    return sortedTotals[sortedLength - 1] + sortedTotals[sortedLength - 2] + sortedTotals[sortedLength - 3];
});
exports.part2 = part2;
// const part1 = (input: string): number => {
//   const calorieLists: string[] = input.split('\n\n');
//   const calorieTotals: number[] = calorieLists.map((calList: string): number => {
//     const calories: number[] = calList.split('\n').map((stringNum: string) => parseInt(stringNum));
//     return calories.reduce((total: number, cal: number) => total + cal, 0);
//   });
//   return calorieTotals.sort((a: number, b: number) => a - b)[calorieTotals.length - 1];
// };
// const part2 = (input: string): number => {
//   const calorieLists: string[] = input.split('\n\n');
//   const calorieTotals: number[] = calorieLists.map((calList: string): number => {
//     const calories: number[] = calList.split('\n').map((stringNum: string) => parseInt(stringNum));
//     return calories.reduce((total: number, cal: number) => total + cal, 0);
//   });
//   const sortedTotals = calorieTotals.sort((a: number, b: number) => a - b);
//   const sortedLength = sortedTotals.length;
//   return sortedTotals[sortedLength - 1] + sortedTotals[sortedLength - 2] + sortedTotals[sortedLength - 3]
// };
