"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const checkOrder = (left, right) => {
    const longest = left.length > right.length ? left.length : right.length;
    for (let i = 0; i < longest; i++) {
        const l = left[i];
        const r = right[i];
        if (typeof l === 'number' && typeof r === 'number') {
            if (l < r)
                return -1;
            if (l > r)
                return 1;
        }
        else if (typeof l === 'object' && typeof r === 'object') {
            const test = checkOrder(l, r);
            if (test !== 0)
                return test;
        }
        else if (typeof l === 'number' && typeof r === 'object') {
            const test = checkOrder([l], r);
            if (test !== 0)
                return test;
        }
        else if (typeof l === 'object' && typeof r === 'number') {
            const test = checkOrder(l, [r]);
            if (test !== 0)
                return test;
            ;
        }
        else if (!l && r) {
            return -1;
        }
        else if (l && !r) {
            return 1;
        }
    }
    return 0;
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day13.txt', '\n\n');
    const correctOrder = new Set();
    input.forEach((pair, idx) => {
        const [left, right] = pair.split('\n');
        const lPacket = JSON.parse(left);
        const rPacket = JSON.parse(right);
        const ordered = checkOrder(lPacket, rPacket);
        if (ordered === -1)
            correctOrder.add(idx + 1);
    });
    return Array.from(correctOrder).reduce((total, next) => total + next);
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day13.txt', '\n\n');
    const packets = input.reduce((parsed, pair) => {
        const [left, right] = pair.split('\n');
        parsed.push(JSON.parse(left));
        parsed.push(JSON.parse(right));
        return parsed;
    }, []);
    const two = [[2]];
    const six = [[6]];
    const fewertwo = packets.reduce((total, packet) => checkOrder(packet, two) === -1 ? total + 1 : total, 1);
    const fewersix = packets.reduce((total, packet) => checkOrder(packet, six) === -1 ? total + 1 : total, 1);
    return fewertwo * (fewersix + 1);
});
exports.part2 = part2;
