"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const NOOP_REGEX = /noop/;
const ADDX_REGEX = /addx/;
const CHECK_CYCLES = new Set([20, 60, 100, 140, 180, 220]);
const analyze = (cycle, x, totals) => {
    if (CHECK_CYCLES.has(cycle)) {
        totals.push(x * cycle);
    }
};
const draw = (cycle, x, canvas) => {
    const row = Math.floor(cycle / 40);
    const column = cycle % 40;
    const pixel = (column === x || column === x - 1 || column === x + 1) ? '#' : ' ';
    canvas[row][column] = pixel;
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day10.txt', '\n');
    let cycle = 0;
    let x = 1;
    const totals = [];
    input.forEach((command) => {
        if (command.match(NOOP_REGEX)) {
            cycle = cycle + 1;
            analyze(cycle, x, totals);
        }
        if (command.match(ADDX_REGEX)) {
            cycle = cycle + 1;
            analyze(cycle, x, totals);
            cycle = cycle + 1;
            analyze(cycle, x, totals);
            x = x + parseInt(command.split(' ')[1]);
        }
    });
    return totals.reduce((sum, current) => sum + current);
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day10.txt', '\n');
    let cycle = 0;
    let x = 1;
    const canvas = [
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
    ];
    input.forEach((command) => {
        if (command.match(NOOP_REGEX)) {
            cycle = cycle + 1;
            draw(cycle - 1, x, canvas);
        }
        if (command.match(ADDX_REGEX)) {
            cycle = cycle + 1;
            draw(cycle - 1, x, canvas);
            cycle = cycle + 1;
            draw(cycle - 1, x, canvas);
            x = x + parseInt(command.split(' ')[1]);
        }
    });
    const final = canvas.map((row) => row.join('')).join('\n');
    return '\n' + final;
});
exports.part2 = part2;
