"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const DELTA_N = [-1, 0];
const DELTA_S = [1, 0];
const DELTA_E = [0, 1];
const DELTA_W = [0, -1];
const DELTAS = [
    DELTA_N,
    DELTA_W,
    DELTA_S,
    DELTA_E,
];
const analyzeRows = (input, allVisible) => {
    let greatest;
    for (let i = 0; i < input.length; i++) {
        greatest = -1;
        for (let j = 0; j < input[i].length; j++) {
            const element = parseInt(input[i][j]);
            if (element > greatest) {
                allVisible.add(`${i},${j}`);
                greatest = element;
            }
        }
        greatest = -1;
        for (let j = input[i].length - 1; j >= 0; j--) {
            const element = parseInt(input[i][j]);
            if (element > greatest) {
                allVisible.add(`${i},${j}`);
                greatest = element;
            }
        }
    }
};
const analyzeColumns = (input, allVisible) => {
    let greatest;
    for (let j = 0; j < input[0].length; j++) {
        greatest = -1;
        for (let i = 0; i < input.length; i++) {
            const element = parseInt(input[i][j]);
            if (element > greatest) {
                allVisible.add(`${i},${j}`);
                greatest = element;
            }
        }
        greatest = -1;
        for (let i = input.length - 1; i >= 0; i--) {
            const element = parseInt(input[i][j]);
            if (element > greatest) {
                allVisible.add(`${i},${j}`);
                greatest = element;
            }
        }
    }
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = (yield (0, helpers_1.toArray)('day8.txt', '\n')).map((row) => row.split(''));
    const allVisible = new Set();
    analyzeRows(input, allVisible);
    analyzeColumns(input, allVisible);
    return allVisible.size;
});
exports.part1 = part1;
const analyzeDeltas = (input, i, j) => {
    const posHeight = parseInt(input[i][j]);
    return DELTAS.map((delta) => {
        const [dx, dy] = delta;
        let [x, y] = [i + dx, j + dy];
        let total = 0;
        let end = false;
        while (!end && (x >= 0 && x < input.length) && (y >= 0 && y < input[0].length)) {
            let current = parseInt(input[x][y]);
            if (current >= posHeight) {
                total = total + 1;
                end = true;
            }
            else {
                total = total + 1;
                x = x + dx;
                y = y + dy;
            }
        }
        ;
        return total;
    });
};
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = (yield (0, helpers_1.toArray)('day8.txt', '\n')).map((row) => row.split(''));
    const scenicScores = new Map();
    for (let i = 0; i < input.length; i++) {
        for (let j = 0; j < input[0].length; j++) {
            const score = analyzeDeltas(input, i, j);
            const totalScore = score.reduce((product, current) => product * current);
            scenicScores.set(`${i},${j}`, totalScore);
        }
    }
    return (0, helpers_1.max)(Array.from(scenicScores.values()));
});
exports.part2 = part2;
