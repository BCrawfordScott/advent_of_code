"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day6.txt', '');
    let start = 0;
    let end = 4;
    let found = false;
    while (!found && end < input.length) {
        const window = new Set(input.slice(start, end));
        if (window.size === 4)
            found = true;
        end = end + 1;
        start = start + 1;
    }
    ;
    return end - 1;
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day6.txt', '');
    let start = 0;
    let end = 14;
    let found = false;
    while (!found && end < input.length) {
        const window = new Set(input.slice(start, end));
        if (window.size === 14)
            found = true;
        end = end + 1;
        start = start + 1;
    }
    ;
    return end - 1;
});
exports.part2 = part2;
