"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const MONK_NUM = /Monkey (\d+):/;
const STARTING = /Starting items: (.*)/;
const OPERATION = /Operation: new = old (.) (old|\d+)/;
const TEST = /Test: divisible by (\d+)/;
const TRUE = /If true: throw to monkey (\d+)/;
const FALSE = /If false: throw to monkey (\d+)/;
const matchers = [
    MONK_NUM,
    STARTING,
    OPERATION,
    TEST,
    TRUE,
    FALSE,
];
class Monkey {
    constructor(inputString) {
        const rawAttrs = matchers.map((matcher) => inputString.match(matcher));
        if (rawAttrs.some((match) => match === null))
            throw `Regex error on ${inputString}`;
        this.id = rawAttrs[0] ? parseInt(rawAttrs[0][1]) : -1;
        this.items = rawAttrs[1] ? rawAttrs[1][1].replace(' ', '').split(',').map((string => parseInt(string))) : [];
        const operand = rawAttrs[2] ? rawAttrs[2][1] : '+';
        const amount = rawAttrs[2] ? rawAttrs[2][2] : '0';
        this.op = (value) => {
            const arg = amount && parseInt(amount) ? parseInt(amount) : value;
            return operand === '+' ? value + arg : (value || 1) * arg;
        };
        this.test = rawAttrs[3] ? parseInt(rawAttrs[3][1]) : 0;
        this.true = rawAttrs[4] ? parseInt(rawAttrs[4][1]) : -1;
        this.false = rawAttrs[5] ? parseInt(rawAttrs[5][1]) : -1;
    }
    nextItem() {
        const item = this.items[0];
        this.items = this.items.slice(1);
        return item;
    }
}
const PRINT_ROUNDS = new Set([1, 20, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000]);
const print = (monkeys) => {
    return monkeys.map((monkey) => {
        return `Monkey ${monkey.id}: ${monkey.items.join(', ')}`;
    }).join('\n');
};
const shouldPrint = (round) => {
    return PRINT_ROUNDS.has(round + 1);
};
const processRound = (monkey, monkeys) => {
    while (monkey.items.length) {
        const currentItem = monkey.nextItem();
        const operated = monkey.op(currentItem);
        const decreased = operated.valueOf() / 3;
        const passedTest = decreased % monkey.test === 0;
        const targetMonk = passedTest ? monkey.true : monkey.false;
        monkeys[targetMonk].items.push(decreased);
    }
};
const processRoundPart2 = (monkey, monkeys) => {
    const modProd = monkeys.reduce((prod, monkey) => prod * monkey.test, 1);
    while (monkey.items.length) {
        const currentItem = monkey.nextItem();
        const operated = monkey.op(currentItem);
        const testResult = operated % monkey.test;
        const passedTest = testResult === 0;
        const targetMonk = passedTest ? monkey.true : monkey.false;
        monkeys[targetMonk].items.push(operated % modProd);
    }
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day11.txt', '\n\n');
    const monkeys = [];
    input.forEach((line) => {
        const monk = new Monkey(line);
        monkeys.push(monk);
    });
    const monkeyActions = [0, 0, 0, 0, 0, 0, 0, 0];
    for (let round = 0; round < 20; round++) {
        monkeys.forEach((monkey) => {
            monkeyActions[monkey.id] = monkeyActions[monkey.id] + monkey.items.length;
            processRound(monkey, monkeys);
        });
    }
    const sorted = monkeyActions.sort((a, b) => a - b);
    const { length } = sorted;
    return sorted[length - 2] * sorted[length - 1];
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day11.txt', '\n\n');
    const monkeys = [];
    input.forEach((line) => {
        const monk = new Monkey(line);
        monkeys.push(monk);
    });
    const monkeyActions = [0, 0, 0, 0, 0, 0, 0, 0];
    // console.log('Start:');
    // console.log(print(monkeys));
    for (let round = 0; round < 10000; round++) {
        monkeys.forEach((monkey) => {
            monkeyActions[monkey.id] = monkeyActions[monkey.id] + monkey.items.length;
            processRoundPart2(monkey, monkeys);
        });
        // if (shouldPrint(round)) {
        //   console.log('Round -', round + 1);
        //   for (let i = 0; i < monkeyActions.length; i++) {
        //     const monkeyTouches = monkeyActions[i];
        //     console.log(`Monkey ${i} inspected items ${monkeyTouches} times.`)
        //   }
        // }
    }
    const sorted = monkeyActions.sort((a, b) => a - b);
    const { length } = sorted;
    return sorted[length - 2] * sorted[length - 1];
});
exports.part2 = part2;
