"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const firstIntersection = (set1, set2) => {
    let firstIntersect;
    set1.forEach((element) => {
        if (firstIntersect)
            return;
        if (set2.has(element))
            firstIntersect = element;
    });
    return firstIntersect || '';
};
const allintersections = (set1, set2) => {
    const intersections = [];
    set1.forEach((element) => {
        if (set2.has(element))
            intersections.push(element);
    });
    return intersections;
};
const priorityValue = (char) => {
    if (!char.match(/[A-z]/))
        throw 'Regex Womp';
    if (char.match(/[a-z]/))
        return char.charCodeAt(0) % 96;
    return char.charCodeAt(0) % 96 - 38;
};
const toPrioritizedDuplicate = (contents) => {
    const firstHalf = contents.slice(0, contents.length / 2);
    const secondHalf = contents.slice(contents.length / 2, contents.length);
    const pocket1Set = new Set(firstHalf.split(''));
    const pocket2Set = new Set(secondHalf.split(''));
    const dup = firstIntersection(pocket1Set, pocket2Set);
    if (dup === '')
        throw 'womp womp';
    return dup;
};
const toPrioritizedTriplicate = (rucksacks) => {
    const [sack1, sack2, sack3] = rucksacks;
    const sack1Set = new Set(sack1.split(''));
    const sack2Set = new Set(sack2.split(''));
    const sack3Set = new Set(sack3.split(''));
    const potentialDups = allintersections(sack1Set, sack2Set);
    const dup = firstIntersection(new Set(potentialDups), sack3Set);
    if (dup === '')
        throw 'womp womp';
    return dup;
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day3.txt', '\n');
    return input.map(toPrioritizedDuplicate)
        .map(priorityValue)
        .reduce((total, current) => total + current);
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day3.txt', '\n');
    const groupings = new Map();
    input.forEach((current, idx) => {
        var _a;
        const key = Math.floor(idx / 3);
        if (!groupings.has(key))
            groupings.set(key, []);
        (_a = groupings.get(key)) === null || _a === void 0 ? void 0 : _a.push(current);
    });
    3;
    return Array.from(groupings.values())
        .map(toPrioritizedTriplicate)
        .map(priorityValue)
        .reduce((total, current) => total + current);
});
exports.part2 = part2;
