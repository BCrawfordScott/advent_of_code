"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
// const INPUT_REGEX = /Valve (.*) has flow rate=(.*); [tunnels lead|tunnel leads] to valve(?=s?) (.*)/
const INPUT_REGEX = /Valve (.*) has flow rate=(.*); tunnel(?:s?) lead(?:s?) to valve(?:s?) (.*)/;
const buildValveGraph = (input) => {
    const valveGraph = input.reduce((vGraph, line) => {
        const match = line.match(INPUT_REGEX);
        if (!match)
            throw `Error parsing line: ${line}`;
        const [_line, valve, flow, stringTunnels] = match;
        const tunnels = stringTunnels.split(', ');
        vGraph[valve] = {
            name: valve,
            flow: parseInt(flow),
            tunnels,
        };
        return vGraph;
    }, {});
    return valveGraph;
};
const findMaxPressure = (arg) => {
    const { valve, valveGraph, memo, minute, open, } = arg;
    if (minute <= 0)
        return 0;
    const foundClosed = memo.get(`${valve}-${minute}`); // max value for this valve at this minute
    const foundOpen = memo.get(`${valve}-${minute}-open`); // max value for this valve at this minute
    const valveNode = valveGraph[valve];
    if (!valveNode)
        throw `Valve ${valve} wasn't in graph: ${valveGraph}`;
    const { tunnels, flow } = valveNode;
    if (open.has(valve)) {
        if (foundClosed) {
            return foundClosed;
        }
        else {
            const tunnelVals = tunnels.map((tunnel) => {
                const nextArg = {
                    valve: tunnel,
                    valveGraph,
                    memo,
                    minute: minute - 1,
                    open: new Set(open),
                };
                return findMaxPressure(nextArg);
            });
            const maxNoOpen = (0, helpers_1.max)(tunnelVals);
            memo.set(`${valve}-${minute}`, maxNoOpen);
            return maxNoOpen;
        }
    }
    else {
        if (foundOpen) {
            return foundOpen;
        }
        else {
            const tunnelVals = tunnels.map((tunnel) => {
                const nextArg = {
                    valve: tunnel,
                    valveGraph,
                    memo,
                    minute: minute - 1,
                    open: new Set(open),
                };
                return findMaxPressure(nextArg);
            });
            const maxNoOpen = (0, helpers_1.max)(tunnelVals);
            const nextOpen = new Set(open);
            nextOpen.add(valve);
            const tunnelValsWithOpen = tunnels.map((tunnel) => {
                const nextArg = {
                    valve: tunnel,
                    valveGraph,
                    memo,
                    minute: minute - 2,
                    open: nextOpen,
                };
                return flow * (minute - 1) + findMaxPressure(nextArg);
            });
            const maxWithOpen = (0, helpers_1.max)(tunnelValsWithOpen);
            const highest = (0, helpers_1.max)([maxNoOpen, maxWithOpen]);
            memo.set(`${valve}-${minute}`, highest);
            return highest;
            // if (maxWithOpen > maxNoOpen) {
            //   memo.set(`${valve}-${minute}`, maxWithOpen);
            //   return maxWithOpen;
            // } else {
            //   memo.set(`${valve}-${minute}`, maxNoOpen);
            //   return maxNoOpen;
            // }
        }
    }
    // if (found) return found;
    // const tunnelVals = tunnels.map((tunnel: string): number => {
    //   const nextArg: CalcArg = {
    //     valve: tunnel,
    //     valveGraph,
    //     memo,
    //     minute: minute - 1,
    //     open: new Set(open),
    //   };
    //   return findMaxPressure(nextArg);
    // });
    // const maxNoOpen = max(tunnelVals);
    // if (open.has(valve)) {
    // memo.set(`${valve}-${minute}`, maxNoOpen);
    // // const biggest = memo.get(`${valve}-${minute}`);
    // // if (!biggest) throw ' everythings broken ';
    // return maxNoOpen;
    // } else {
    // let maxWithOpen;
    //   const nextOpen = new Set(open);
    //   nextOpen.add(valve);
    //   const tunnelValsWithOpen = tunnels.map((tunnel: string): number => {
    //     const nextArg: CalcArg = {
    //       valve: tunnel,
    //       valveGraph,
    //       memo,
    //       minute: minute - 2,
    //       open: nextOpen,
    //     };
    //     return flow * (minute - 1) + findMaxPressure(nextArg);
    //   });
    //   maxWithOpen = max(tunnelValsWithOpen);
    // }
    //   if (maxWithOpen > maxNoOpen) {
    //     memo.set(`${valve}-${minute }`, maxWithOpen);
    //     return maxWithOpen;
    //   } else {
    //     memo.set(`${valve}-${minute}`, maxNoOpen);
    //     return maxNoOpen;
    //   }
    // }
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    // const input = await toArray('day16.txt', '\n');
    const input = yield (0, helpers_1.toArray)('day16test.txt', '\n');
    const valveGraph = buildValveGraph(input);
    const memo = new Map();
    // const maxFromStart = Object.keys(valveGraph).map((valveGraphKey: string): number => {
    //   const startArg: CalcArg = {
    //     valve: valveGraphKey,
    //     valveGraph,
    //     open: new Set<string>(),
    //     memo,
    //     minute: 30,
    //   };
    //   return findMaxPressure(startArg);
    // });
    // return max(maxFromStart);
    // const maxFromStart = Object.keys(valveGraph).map((valveGraphKey: string): number => {
    const startArg = {
        valve: 'DD',
        valveGraph,
        open: new Set(),
        memo,
        minute: 30,
    };
    // return findMaxPressure(startArg);
    // });
    // return max(maxFromStart);
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () { });
exports.part2 = part2;
