"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const PATTERNS = [
    {
        top: [1, 1, 1, 1],
        bottom: [0, 0, 0, 0],
    },
    {
        top: [2, 3, 2],
        bottom: [-1, -2, -1],
    },
    {
        top: [1, 1, 3],
        bottom: [0, 0, 0],
    },
    {
        top: [4],
        bottom: [0],
    },
    {
        top: [2, 2],
        bottom: [0, 0],
    }
];
const PATTERN_MAX_OFFSET = [
    3,
    4,
    4,
    6,
    5,
];
const MOVE_MAP = {
    '>': 1,
    '<': -1,
};
;
class RockLine {
    constructor(startIdx, pattern, offset = 0) {
        const defaultLine = [
            {
                top: 0,
                bottom: Infinity,
            },
            {
                top: 0,
                bottom: Infinity,
            },
            {
                top: 0,
                bottom: Infinity,
            },
            {
                top: 0,
                bottom: Infinity,
            },
            {
                top: 0,
                bottom: Infinity,
            },
            {
                top: 0,
                bottom: Infinity,
            },
            {
                top: 0,
                bottom: Infinity,
            },
        ];
        const { top, bottom } = PATTERNS[pattern];
        console.log('StartIdx:', startIdx, 'top:', top, 'bottom:', bottom);
        this.rockLine = defaultLine.reduce((finalLine, dVal, idx) => {
            if (idx >= offset) {
                const topVal = top[idx - offset];
                const botVal = bottom[idx - offset];
                if (topVal === undefined || botVal === undefined) {
                    finalLine.push(dVal);
                    return finalLine;
                }
                finalLine.push({ top: topVal, bottom: startIdx + botVal });
            }
            else {
                finalLine.push(dVal);
            }
            return finalLine;
        }, []);
    }
    stop(bottom) {
        for (let i = 0; i < bottom.length; i++) {
            const currBot = bottom[i];
            const lineBottom = this.rockLine[i].bottom;
            if (currBot > lineBottom)
                return currBot;
        }
        return NaN;
    }
}
;
class Stack {
    constructor() {
        this.stack = [];
    }
    peek() {
        return this.stack[this.stack.length - 1];
    }
    push(item) {
        return this.stack.push(item);
    }
    pop() {
        return this.stack.pop();
    }
    size() {
        return this.stack.length;
    }
}
;
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    // const input = await toArray('day17.txt', '');
    const input = yield (0, helpers_1.toArray)('day17test.txt', '');
    let offset = 2;
    let moveIdx = 0;
    let totalRocks = 0;
    let pattern = 0;
    const heights = [0, 0, 0, 0, 0, 0, 0];
    let startIdx = (0, helpers_1.max)(heights) + 3;
    while (totalRocks < 2022) {
        // while (totalRocks < 3) {
        let rockLine = new RockLine(startIdx, pattern, offset);
        console.log('Rockline:', rockLine);
        const stopIdx = rockLine.stop(heights);
        if (!Number.isNaN(stopIdx)) {
            for (let i = offset; i < offset + PATTERNS[pattern].top.length; i++) {
                const addition = rockLine.rockLine[i].top;
                heights[i] = stopIdx + addition;
            }
            pattern = (pattern + 1) % PATTERNS.length;
            ++totalRocks;
            console.log(totalRocks);
            console.log(heights);
            offset = 2;
            startIdx = (0, helpers_1.max)(heights) + 3;
        }
        else {
            const moveKey = input[moveIdx];
            console.log('MoveKey:', moveKey);
            const move = MOVE_MAP[moveKey];
            const maxOffset = PATTERN_MAX_OFFSET[pattern];
            const newOffset = offset + move;
            if (newOffset < 0) {
                offset = 0;
                console.log('new offset was less than 0');
            }
            else if (newOffset > maxOffset) {
                offset = maxOffset;
                console.log('new offset was greater than max');
            }
            else {
                offset = newOffset;
                console.log('new offset is', offset);
            }
            console.log('Offset:', offset);
            moveIdx = (moveIdx + 1) % input.length;
            --startIdx;
        }
    }
    return (0, helpers_1.max)(heights);
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () { });
exports.part2 = part2;
