"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const DIR_REG = /dir (.+)/;
const FILE_REG = /(\d+) (.+)/;
const CD_REG = /\$ cd (.+)/;
const LS_REG = /\$ ls/;
const getDirSize = (dir, tree) => {
    const elements = tree[dir];
    return elements.reduce((total, element) => {
        const current = parseInt(element);
        if (current) {
            return total + current;
        }
        else {
            return total + getDirSize(element, tree);
        }
    }, 0);
};
const calculateTotals = (finalTree) => {
    return Object.keys(finalTree).map((key) => { return getDirSize(key, finalTree); });
};
const buildTree = (input) => {
    const tree = {};
    const [_finalDir, finalTree] = input.reduce((current, command) => {
        const [dir, treeProgress] = current;
        if (command.match(CD_REG)) {
            const match = command.match(CD_REG);
            if (match) {
                const [_, next] = match;
                if (next === '..' && dir) {
                    const newDir = dir.split('/');
                    newDir.pop();
                    const middle = [newDir.join('/'), treeProgress];
                    return middle;
                }
                if (next && !treeProgress[`${dir}/${next}`])
                    treeProgress[`${dir}/${next}`] = [];
                const middle = [`${dir}/${next}`, treeProgress];
                if (next)
                    return middle;
            }
            return current;
        }
        else if (command.match(LS_REG)) {
            return current;
        }
        else if (command.match(DIR_REG)) {
            const match = command.match(DIR_REG);
            if (match) {
                const [_, newDir] = match;
                if (dir)
                    treeProgress[dir].push(`${dir}/${newDir}`);
            }
            const middle = [dir, treeProgress];
            return middle;
        }
        else if (command.match(FILE_REG)) {
            const match = command.match(FILE_REG);
            if (match) {
                const [_, file] = match;
                if (dir)
                    treeProgress[dir].push(file);
            }
            const middle = [dir, treeProgress];
            return middle;
        }
        else {
            return current;
        }
    }, [null, tree]);
    return finalTree;
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day7.txt', '\n');
    const finalTree = buildTree(input);
    const totals = calculateTotals(finalTree);
    return totals.reduce((total, size) => {
        return size <= 100000 ? total + size : total;
    }, 0);
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('day7.txt', '\n');
    const finalTree = buildTree(input);
    const totals = calculateTotals(finalTree);
    const freeSpace = 70000000 - totals[0];
    const options = totals.slice(1).filter((size) => freeSpace + size >= 30000000);
    return (0, helpers_1.min)(options);
});
exports.part2 = part2;
