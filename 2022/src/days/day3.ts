import { toArray } from "../helpers";

const firstIntersection = (set1: Set<any>, set2: Set<any>): any => {
  let firstIntersect: any;
  set1.forEach((element: any) => {
    if (firstIntersect) return;
    if (set2.has(element)) firstIntersect = element;
  });

  return firstIntersect || '';
}

const allintersections = (set1: Set<any>, set2: Set<any>): Array<string> => {
  const intersections: string[] = [];
  set1.forEach((element: any) => {
    if (set2.has(element)) intersections.push(element);
  });

  return intersections;
}

const priorityValue = (char: string): number => {
  if (!char.match(/[A-z]/)) throw 'Regex Womp';
  if (char.match(/[a-z]/)) return char.charCodeAt(0) % 96
  return char.charCodeAt(0) % 96 - 38;
}

const toPrioritizedDuplicate = (contents: string): string => {
  const firstHalf = contents.slice(0, contents.length / 2)
  const secondHalf = contents.slice(contents.length / 2, contents.length);

  const pocket1Set: Set<string> = new Set(firstHalf.split(''));
  const pocket2Set: Set<string> = new Set(secondHalf.split(''));

  const dup = firstIntersection(pocket1Set, pocket2Set);

  if (dup === '') throw 'womp womp';

  return dup;
}
const toPrioritizedTriplicate = (rucksacks: string[]): string => {
  const [sack1, sack2, sack3] = rucksacks;

  const sack1Set: Set<string> = new Set(sack1.split(''));
  const sack2Set: Set<string> = new Set(sack2.split(''));
  const sack3Set: Set<string> = new Set(sack3.split(''));

  const potentialDups = allintersections(sack1Set, sack2Set);
  const dup = firstIntersection(new Set(potentialDups), sack3Set);

  if (dup === '') throw 'womp womp';

  return dup;
}

const part1 = async (): Promise<number> => {
  const input = await toArray('day3.txt','\n');
  return input.map(toPrioritizedDuplicate)
              .map(priorityValue)
              .reduce((total: number, current: number) => total + current);
};
const part2 = async (): Promise<number> => {
  const input = await toArray('day3.txt', '\n');
  const groupings = new Map();
  input.forEach((
    current,
    idx,
  ) => {
    const key = Math.floor(idx / 3)
    if (!groupings.has(key)) groupings.set(key, []);
    groupings.get(key)?.push(current);
  });
3 
  return Array.from(groupings.values())
               .map(toPrioritizedTriplicate)
               .map(priorityValue)
               .reduce((total: number, current: number) => total + current);
};

export {
  part1,
  part2,
};
