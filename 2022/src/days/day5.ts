import { toArray } from "../helpers";
const COMMAND_REGEX = /move (\d+) from (\d+) to (\d+)/;

const toCommandTuple = (command: string): string[] => {
  const intCommand = command.match(COMMAND_REGEX);

  return intCommand ? intCommand.slice(1) : [];
}

const processInput = async (): Promise<[Array<string[]>, string[][]]> => {
  const [startStacks, commandList] = await toArray('day5.txt', '\n\n');
  const stacks = startStacks.split('\n')
                            .reverse()
                            .slice(1)
                            .reduce((stcks: Array<string[]>, row: string) => {
                              row.split('').forEach((crate: string, idx: number) => {
                                const address = Math.floor(idx / 4);
                                if (!stcks[address]) stcks[address] = [];
                                if (crate.match(/[A-Z]/)) stcks[address].push(crate);
                              });
                              return stcks;
                            }, []);

  const commands = commandList.split('\n').map(toCommandTuple);

  return [stacks, commands];
}

const part1 = async (): Promise<string> => {
  const [stacks, commands] = await processInput();

  commands.forEach((command: string[]) => {
    const [strAmmount, fromPlus, toPlus] = command;
    const ammount = parseInt(strAmmount);
    const from = parseInt(fromPlus) - 1;
    const to = parseInt(toPlus) - 1;

    for (let times = ammount || 0; times > 0; times--) {
      const current = stacks[from].pop();
      if (current) stacks[to].push(current);
    }
  });

  return stacks.reduce((endString: string, stack: string[]): string => `${endString}${stack.pop()}`, '');
};
const part2 = async (): Promise<string> => {
  const [stacks, commands] = await processInput();

  commands.forEach((command: string[]) => {
    const [strAmmount, fromPlus, toPlus] = command;
    const ammount = parseInt(strAmmount);
    const from = parseInt(fromPlus) - 1;
    const to = parseInt(toPlus) - 1;

    const carrier: string[] = [];

    for (let times = ammount || 0; times > 0; times--) {
      const current = stacks[from].pop();
      if (current) carrier.push(current);
    }
    while (carrier.length > 0) {
      const current = carrier.pop();
      if (current) stacks[to].push(current);
    }
  });

  return stacks.reduce((endString: string, stack: string[]): string => `${endString}${stack.pop()}`, '');
};

export {
  part1,
  part2,
};
