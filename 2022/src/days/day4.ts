import { toArray } from "../helpers";

const toRanges = (stringRange: string): [number[], number[]] => {
  const [sRange1, sRange2] = stringRange.split(',');
  return [
    sRange1.split('-').map((int: string): number => parseInt(int)),
    sRange2.split('-').map((int: string): number => parseInt(int)),
  ];
}

const part1 = async (): Promise<number> => {
  const input = await toArray('day4.txt', '\n');
  const ranges = input.map(toRanges);
  const overlapCount = ranges.reduce((total: number, currentRanges: [number[], number[]]) => {
    const [range1, range2] = currentRanges;
    const [r1start, r1end] = range1;
    const [r2start, r2end] = range2;
    if (r1start <= r2start && r1end >= r2end) return total + 1
    if (r1start >= r2start && r1end <= r2end) return total + 1

    return total;
  }, 0);

  return overlapCount;
};
const part2 = async (): Promise<number> => {
  const input = await toArray('day4.txt', '\n');
  const ranges = input.map(toRanges);
  const overlapCount = ranges.reduce((total: number, currentRanges: [number[], number[]]) => {
    const [range1, range2] = currentRanges;
    const [r1start, r1end] = range1;
    const [r2start, r2end] = range2;
    if (r1end < r2start || r2end < r1start) return total;

    return total + 1;
  }, 0);

  return overlapCount;
};

export {
  part1,
  part2,
};
