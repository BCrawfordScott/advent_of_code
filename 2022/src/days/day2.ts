import { toArray } from "../helpers";

type commandString = 'A X' | 'A Y' | 'A Z' | 'B X' | 'B Y' | 'B Z' | 'C X' | 'C Y' | 'C Z';

const SHAPE_POINTS: Record<string, number> = {
  'X': 1,
  'Y': 2,
  'Z': 3
};

const SHAPE_POINTS2: Record<string, number> = {
  'A': 1,
  'B': 2,
  'C': 3
};

const determineOutcome = (opponent: string, player: string): 6 | 3 | 0 | null => {
  switch(opponent) {
    case 'A':
      if (player === 'X') return 3;
      if (player === 'Y') return 6;
      if (player === 'Z') return 0;
      return null;
    case 'B':
      if (player === 'X') return 0;
      if (player === 'Y') return 3;
      if (player === 'Z') return 6;
      return null;
    case 'C':
      if (player === 'X') return 6;
      if (player === 'Y') return 0;
      if (player === 'Z') return 3;
      return null;
    default:
      return null;
  }
}

const determineOutcome2 = (opponent: string, player: string): [number, string] | null => {
  switch(opponent) {
    case 'A':
      if (player === 'X') return [0, 'C'];
      if (player === 'Y') return [3, 'A'];
      if (player === 'Z') return [6, 'B'];
      return null;
    case 'B':
      if (player === 'X') return [0, 'A'];
      if (player === 'Y') return [3, 'B'];
      if (player === 'Z') return [6, 'C'];
      return null;
    case 'C':
      if (player === 'X') return [0, 'B'];
      if (player === 'Y') return [3, 'C'];
      if (player === 'Z') return [6, 'A'];
      return null;
    default:
      return null;
  }
}

const tallyCommand = (command: string): number => {
  const [opponent, player] = command.split(' ');
  const outcomePoints: number | null = determineOutcome(opponent, player);
  if (outcomePoints === null) throw "Womp womp"

  return outcomePoints + SHAPE_POINTS[player];
}

const tallyCommand2 = (command: string): number => {
  const [opponent, player] = command.split(' ');
  const outcomePoints: [number, string] | null = determineOutcome2(opponent, player);
  if (outcomePoints === null) throw "Womp womp"


  const [outcome, shape] = outcomePoints;
  return outcome + SHAPE_POINTS2[shape];
}

const part1 = async (): Promise<number> => {
  const commands: string[] = await toArray('day2.txt', '\n');
  return commands.map(tallyCommand).reduce((total, current) => total + current)
  
};

const part2 = async (): Promise<number> => {
  const commands: string[] = await toArray('day2.txt', '\n');
  return commands.map(tallyCommand2).reduce((total, current) => total + current)

};

export {
  part1,
  part2,
};
