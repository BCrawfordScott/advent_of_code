import { max, toArray } from "../helpers";

// const INPUT_REGEX = /Valve (.*) has flow rate=(.*); [tunnels lead|tunnel leads] to valve(?=s?) (.*)/
const INPUT_REGEX = /Valve (.*) has flow rate=(.*); tunnel(?:s?) lead(?:s?) to valve(?:s?) (.*)/

type Valve = {
  name: string;
  flow: number;
  tunnels: string[];
}

type ValveGraph = Record<string, Valve>;

interface CalcArg {
  valve: string,
  valveGraph: ValveGraph,
  open: Set<string>,
  memo: Map<string, number>,
  minute: number,
}


const buildValveGraph = (input: string[]): ValveGraph => {
  const valveGraph: ValveGraph = input.reduce(
    (vGraph: ValveGraph, line: string): ValveGraph => {
      const match = line.match(INPUT_REGEX);
      if (!match) throw `Error parsing line: ${line}`;

      const [_line, valve, flow, stringTunnels] = match;
      const tunnels = stringTunnels.split(', ');
      vGraph[valve] = {
        name: valve,
        flow: parseInt(flow),
        tunnels,
      };

      return vGraph;
    },
    {},
  )

  return valveGraph;
}

const findMaxPressure = (arg: CalcArg): number => {
  const {
    valve,
    valveGraph,
    memo,
    minute,
    open,
  } = arg;

  if (minute <= 0) return 0;

  const foundClosed = memo.get(`${valve}-${minute}`); // max value for this valve at this minute
  const foundOpen = memo.get(`${valve}-${minute}-open`); // max value for this valve at this minute

  const valveNode = valveGraph[valve];
  if (!valveNode) throw `Valve ${valve} wasn't in graph: ${valveGraph}`;
  const { tunnels, flow } = valveNode;

  if (open.has(valve)) {
    if (foundClosed) {
      return foundClosed;
    } else {
      const tunnelVals = tunnels.map((tunnel: string): number => {
        const nextArg: CalcArg = {
          valve: tunnel,
          valveGraph,
          memo,
          minute: minute - 1,
          open: new Set(open),
        };

        return findMaxPressure(nextArg);
      });

      const maxNoOpen = max(tunnelVals);
      memo.set(`${valve}-${minute}`, maxNoOpen);

      return maxNoOpen;
    }
  } else {
    if(foundOpen) {
      return foundOpen;
    } else {
      const tunnelVals = tunnels.map((tunnel: string): number => {
        const nextArg: CalcArg = {
          valve: tunnel,
          valveGraph,
          memo,
          minute: minute - 1,
          open: new Set(open),
        };

        return findMaxPressure(nextArg);
      });

      const maxNoOpen = max(tunnelVals);

      const nextOpen = new Set(open);
      nextOpen.add(valve);

      const tunnelValsWithOpen = tunnels.map((tunnel: string): number => {
        const nextArg: CalcArg = {
          valve: tunnel,
          valveGraph,
          memo,
          minute: minute - 2,
          open: nextOpen,
        };

        return flow * (minute - 1) + findMaxPressure(nextArg);
      });

      const maxWithOpen = max(tunnelValsWithOpen);
      const highest = max([maxNoOpen, maxWithOpen]);
      memo.set(`${valve}-${minute}`, highest);
      return highest;
      // if (maxWithOpen > maxNoOpen) {
      //   memo.set(`${valve}-${minute}`, maxWithOpen);
      //   return maxWithOpen;
      // } else {
      //   memo.set(`${valve}-${minute}`, maxNoOpen);
      //   return maxNoOpen;
      // }
    }
  }
  // if (found) return found;
  

  // const tunnelVals = tunnels.map((tunnel: string): number => {
  //   const nextArg: CalcArg = {
  //     valve: tunnel,
  //     valveGraph,
  //     memo,
  //     minute: minute - 1,
  //     open: new Set(open),
  //   };

  //   return findMaxPressure(nextArg);
  // });

  // const maxNoOpen = max(tunnelVals);

  // if (open.has(valve)) {

    // memo.set(`${valve}-${minute}`, maxNoOpen);
    // // const biggest = memo.get(`${valve}-${minute}`);
    // // if (!biggest) throw ' everythings broken ';

    // return maxNoOpen;
  // } else {
    // let maxWithOpen;
    //   const nextOpen = new Set(open);
    //   nextOpen.add(valve);
  
    //   const tunnelValsWithOpen = tunnels.map((tunnel: string): number => {
    //     const nextArg: CalcArg = {
    //       valve: tunnel,
    //       valveGraph,
    //       memo,
    //       minute: minute - 2,
    //       open: nextOpen,
    //     };
  
    //     return flow * (minute - 1) + findMaxPressure(nextArg);
    //   });
  
    //   maxWithOpen = max(tunnelValsWithOpen);
    // }


  //   if (maxWithOpen > maxNoOpen) {
  //     memo.set(`${valve}-${minute }`, maxWithOpen);
  //     return maxWithOpen;
  //   } else {
  //     memo.set(`${valve}-${minute}`, maxNoOpen);
  //     return maxNoOpen;
  //   }
  // }
  
};

const part1 = async (): Promise<void> => {
  // const input = await toArray('day16.txt', '\n');
  const input = await toArray('day16test.txt', '\n');
  const valveGraph = buildValveGraph(input);
  const memo = new Map<string, number>();
  
  // const maxFromStart = Object.keys(valveGraph).map((valveGraphKey: string): number => {
  //   const startArg: CalcArg = {
  //     valve: valveGraphKey,
  //     valveGraph,
  //     open: new Set<string>(),
  //     memo,
  //     minute: 30,
  //   };

  //   return findMaxPressure(startArg);
  // });

  // return max(maxFromStart);
  // const maxFromStart = Object.keys(valveGraph).map((valveGraphKey: string): number => {
    const startArg: CalcArg = {
      valve: 'DD',
      valveGraph,
      open: new Set<string>(),
      memo,
      minute: 30,
    };

    // return findMaxPressure(startArg);
  // });

  // return max(maxFromStart);
};
const part2 = async (): Promise<void> => {};

export {
  part1,
  part2,
};

