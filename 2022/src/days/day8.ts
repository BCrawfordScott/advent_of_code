import { max, toArray } from "../helpers";
const DELTA_N = [-1, 0];
const DELTA_S = [1, 0];
const DELTA_E = [0, 1];
const DELTA_W = [0, -1];

const DELTAS = [
  DELTA_N,
  DELTA_W,
  DELTA_S,
  DELTA_E,
];

const analyzeRows = (input: string[][], allVisible: Set<string>): void => {
  let greatest: number;
  for (let i = 0; i < input.length; i++) {
    greatest = -1;
    for (let j = 0; j < input[i].length; j++) {
      const element = parseInt(input[i][j]);
      if (element > greatest) {
        allVisible.add(`${i},${j}`);
        greatest = element;
      }
    }
    greatest = -1;
    for (let j = input[i].length - 1; j >= 0; j--) {
      const element = parseInt(input[i][j]);
      if (element > greatest) {
        allVisible.add(`${i},${j}`);
        greatest = element;
      }
    }
  }
}
const analyzeColumns = (input: string[][], allVisible: Set<string>): void => {
  let greatest: number;
  for (let j = 0; j < input[0].length; j++) {
    greatest = -1;
    for (let i = 0; i < input.length; i++) {
      const element = parseInt(input[i][j]);
      if (element > greatest) {
        allVisible.add(`${i},${j}`);
        greatest = element;
      }
    }
    greatest = -1;
    for (let i = input.length - 1; i >= 0; i--) {
      const element = parseInt(input[i][j]);
      if (element > greatest) {
        allVisible.add(`${i},${j}`);
        greatest = element;
      }
    }
  }
}

const part1 = async (): Promise<number> => {
  const input = (await toArray('day8.txt', '\n')).map((row: string) => row.split(''));
  const allVisible = new Set<string>();

  analyzeRows(input, allVisible);
  analyzeColumns(input, allVisible);

  return allVisible.size;
};

const analyzeDeltas = (input: string[][], i: number, j: number): number[] => {
  const posHeight = parseInt(input[i][j]);
  return DELTAS.map((delta: number[]): number => {
    const [dx, dy] = delta;
    let [x, y] = [i + dx, j + dy];
    let total = 0;
    let end = false;
    while (!end && (x >= 0 && x < input.length) && ( y >= 0 && y < input[0].length)) {
      let current = parseInt(input[x][y]);
      if (current >= posHeight) {
        total = total + 1;
        end = true
      } else {
        total = total + 1;
        x = x + dx;
        y = y + dy;
      }
    };
    return total;
  })
}

const part2 = async (): Promise<number> => {
  const input = (await toArray('day8.txt', '\n')).map((row: string) => row.split(''));
  const scenicScores = new Map<string, number>();
  for (let i = 0; i < input.length; i++) {
    for (let j = 0; j < input[0].length; j++) {
      const score = analyzeDeltas(input, i, j);
      const totalScore = score.reduce((product: number, current: number): number => product * current);

      scenicScores.set(`${i},${j}`, totalScore);
    }    
  }

  return max(Array.from(scenicScores.values()));
};

export {
  part1,
  part2,
};