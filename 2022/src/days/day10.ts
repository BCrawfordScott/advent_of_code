import { toArray } from "../helpers";

const NOOP_REGEX = /noop/;
const ADDX_REGEX = /addx/;

const CHECK_CYCLES = new Set<number>([20, 60, 100, 140, 180, 220])

const analyze = (cycle: number, x: number, totals: number[]): void => {
  if (CHECK_CYCLES.has(cycle)) {
    totals.push(x * cycle);
  }
};

const draw = (cycle: number, x: number, canvas: string[][]): void => {
  const row = Math.floor(cycle / 40);
  const column = cycle % 40;
  const pixel = (column === x || column === x - 1 || column === x + 1) ? '#' : ' ';

  canvas[row][column] = pixel;
}


const part1 = async (): Promise<number> => {
  const input = await toArray('day10.txt', '\n');
  let cycle = 0;
  let x = 1;
  const totals: number[] = [];
  input.forEach((command: string): void => {
    if (command.match(NOOP_REGEX)) {
      cycle = cycle + 1;
      analyze(cycle, x, totals);
    }

    if (command.match(ADDX_REGEX)) {
      cycle = cycle + 1;
      analyze(cycle, x, totals);
      cycle = cycle + 1;
      analyze(cycle, x, totals);
      x = x + parseInt(command.split(' ')[1]);
    }
  })

  return totals.reduce((sum: number, current: number): number => sum + current);
};

const part2 = async (): Promise<string> => {
  const input = await toArray('day10.txt', '\n');
  let cycle = 0;
  let x = 1;
  const canvas = [
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    [],
  ];
  input.forEach((command: string): void => {
    if (command.match(NOOP_REGEX)) {
      cycle = cycle + 1;
      draw(cycle - 1, x, canvas);
    }

    if (command.match(ADDX_REGEX)) {
      cycle = cycle + 1;
      draw(cycle - 1, x, canvas);
      cycle = cycle + 1;
      draw(cycle - 1, x, canvas);
      x = x + parseInt(command.split(' ')[1]);
    }
  });

  const final = canvas.map((row) => row.join('')).join('\n');
  return '\n' + final;
};

export {
  part1,
  part2,
};
