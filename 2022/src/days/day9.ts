import { max, toArray } from "../helpers"

class Node {
  pos: number[];
  next: Node | null;
  id: string | number;

  constructor(pos: number[], next: Node | null, id: string | number) {
    this.pos = pos;
    this.next = next;
    this.id = id;
  }

  stringPos(): string {
    return this.pos.join(',');
  }

  posDiff(checkNode: Node) {
    const [px, py] = this.pos;
    const [cx, cy] = checkNode.pos;

    return [px - cx, py - cy];
  };
}

const print = (headNode: Node, maximum: number): void => {
  const positions = new Map<number[], number|string>();
  let current: Node | null = headNode;

  while (current) {
    positions.set(current.pos, current.id);
    current = current.next;
  }

  const rows: string[][] = [];

  for (let index = 0; index < maximum * 2; index++) {
    const periods = '.'.repeat(maximum * 2).split('');
    rows.push(periods);
  }

  positions.forEach((value, key) => {
    const [x, y] = key;
    if (rows[x][y] === '.') rows[x][y] = value.toString();
  });

  if (rows[maximum][maximum] === '.') rows[maximum][maximum] = 's';

  console.log(rows.map((row) => row.join(' ')).join('\n'));
  console.log('\n')
}

const DELTA_MAP: Record<string, number[]> = {
  U: [-1, 0],
  D: [1, 0],
  L: [0, -1],
  R: [0, 1],
};

const ADJACENT_DELTAS = [
  [1, 0],
  [-1, 0],
  [0, -1],
  [0, 1],
  [-1, -1],
  [1, -1],
  [-1, 1],
  [1, 1],
];

const COMMAND_REGEX = /([UDLR]) (\d+)/;

const isAdjacent = (head: number[], tail: number[]): boolean => {
  const [hx, hy] = head;
  const [tx, ty] = tail;
  if (hx === tx && hy === ty) return true;

  return ADJACENT_DELTAS.some((delta): boolean => {
    const [dx, dy] = delta;
    const testX = dx + hx;
    const testY = dy + hy;

    return (tx === testX && ty === testY)
  })
};

const part1 = async (): Promise<number> => {
  const input = await toArray('day9.txt', '\n');
  let head = [0, 0];
  let tail = [0, 0];
  const tailVisited = new Set<string>();
  tailVisited.add(tail.join(','));

  input.forEach((command: string): void => {
    const match = command.match(COMMAND_REGEX);
    if (!match) return;

    const [_, direction, steps] = match;
    const [dx, dy] = DELTA_MAP[direction];

    for (let step = 0; step < parseInt(steps); step++) {
      const prevHead = head;
      head = [head[0] + dx, head[1] + dy];
      const adjacent = isAdjacent(head, tail);

      if (!adjacent) {
        tail = prevHead;
        tailVisited.add(tail.join(','))
      } 
    }
  });
  return tailVisited.size;
}
const part2 = async (): Promise<number> => {
  const input = await toArray('day9.txt', '\n');
  const maximum = max(input.map((command) => parseInt(command.split(' ')[1])));
  const head = new Node([maximum, maximum], null, 'H');
  const tail = new Node([maximum, maximum], null, 9);
  let current = head;
  for (let num = 0; num < 8; num++) {
    const nextNode = new Node([maximum, maximum], null, num + 1);
    current.next = nextNode;
    current = nextNode;
  }
  current.next = tail;
  const tailVisited = new Set<string>();
  tailVisited.add(tail.stringPos());

  input.forEach((command: string): void => {
    const match = command.match(COMMAND_REGEX);
    if (!match) return;

    const [_, direction, steps] = match;
    const [dx, dy] = DELTA_MAP[direction];

    for (let step = 0; step < parseInt(steps); step++) {
      head.pos = [head.pos[0] + dx, head.pos[1] + dy];
      let current = head;
      while (current.next) {
        const node1 = current;
        const node2 = current.next;

        const adjacent = isAdjacent(node1.pos, node2.pos);
        if (!adjacent) {
          let [sx, sy] = node1.posDiff(node2)
          if (sx > 1 || sx < -1) sx = sx / 2;
          if (sy > 1 || sy < -1) sy = sy / 2;

          const [px, py] = node2.pos;
          node2.pos = [px + sx, py + sy];
        }

        current = current.next;
      }
      
      tailVisited.add(tail.stringPos());
      // print(head, maximum)
    }
  });
  return tailVisited.size;
}

export {
  part1,
  part2,
}