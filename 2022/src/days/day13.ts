import { toArray } from "../helpers";

const checkOrder = (left: any[], right: any[]): number => {
  const longest: number = left.length > right.length ? left.length : right.length;

  for (let i = 0; i < longest; i++) {
    const l = left[i];
    const r = right[i];

    if (typeof l === 'number' && typeof r === 'number') {
      if (l < r) return -1;
      if (l > r) return 1;
    } else if (typeof l === 'object' && typeof r === 'object') {
      const test = checkOrder(l, r);
      if (test !== 0) return test;
    } else if (typeof l === 'number' && typeof r === 'object') {
      const test = checkOrder([l], r);
      if (test !== 0) return test;
    } else if (typeof l === 'object' && typeof r === 'number') {
      const test = checkOrder(l, [r]);
      if (test !== 0) return test;;
    } else if (!l && r) {
      return -1;
    } else if (l && !r) {
      return 1;
    }
  }

  return 0;
}

const part1 = async (): Promise<number> => {
  const input = await toArray('day13.txt', '\n\n');
  const correctOrder = new Set<number>();

  input.forEach((pair: string, idx: number): void => {
    const [left, right] = pair.split('\n');
    const lPacket = JSON.parse(left);
    const rPacket = JSON.parse(right);
    const ordered = checkOrder(lPacket, rPacket);

    if (ordered === -1) correctOrder.add(idx + 1);

  })

  return Array.from(correctOrder).reduce((total, next) => total + next);
};

const part2 = async (): Promise<number> => {
  const input = await toArray('day13.txt', '\n\n');
  const packets = input.reduce((parsed: any[], pair: string): any[] => {
    const [left, right] = pair.split('\n');
    parsed.push(JSON.parse(left));
    parsed.push(JSON.parse(right));
    return parsed;
  }, [])

  const two = [[2]];
  const six = [[6]];

  const fewertwo = packets.reduce((total: number, packet: any[]) => checkOrder(packet, two) === -1 ? total + 1 : total, 1)
  const fewersix = packets.reduce((total: number, packet: any[]) => checkOrder(packet, six) === -1 ? total + 1 : total, 1)

  return fewertwo * (fewersix + 1)
};

export {
  part1,
  part2,
};
