import { toArray } from "../helpers";

const part1 = async (): Promise<number> => {
  const input = await toArray('day6.txt', '');
  let start = 0;
  let end = 4;
  let found = false;
  while (!found && end < input.length) {
    const window = new Set(input.slice(start, end));
    if (window.size === 4) found = true;

    end = end + 1;
    start = start + 1;
  };

  return end - 1;
};
const part2 = async (): Promise<number> => {
  const input = await toArray('day6.txt', '');
  let start = 0;
  let end = 14;
  let found = false;
  while (!found && end < input.length) {
    const window = new Set(input.slice(start, end));
    if (window.size === 14) found = true;

    end = end + 1;
    start = start + 1;
  };

  return end - 1;
};

export {
  part1,
  part2,
};
