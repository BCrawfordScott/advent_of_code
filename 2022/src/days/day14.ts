import { toArray } from "../helpers";

const SAND_DELTAS = [
  [0, 1],
  [-1, 1],
  [1, 1],
];

const buildRockPositions = (input: string[]): [Set<string>, number] => {
  const positions = new Set<string>();
  let maxY: number = -Infinity;

  input.forEach((instruction: string): void => {
    const points = instruction.split(' -> ');
    let last: string|null = null;

    points.forEach((point: string): void => {
      if (last === null) {
        last = point;
      } else {
        const [cx, cy] = last.split(',').map((str: string): number => parseInt(str));
        const [nx, ny] = point.split(',').map((str: string): number => parseInt(str));
        if (cy > maxY) maxY = cy;
        if (ny > maxY) maxY = ny;
        
        if (cx === nx) {
          const [s, e] = [cy, ny].sort((a, b) => a - b);
          for (let y = s; y <= e; y++) {
            positions.add(`${cx},${y}`);
          }
        }
        if (cy === ny) {
          const [s, e] = [cx, nx].sort();
          for (let x = s; x <= e; x++) {
            positions.add(`${x},${cy}`);
          }
        }

        last = point;
      }
    })
  });

  return [positions, maxY];
}

const part1 = async (): Promise<number> => {
  const input = await toArray('day14.txt', '\n');
  const [rockPositions, maxY]: [Set<string>, number] = buildRockPositions(input);
  const sandPositions = new Set<string>();
  let full = false;

  while (!full) {
    let sand = [500, 0];
    while (!sandPositions.has(sand.join(','))) {
      const [sx, sy] = sand;
      if (sy > maxY) {
        sandPositions.add(sand.join(','));
        full = true;
      } else {
        const cannotMove = SAND_DELTAS.map((delta): boolean => {
          const [dx, dy] = delta;
          const nx = sx + dx;
          const ny = sy + dy;

          return rockPositions.has(`${nx},${ny}`) || sandPositions.has(`${nx},${ny}`);
        });

        if (cannotMove.every((check) => check)) {
          sandPositions.add(sand.join(','));
        } else {
          const delta = SAND_DELTAS[(cannotMove.findIndex((check) => !check))];
          const [dx, dy] = delta;
          const nx = sx + dx;
          const ny = sy + dy;

          sand = [nx, ny];
        }
      }
    }
  }

  return sandPositions.size - 1;

};

const part2 = async (): Promise<number> => {
  const input = await toArray('day14.txt', '\n');
  const [rockPositions, maxY]: [Set<string>, number] = buildRockPositions(input);
  const sandPositions = new Set<string>();

  while (!sandPositions.has('500,0')) {
    let sand = [500, 0];
    while (!sandPositions.has(sand.join(','))) {
      const [sx, sy] = sand;
      if (sy === maxY + 1) {
        sandPositions.add(sand.join(','));
      } else {
        const cannotMove = SAND_DELTAS.map((delta): boolean => {
          const [dx, dy] = delta;
          const nx = sx + dx;
          const ny = sy + dy;

          return rockPositions.has(`${nx},${ny}`) || sandPositions.has(`${nx},${ny}`);
        });

        if (cannotMove.every((check) => check)) {
          sandPositions.add(sand.join(','));
        } else {
          const delta = SAND_DELTAS[(cannotMove.findIndex((check) => !check))];
          const [dx, dy] = delta;
          const nx = sx + dx;
          const ny = sy + dy;

          sand = [nx, ny];
        }
      }
    }
  }

  return sandPositions.size;
};

export {
  part1,
  part2,
};
