import { toArray } from "../helpers";

const manahattanDistance = (point1: number[], point2: number[]): number => {
  const [x1, y1] = point1;
  const [x2, y2] = point2;

  return Math.abs(x1 - x2) + Math.abs(y1 - y2);
};

const INPUT_PATTERN = /Sensor at x=(.+), y=(.+): closest beacon is at x=(.+), y=(.+)/;

class Sensor {
  sensor: number[];
  beacon: number[];
  maxDistance: number;
  minY: number;
  maxY: number;
  minX: number;
  maxX: number;

  constructor(sensor: number[], beacon: number[]) {
    this.sensor = sensor;
    this.beacon = beacon;
    this.maxDistance = manahattanDistance(sensor, beacon);
    this.minX = sensor[0] - this.maxDistance;
    this.maxX = sensor[0] + this.maxDistance;
    this.minY = sensor[1] - this.maxDistance;
    this.maxY = sensor[1] + this.maxDistance;
  }

  mDistanceFrom(point: number[]): number {
    return manahattanDistance(this.sensor, point);
  }
};

const part1 = async (): Promise<number> => {
  const input = await toArray('day15.txt', '\n');
  const sensors = input.map((line: string): Sensor => {
    const match = line.match(INPUT_PATTERN);
    if (!match) throw 'REGEX didnt work';

    const [_line, sx, sy, bx, by] = match;

    return new Sensor(
      [parseInt(sx), parseInt(sy)],
      [parseInt(bx), parseInt(by)],
    );
  });

  const beaconPositions = new Set(sensors.map((sensor): string => sensor.beacon.join(',')));

  const y2 = 2000000;
  const impossiblePositions = new Set<number>()

  sensors.forEach((sensor: Sensor) => {
    if (y2 >= sensor.minY && y2 <= sensor.maxY) {
      const { maxDistance, sensor: pos } = sensor;
      const [sx, sy] = pos;
      const x2op1 = maxDistance - Math.abs(sy - y2) + sx
      const x2op2 = (maxDistance - Math.abs(sy - y2) - sx) * -1
      const [start, end] = [x2op1, x2op2].sort((a, b) => a - b);

      for (let x = start; x <= end; x++) {
        if (!beaconPositions.has(`${x},${y2}`)) impossiblePositions.add(x);
      }
    }
  });

  return impossiblePositions.size;
};


const part2 = async (): Promise<BigInt> => {
  const input = await toArray('day15.txt', '\n');
  const sensors = input.map((line: string): Sensor => {
    const match = line.match(INPUT_PATTERN);
    if (!match) throw 'REGEX didnt work';

    const [_line, sx, sy, bx, by] = match;

    return new Sensor(
      [parseInt(sx), parseInt(sy)],
      [parseInt(bx), parseInt(by)],
    );
  });

  let adjacentSensors: null|Sensor[] = null;

  for (let i = 0; i < sensors.length - 1; i++) {
    for (let j = i + 1; j < sensors.length; j++) {
      if (!adjacentSensors) {
        const sensor1 = sensors[i];
        const sensor2 = sensors[j];
        
        if ((sensor1.mDistanceFrom(sensor2.sensor) - sensor1.maxDistance - sensor2.maxDistance) == 2) {
          adjacentSensors = [sensor1, sensor2]
        }
      }
    } 
  }

  if (!adjacentSensors) throw 'Couldnt find adjacent sensors';
  const [sensor1, sensor2] = adjacentSensors;
  const { sensor: [s1x, s1y] } = sensor1;
  const { sensor: [s2x, s2y] } = sensor2;

  const possiblePositions = new Set<string>()
  const found: number[][] = [];

  let minY = sensor1.minY < 0 ? 0 : sensor1.minY;
  let maxY = sensor1.maxY > 4000000 ? 4000000 : sensor1.maxY;
  for (let y = minY; y <= maxY; y++) {
    const x2op1 = (sensor1.maxDistance + 1) - Math.abs(s1y - y) + s1x
    const x2op2 = ((sensor1.maxDistance + 1) - Math.abs(s1y - y) - s1x) * -1

    possiblePositions.add(`${x2op1},${y}`);
    possiblePositions.add(`${x2op2},${y}`);
  }

  minY = sensor2.minY < 0 ? 0 : sensor2.minY;
  maxY = sensor2.maxY > 4000000 ? 4000000 : sensor2.maxY;

  for (let y = minY; y <= maxY; y++) {
    const x2op1 = (sensor2.maxDistance + 1) - Math.abs(s2y - y) + s2x
    const x2op2 = ((sensor2.maxDistance + 1) - Math.abs(s2y - y) - s2x) * -1

    if (possiblePositions.has(`${x2op1},${y}`)) found.push([x2op1, y]);
    if (possiblePositions.has(`${x2op2},${y}`)) found.push([x2op2, y]);
  }

  const final = found.find((potential) => {
    return sensors.every(sensor => sensor.mDistanceFrom(potential) > sensor.maxDistance)
  });

  if (!final) throw 'Couldnt find final'
  const [fx, fy] = final;
  
  return BigInt(fx) * BigInt(4000000) + BigInt(fy);
};

export {
  part1,
  part2,
};


