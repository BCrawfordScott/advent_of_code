import { toArray } from "../helpers";

type Node = {
  value: number,
  pos: number[],
  shortest: number,
};

const ADJ_DELTAS = [
  [1, 0],
  [-1, 0],
  [0, 1],
  [0, -1],
];

const buildMap = (lines: string[]): [number[], number[], Map<string, Node>] => {
  const charLines = lines.map((line: string) => line.split(''));
  let start: number[] = [-1, -1];
  let end: number[] = [-1, -1];
  const numMap = new Map<string, Node>();
  charLines.forEach((line: string[], i: number): void => {
     line.forEach((char: string, j: number): void => {
      const pos = `${i},${j}`;
      if (char === 'S') {
        start = [i, j];
        const value = 'a'.charCodeAt(0);
        numMap.set(pos, { pos: [i, j], value, shortest: Infinity });
      } else if (char === 'E') {
        end = [i, j];
        const value = 'z'.charCodeAt(0);
        numMap.set(pos, { pos: [i, j], value, shortest: Infinity })
      } else {
        const value = char.charCodeAt(0);
        numMap.set(pos, { pos: [i, j], value, shortest: Infinity })
      }
    });
  });

  return [start, end, numMap];
};

const toKey = (pos: number[]): string => pos.join(',');

const getAdjacentPathNodes = (currentNode: Node, map: Map<string, Node>, visited: Set<Node>): Node[] => {
  const { pos } = currentNode;
  const allAdjacents = ADJ_DELTAS.map((delta: number[]): Node | undefined => {
    const [dx, dy] = delta;
    const [cx, cy] = pos;
    const key = toKey([cx + dx, cy + dy]);
    return map.get(key);
  });

  return allAdjacents.filter((node: Node | undefined): node is Node => {
    if (node === undefined) return false;
    if (visited.has(node)) return false;
    const { value } = node;
    return currentNode.value >= value || currentNode.value === value - 1;
  });
}

const getAdjacentPathNodesPart2 = (currentNode: Node, map: Map<string, Node>, visited: Set<Node>): Node[] => {
  const { pos } = currentNode;
  const allAdjacents = ADJ_DELTAS.map((delta: number[]): Node | undefined => {
    const [dx, dy] = delta;
    const [cx, cy] = pos;
    const key = toKey([cx + dx, cy + dy]);
    return map.get(key);
  });

  return allAdjacents.filter((node: Node | undefined): node is Node => {
    if (node === undefined) return false;
    if (visited.has(node)) return false;
    const { value } = node;
    return currentNode.value <= value || currentNode.value === value + 1;
  });
}

const part1 = async (): Promise<number|undefined> => {
  const lines = await toArray('day12.txt', '\n');
  const [start, end, map] = buildMap(lines);
  const visited = new Set<Node>()
  const unvisited = new Set<Node>(map.values())


  const startNode = map.get(toKey(start));
  const endNode = map.get(toKey(end));
  if (!startNode) throw 'Broken from the start'
  if (!endNode) throw 'Broken from the end'
  startNode.shortest = 0;
  const startAdjacents = getAdjacentPathNodes(startNode, map, visited);
  startAdjacents.forEach((node: Node) => {
    node.shortest = startNode.shortest + 1;
  });

  unvisited.delete(startNode);
  visited.add(startNode);

  let currentNode: Node|null = Array.from(unvisited).sort((a:Node, b:Node) => a.shortest - b.shortest)[0];

  while (currentNode) {
    const adjacentNodes = getAdjacentPathNodes(currentNode, map, visited);
    adjacentNodes.forEach((node: Node) => {
      const currentDistance = currentNode ? currentNode.shortest + 1 : -1;
      if (node.shortest > currentDistance) node.shortest = currentDistance;
    });
    unvisited.delete(currentNode);
    visited.add(currentNode);

    currentNode = Array.from(unvisited).sort((a: Node, b: Node) => a.shortest - b.shortest)[0];
    if (visited.has(endNode)) {
      currentNode = null;
    }
  }

  return map.get(toKey(end))?.shortest;
};
const part2 = async (): Promise<number> => {
  const lines = await toArray('day12.txt', '\n');
  const [start, end, map] = buildMap(lines);
  const visited = new Set<Node>()
  const unvisited = new Set<Node>(map.values())


  const startNode = map.get(toKey(end));
  const endNode = map.get(toKey(start));
  if (!startNode) throw 'Broken from the start'
  if (!endNode) throw 'Broken from the end'
  startNode.shortest = 0;
  const startAdjacents = getAdjacentPathNodesPart2(startNode, map, visited);
  startAdjacents.forEach((node: Node) => {
    node.shortest = startNode.shortest + 1;
  });

  unvisited.delete(startNode);
  visited.add(startNode);

  let currentNode: Node | null = Array.from(unvisited).sort((a: Node, b: Node) => a.shortest - b.shortest)[0];

  while (currentNode) {
    const adjacentNodes = getAdjacentPathNodesPart2(currentNode, map, visited);
    adjacentNodes.forEach((node: Node) => {
      const currentDistance = currentNode ? currentNode.shortest + 1 : -1;
      if (node.shortest > currentDistance) node.shortest = currentDistance;
    });
    unvisited.delete(currentNode);
    visited.add(currentNode);

    currentNode = Array.from(unvisited).sort((a: Node, b: Node) => a.shortest - b.shortest)[0];
    // if (visited.has(endNode)) {
    //   currentNode = null;
    // }
  }

  const aCode = 'a'.charCodeAt(0);
  const shortestA = Array.from(map.values())
                         .filter((node: Node) => node.value === aCode)
                         .sort((a:Node, b:Node) => a.shortest - b.shortest)[0]

  return shortestA.shortest;
};

export {
  part1,
  part2,
};
