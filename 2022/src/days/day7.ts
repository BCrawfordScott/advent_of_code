import { toArray, min } from "../helpers";

const DIR_REG = /dir (.+)/;
const FILE_REG = /(\d+) (.+)/;
const CD_REG = /\$ cd (.+)/;
const LS_REG = /\$ ls/;

type TreeHelper = [(string | null), Record<string, string[]>]

const getDirSize = (dir: string, tree: Record<string, (string)[]>): number => {
  const elements = tree[dir];
  return elements.reduce((total: number, element: string): number => {
    const current = parseInt(element);
    if (current) {
      return total + current;
    } else {
      return total + getDirSize(element, tree);
    }
  }, 0);
}

const calculateTotals = (finalTree: Record<string, string[]>): number[] => {
 return Object.keys(finalTree).map((key: string) => { return getDirSize(key, finalTree) });
};

const buildTree = (input: string[]): Record<string, string[]> => {
  const tree: Record<string, string[]> = {};
  const [_finalDir, finalTree] = input.reduce((current: TreeHelper, command: string) => {
    const [dir, treeProgress] = current;
    if (command.match(CD_REG)) {
      const match = command.match(CD_REG);
      if (match) {
        const [_, next] = match;
        if (next === '..' && dir) {
          const newDir = dir.split('/');
          newDir.pop();
          const middle: TreeHelper = [newDir.join('/'), treeProgress];
          return middle;
        }
        if (next && !treeProgress[`${dir}/${next}`]) treeProgress[`${dir}/${next}`] = [];
        const middle: TreeHelper = [`${dir}/${next}`, treeProgress]
        if (next) return middle;
      }
      return current;
    } else if (command.match(LS_REG)) {
      return current;
    } else if (command.match(DIR_REG)) {
      const match = command.match(DIR_REG);
      if (match) {
        const [_, newDir] = match;
        if (dir) treeProgress[dir].push(`${dir}/${newDir}`);
      }
      const middle: TreeHelper = [dir, treeProgress]
      return middle;
    } else if (command.match(FILE_REG)) {
      const match = command.match(FILE_REG);
      if (match) {
        const [_, file] = match;
        if (dir) treeProgress[dir].push(file);
      }
      const middle: TreeHelper = [dir, treeProgress]
      return middle;
    } else {
      return current;
    }
  }, [null, tree]);

  return finalTree;
}

const part1 = async (): Promise<number> => {
  const input = await toArray('day7.txt', '\n');
  const finalTree = buildTree(input);

  const totals = calculateTotals(finalTree);
  return totals.reduce((total: number, size: number): number => {
    return size <= 100000 ? total + size : total;
  }, 0);
};
const part2 = async (): Promise<number> => {
  const input = await toArray('day7.txt', '\n');
  const finalTree = buildTree(input);
  const totals = calculateTotals(finalTree);
  const freeSpace = 70000000 - totals[0];
  const options = totals.slice(1).filter((size: number): boolean => freeSpace + size >= 30000000);
  return min(options);
};

export {
  part1,
  part2,
};
