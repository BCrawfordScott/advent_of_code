{:ok, raw} = File.read('./inputs.txt')
input = String.split(raw, "\n")


input
|> Enum.reject(fn str -> String.match?(str, ~r/ab|cd|pq|xy/) end)
|> Enum.filter(fn str -> String.match?(str, ~r/aa|bb|cc|dd|ee|ff|gg|hh|ii|jj|kk|ll|mm|nn|oo|pp|qq|rr|ss|tt|uu|vv|ww|xx|yy|zz/) end)
|> Enum.filter(fn str -> String.match?(str, ~r/[aeiou].*[aeiou].*[aeiou]/) end)
|> length
|> IO.puts

input
|> Enum.filter(fn str -> String.match?(str, ~r/a.{1}a|b.{1}b|c.{1}c|d.{1}d|e.{1}e|f.{1}f|g.{1}g|h.{1}h|i.{1}i|j.{1}j|k.{1}k|l.{1}l|m.{1}m|n.{1}n|o.{1}o|p.{1}p|q.{1}q|r.{1}r|s.{1}s|t.{1}t|u.{1}u|v.{1}v|w.{1}w|x.{1}x|y.{1}y|z.{1}z/) end)
|> Enum.filter(fn str -> String.match?(str, ~r/(\w{2}).*?(\1)/) end)
|> length
|> IO.puts
