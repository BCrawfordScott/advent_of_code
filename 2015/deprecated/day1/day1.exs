{:ok, input} = File.read("./input.txt")

steps = input |> String.split("")

defmodule FindFloor do

  def calc(steps, count \\ 0, idx \\ 0)

  def calc(steps, -1, idx) do
    idx
  end

  def calc(["(" | tail], count, idx) do
    calc(tail, count + 1, idx + 1)
  end

  def calc([")" | tail], count, idx) do
    calc(tail, count - 1, idx + 1)
  end

  def calc([head | tail], count, idx) do
    calc(tail, count, idx)
  end

  def calc([], count, idx) do
    count
  end
end

IO.puts(FindFloor.calc(steps))
