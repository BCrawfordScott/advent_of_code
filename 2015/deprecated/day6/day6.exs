defmodule LightShow do
  def process(input) do
    IO.puts(input)
    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn str -> LightShow.build_instruction(str) end)
  end

  def setup(list, map \\ %MapSet{})
  def setup([], set) do
    set
  end
  def setup([{:+, {xs, ys}, {xe, ye}} | tail], set) do
    xs..xe
    |> Enum.each(fn x ->
      ys..ye
      |> Enum.each(fn y ->
        set = MapSet.put(set, [x, y])
      end)
    end)

    setup(tail, set)
  end
  def setup([{:-, {xs, ys}, {xe, ye}} | tail], set) do
    new_set = xs..xe
    |> Enum.reduce(%MapSet{}, fn -> |
      ys..ye
      |> Enum.reduce(%MapSet{}, fn -> )
    end)
    
    
    # xs..xe
    # |> Enum.each(fn x ->
    #   ys..ye
    #   |> Enum.each(fn y ->
    #     set = MapSet.delete(set, [x, y])
      end)
    end)

    setup(tail, set)
  end
  def setup([{:<>, {xs, ys}, {xe, ye}} | tail], set) do
    xs..xe
    |> Enum.each(fn x ->
      ys..ye
      |> Enum.each(fn y ->
        set = if(MapSet.member?(set, [x, y]), do: MapSet.delete(set, [x, y]), else: MapSet.put(set, [x, y]))
      end)
    end)

    setup(tail, set)
  end

  def build_instruction("turn on " <> orders) do
    [start, endpoint] = process_orders(orders)
    {:+, start, endpoint}
  end

  def build_instruction("turn off " <> orders) do
    [start, endpoint] = process_orders(orders)
    {:-, start, endpoint}
  end

  def build_instruction("toggle " <> orders) do
    [start, endpoint] = process_orders(orders)
    {:<>, start, endpoint}
  end

  def process_orders(orders) do
    orders
    |> String.split(" through ")
    |> Enum.map(fn coords ->
      coords
      |> String.split(",")
      |> Enum.map(fn int -> String.to_integer(int) end)
      |> List.to_tuple
    end)
  end
end

{:ok, input} = File.read('./input.txt')
LightShow.process(input)
|> LightShow.setup
|> MapSet.to_list
|> length
|> IO.puts
