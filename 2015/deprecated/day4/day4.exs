# input = "ckczppom"

defmodule AdventCoin do
  @input "ckczppom"

  def mine(num, hash \\ "0")
  def mine(num, "000000" <> _rest) do
    num
  end
  def mine(num, _hash) do
    # :crypto.hash(:md5, @input <> Integer.to_string(num)) |> IO.inspect
    mine(num + 1, :crypto.hash(:md5, @input <> Integer.to_string(num + 1)) |> Base.encode16)
  end
end

0
|> AdventCoin.mine
|> IO.puts
