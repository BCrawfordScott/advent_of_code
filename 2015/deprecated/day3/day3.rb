
input = File.open("./input.txt").read.split("")

class Santa

    attr_reader :list
    attr_accessor :robo, :santa

    def initialize(robo = nil)
        @robo = robo ? [0,0] : robo
        @santa = [0,0]
        @list = Hash.new(0)
        list[santa] += 1
    end

    def travel(dir)
        case dir
        when "^"
            x, y = santa
            @santa = [x + 1, y]
            list[santa] += 1
        when ">"
            x, y = santa
            @santa = [x, y + 1]
            list[santa] += 1
        when "v"
            x, y = santa
            @santa = [x - 1, y]
            list[santa] += 1
        when "<"
            x, y = santa
            @santa = [x, y - 1]
            list[santa] += 1
        else
            return
        end
    end

    def robo_travel(dir)
        case dir
        when "^"
            x, y = robo
            @robo = [x + 1, y]
            list[robo] += 1
        when ">"
            x, y = robo
            @robo = [x, y + 1]
            list[robo] += 1
        when "v"
            x, y = robo
            @robo = [x - 1, y]
            list[robo] += 1
        when "<"
            x, y = robo
            @robo = [x, y - 1]
            list[robo] += 1
        else
            return
        end
    end

    def route(directions)
        if robo
            directions.each_with_index do |dir, idx|
                if idx.even?
                    travel(dir)
                else
                    robo_travel(dir)
                end
            end
            
        else
            directions.each do |dir|
                travel(dir)
            end
        end 

        self
    end

    # def robo_route(directions)
    #     directions.each_with_index do |dir, idx|
    #         if idx.even?
    #             travel(dir)
    #         else
    #             robo_travel(dir)
    #         end
    #     end
    # end

    def count
        list.length
    end
end

puts Santa.new().route(input).count
puts Santa.new(true).route(input).count