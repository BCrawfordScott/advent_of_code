{:ok, raw} = File.read('./input.txt')
input = String.split(raw, "", trim: true)

defmodule Houses do
  def travel([], _coor, set) do
    set
  end
  def travel(["^" | tail], [x, y], set) do
    coor = [x + 1, y]
    set = MapSet.put(set, coor)
    travel(tail, coor, set)
  end
  def travel([">" | tail] , [x, y], set) do
    coor = [x + 1, y]
    set = MapSet.put(set, coor)
    travel(tail, coor, set)
  end
  def travel(["v" | tail], [x, y], set) do
    coor = [x + 1, y]
    set = MapSet.put(set, coor)
    travel(tail, coor, set)
  end
  def travel(["<" | tail], [x, y], set) do
    coor = [x + 1, y]
    set = MapSet.put(set, coor)
    travel(tail, coor, set)
  end

  def travel([], _santa, _robo, _idx, set) do
    set
  end
  def travel(["^" | tail], santa, robo, idx, set) do
    [x, y] = santa
    coor = [x + 1, y]
    set = MapSet.put(set, coor)
    robo_travel(tail, coor, robo, idx + 1, set)
  end
  def travel([">" | tail], santa, robo, idx, set) do
    [x, y] = santa
    coor = [x, y + 1]
    set = MapSet.put(set, coor)
    robo_travel(tail, coor, robo, idx + 1, set)
  end
  def travel(["v" | tail], santa, robo, idx, set) do
    [x, y] = santa
    coor = [x - 1, y]
    set = MapSet.put(set, coor)
    robo_travel(tail, coor, robo, idx + 1, set)
  end
  def travel(["<" | tail], santa, robo, idx, set) do
    [x, y] = santa
    coor = [x, y - 1]
    set = MapSet.put(set, coor)
    robo_travel(tail, coor, robo, idx + 1, set)
  end

  def robo_travel([], _santa, _robo, _idx, set) do
    set
  end
  def robo_travel(["^" | tail], santa, robo, idx, set) do
    [x, y] = robo
    coor = [x + 1, y]
    set = MapSet.put(set, coor)
    travel(tail, santa, coor, idx + 1, set)
  end
  def robo_travel([">" | tail], santa, robo, idx, set) do
    [x, y] = robo
    coor = [x, y + 1]
    set = MapSet.put(set, coor)
    travel(tail, santa, coor, idx + 1, set)
  end
  def robo_travel(["v" | tail], santa, robo, idx, set) do
    [x, y] = robo
    coor = [x - 1, y]
    set = MapSet.put(set, coor)
    travel(tail, santa, coor, idx + 1, set)
  end
  def robo_travel(["<" | tail], santa, robo, idx, set) do
    [x, y] = robo
    coor = [x, y - 1]
    set = MapSet.put(set, coor)
    travel(tail, santa, coor, idx + 1, set)
  end
end

input
|> Houses.travel([0, 0], MapSet.new())
|> MapSet.size
|> IO.puts

input
|> Houses.travel([0, 0], [0, 0], 1, MapSet.new())
|> MapSet.size
|> IO.puts
