def run
    input = File.open("./input.txt").read.split("\n")

    total = 0

    input.reduce(0) do |acc, string|
        l, w, h = string.split("x").map { |el| el.to_i }
        area = 2*l*w + 2*w*h + 2*h*l
        ex = [l*w, w*h, h*l].min
        puts area + ex
        area + ex + acc
    end
end

puts run