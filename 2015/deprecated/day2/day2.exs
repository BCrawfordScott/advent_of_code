{:ok, input} = File.read("./input.txt")

wrapping = fn (input) ->
  String.split(input, "\n")
  |> Enum.map(fn str -> String.split(str,"x") end)
  |> Enum.map(fn [l, w, h] ->
    [l, w, h] = [String.to_integer(l), String.to_integer(w), String.to_integer(h)]
    area = 2*l*w + 2*w*h + 2*h*l
    min = Enum.min([l*w, w*h, h*l])
    area + min
  end)
  |> Enum.reduce(fn x, acc -> x + acc end)
end

ribbon = fn(input) ->
  input
  |> String.split("\n")
  |> Enum.map(fn str -> String.split(str,"x") end)
  |> Enum.map(fn [l, w, h] ->
    [l, w, h] = [String.to_integer(l), String.to_integer(w), String.to_integer(h)]
    volume = l*w*h
    max = Enum.max([l, w, h])
    ex = 2*l + 2*w + 2*h - 2 * max
    volume + ex
  end)
  |> Enum.reduce(fn x, acc -> x + acc end)
end


IO.puts(wrapping.(input))
IO.puts(ribbon.(input))
defmodule Input do
  def to_int([]) do
    []
  end

  def to_int([head|tail]) do
    [String.to_integer(head) | to_int(tail)]
  end

  def find_smallest_area({a, b, c}) do
    if(a < b, do: if(b < c, do: a * b, else: a * c), else: b * c)
  end

  def convert(list) do
    # IO.inspect(list)
    new_tuple = List.to_tuple(list)
    {new_tuple, Input.find_smallest_area(new_tuple)}
  end

  def process([]) do
    []
  end

  def process([head | tail]) do
    dims = head
      |> String.split("x")
      |> Input.to_int
      |> Input.convert

    [dims | process(tail)]
  end

  def prepare(input) do
    input
    |> String.split("\n")
    |> Input.process
  end
end

# prepared = Input.prepare(input)

# IO.inspect(prepared)

defmodule Wrap do
  def tally([]) do
    []
  end
  def tally([{{h, l, w}, ex} | tail]) do
    total = (2*l*w) + (2*w*h) + (2*h*l) + ex
    [total | tally(tail)]
  end

  def add([]) do
    0
  end
  def add([head | tail]) do
    IO.puts(head)
    head + add(tail)
  end

  def compute(list) do
    IO.inspect(length(list))
    IO.inspect(length(tally(list)))
    list
    |> tally
    |> add
  end
end

# IO.puts(Wrap.compute(prepared))
