defmodule Aoc2015.Day9 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  # def part1, do: part1(@test_file)
  def part1(path) do
    path
    |> parse_input()
    |> build_vertex_map()
    |> get_distances()
    |> Enum.min()
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> build_vertex_map()
    |> get_distances()
    |> Enum.max()
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp build_vertex_map(input) do
    input
    |> Enum.reduce(%{}, fn vertex, acc ->
      {v1, v2, distance} = vertex |> String.replace(~r/\sto\s|\s=\s/, " ") |> String.split(" ") |> List.to_tuple()
      nodes = [v1, v2] |> Enum.sort() |> List.to_tuple()
      Map.put(acc, nodes, String.to_integer(distance))
    end)
  end

  defp get_distances(vertex_map) do
    vertex_map
      |> Map.keys()
      |> Enum.flat_map(&Tuple.to_list/1)
      |> Enum.uniq()
      |> permutations()
      |> Enum.map(&sum_distances(&1, vertex_map))
  end

  defp permutations([]), do: [[]]
  defp permutations(list) do
    for h <- list, t <- permutations(list -- [h]), do: [h | t]
  end

  defp sum_distances(path, vertex_map), do: sum_distances(path, vertex_map, 0)
  defp sum_distances([_v | []], _vertex_map, total), do: total
  defp sum_distances([v1 | [v2 | tail]], vertex_map, total) do
    key = [v1, v2] |> Enum.sort() |> List.to_tuple()
    sum_distances([v2 | tail], vertex_map, total + Map.get(vertex_map, key))
  end
end
