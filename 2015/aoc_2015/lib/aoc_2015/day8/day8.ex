defmodule Aoc2015.Day8 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  @p1_pattern ~r/\\\\|\\"|\\x[\w\d][\w\d]/
  @p2_pattern ~r/\\|"|\\x/

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> get_lengths()
    |> Enum.reduce(0, &length_diff/2)
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> get_encoded_lengths()
    |> Enum.reduce(0, &length_diff/2)
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp get_lengths(input) do
    input |> Enum.map(fn string -> {String.length(string), (Regex.replace(@p1_pattern, string, "!") |> String.length()) - 2} end)
  end

  defp length_diff({code_len, mem_len}, acc) do
    acc + (code_len - mem_len)
  end

  defp get_encoded_lengths(input) do
    input |> Enum.map(fn string ->
      {
        (String.length(string) + (Regex.scan(@p2_pattern, string) |> length())) + 2,
        String.length(string)
      }
    end)
  end
end
