defmodule Aoc2015.Day3 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> get_unique_homes
    |> MapSet.size()
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> get_homes_with_robo()
    |> MapSet.size()
  end

  defp parse_input(path) do
    path
    |> Input.to_array("")
  end

  defp get_unique_homes(directions), do: get_unique_homes(directions, MapSet.new([{0, 0}]), {0, 0})
  defp get_unique_homes([], visits, _curr_pos), do: visits
  defp get_unique_homes([dir | tail], visits, {x, y}) do
    case dir do
      "v" ->
        next = {x, y - 1}
        get_unique_homes(tail, MapSet.put(visits, next), next)
      "^" ->
        next = {x, y + 1}
        get_unique_homes(tail, MapSet.put(visits, next), next)
      "<" ->
        next = {x - 1, y}
        get_unique_homes(tail, MapSet.put(visits, next), next)
      ">" ->
        next = {x + 1, y}
        get_unique_homes(tail, MapSet.put(visits, next), next)
    end
  end

  defp get_homes_with_robo(directions) do
    santa = [nil | directions] |> Enum.drop_every(2)
    robo = directions |> Enum.drop_every(2)

    MapSet.union(
      get_unique_homes(santa, MapSet.new([{0, 0}]), {0, 0}),
      get_unique_homes(robo, MapSet.new([{0, 0}]), {0, 0})
    )
  end
end
