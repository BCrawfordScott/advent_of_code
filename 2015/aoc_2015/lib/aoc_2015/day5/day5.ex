defmodule Aoc2015.Day5 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> count_nice_strings()
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> count_nice_strings_2()
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp count_nice_strings(strings) do
    strings
    |> Stream.reject(fn string -> String.match?(string, ~r/ab|cd|pq|xy/) end)
    |> Stream.filter(fn string -> String.match?(string, ~r/aa|bb|cc|dd|ee|ff|gg|hh|ii|jj|kk|ll|mm|nn|oo|pp|qq|rr|ss|tt|uu|vv|ww|xx|yy|zz/) end)
    |> Stream.filter(fn string -> String.match?(string, ~r/[aeiou].*[aeiou].*[aeiou]/) end)
    |> Enum.count()
  end

  defp count_nice_strings_2(strings) do
    strings
    |> Stream.filter(fn string -> String.match?(string, ~r/a.{1}a|b.{1}b|c.{1}c|d.{1}d|e.{1}e|f.{1}f|g.{1}g|h.{1}h|i.{1}i|j.{1}j|k.{1}k|l.{1}l|m.{1}m|n.{1}n|o.{1}o|p.{1}p|q.{1}q|r.{1}r|s.{1}s|t.{1}t|u.{1}u|v.{1}v|w.{1}w|x.{1}x|y.{1}y|z.{1}z/) end)
    |> Stream.filter(fn string -> String.match?(string, ~r/(\w{2}).*?(\1)/) end)
    |> Enum.count()
  end
end
