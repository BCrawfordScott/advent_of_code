defmodule Aoc2015.Day6 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> Enum.map(&tuple_instruction/1)
    |> hang_lights()
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> Enum.map(&tuple_instruction/1)
    |> adjust_lights()
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp tuple_instruction(instruction) do
    case instruction do
      "turn on " <> rest -> {:+, extract_coords(rest)}
      "turn off " <> rest -> {:-, extract_coords(rest)}
      "toggle " <> rest -> {:^, extract_coords(rest)}
    end
  end

  defp extract_coords(string) do
    [start | [ending | []]] = String.split(string, " through ", trim: true)
    {
      String.split(start, ",", trim: true) |> Enum.map(&String.to_integer/1) |> List.to_tuple(),
      String.split(ending, ",", trim: true) |> Enum.map(&String.to_integer/1) |> List.to_tuple()
    }
  end

  defp hang_lights(instructions), do: hang_lights(instructions, MapSet.new)
  defp hang_lights([], alight), do: MapSet.size(alight)
  defp hang_lights([{:+, {{sx, sy}, {ex, ey}}} | rest], alight) do
    next = sx..ex |> Enum.reduce(alight, fn x, acc1 ->
      sy..ey |> Enum.reduce(acc1, fn y, acc2 ->
        MapSet.put(acc2, {x, y})
      end)
    end)

    hang_lights(rest, next)
  end
  defp hang_lights([{:-, {{sx, sy}, {ex, ey}}} | rest], alight) do
    next = sx..ex |> Enum.reduce(alight, fn x, acc1 ->
      sy..ey |> Enum.reduce(acc1, fn y, acc2 ->
        MapSet.delete(acc2, {x, y})
      end)
    end)

    hang_lights(rest, next)
  end
  defp hang_lights([{:^, {{sx, sy}, {ex, ey}}} | rest], alight) do
    next = sx..ex |> Enum.reduce(alight, fn x, acc1 ->
      sy..ey |> Enum.reduce(acc1, fn y, acc2 ->
        case MapSet.member?(acc2, {x, y}) do
          true -> MapSet.delete(acc2, {x, y})
          false -> MapSet.put(acc2, {x, y})
        end
      end)
    end)

    hang_lights(rest, next)
  end

  defp adjust_lights(instructions), do: adjust_lights(instructions, Map.new)
  defp adjust_lights([], lights), do: Map.values(lights) |> Enum.sum()
  defp adjust_lights([{:+, {{sx, sy}, {ex, ey}}} | rest], lights) do
    next_lights = sx..ex |> Enum.reduce(lights, fn x, acc1 ->
      sy..ey |> Enum.reduce(acc1, fn y, acc2 ->
        Map.update(acc2, {x, y}, 1, fn val -> val + 1 end)
      end)
    end)

    adjust_lights(rest, next_lights)
  end
  defp adjust_lights([{:-, {{sx, sy}, {ex, ey}}} | rest], lights) do
    next_lights = sx..ex |> Enum.reduce(lights, fn x, acc1 ->
      sy..ey |> Enum.reduce(acc1, fn y, acc2 ->
        Map.update(acc2, {x, y}, 0, fn val ->
          case val > 0 do
            true -> val - 1
            false -> 0
          end
        end)
      end)
    end)

    adjust_lights(rest, next_lights)
  end
  defp adjust_lights([{:^, {{sx, sy}, {ex, ey}}} | rest], lights) do
    next_lights = sx..ex |> Enum.reduce(lights, fn x, acc1 ->
      sy..ey |> Enum.reduce(acc1, fn y, acc2 ->
        Map.update(acc2, {x, y}, 2, fn val -> val + 2 end)
      end)
    end)

    adjust_lights(rest, next_lights)
  end
end
