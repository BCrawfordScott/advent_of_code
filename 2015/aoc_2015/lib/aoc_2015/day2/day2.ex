defmodule Aoc2015.Day2 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> get_total_sqr_feet()
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> get_total_ribbon()
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp get_total_sqr_feet(dimensions_list) do
    dimensions_list
    |> Enum.reduce(0, fn dimensions, acc ->
      acc + get_sqr_feet(dimensions)
    end)
  end

  defp get_sqr_feet(dimensions) do
    dimensions
    |> String.split("x")
    |> Enum.map(&String.to_integer/1)
    |> calc_feet()
  end

  defp calc_feet([l, w, h]) do
    areas = [l * w, w * h, h * l]
    Enum.reduce(areas, Enum.min(areas), fn area, acc -> acc + (2 * area) end)
  end

  defp get_total_ribbon(dimensions_list) do
    dimensions_list
    |> Enum.reduce(0, fn dimensions, acc ->
      acc + get_ribbon_length(dimensions)
    end)
  end

  defp get_ribbon_length(dimensions) do
    dimensions
    |> String.split("x")
    |> Enum.map(&String.to_integer/1)
    |> calc_ribbon()
  end

  defp calc_ribbon(sides) do
    (Enum.sum(sides) - Enum.max(sides)) * 2 + Enum.product(sides)
  end

end
