defmodule Aoc2015.Day7 do
  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> map_input()
    |> find_value("a")
    |> Tuple.to_list()
    |> List.first()
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    clean_map = path |> parse_input() |> map_input()
    a = find_value(clean_map, "a") |> Tuple.to_list() |> List.first()
    next_map = Map.put(clean_map, "b", a)
    find_value(next_map, "a") |> Tuple.to_list() |> List.first()
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp map_input(input) do
    input
    |> Enum.reduce(Map.new, fn process, acc ->
      { supply, circuit } = String.split(process, " -> ", trim: true) |> List.to_tuple()
      Map.put(acc, circuit, supply)
    end)
  end

  defp find_value(processes, target) do
    case Map.get(processes, target) do
      supply when is_integer(supply) -> { supply, processes }
      supply -> find_value(calc_circuit(target, supply, processes), target)
    end
  end

  defp calc_circuit(target, supply, processes) do
    cond do
      String.match?(supply, ~r/AND/) -> process_and(target, supply, processes)
      String.match?(supply, ~r/OR/) -> process_or(target, supply, processes)
      String.match?(supply, ~r/NOT/) -> process_not(target, supply, processes)
      String.match?(supply, ~r/LSHIFT/) -> process_lshift(target, supply, processes)
      String.match?(supply, ~r/RSHIFT/) -> process_rshift(target, supply, processes)
      String.match?(supply, ~r/^\d*$/) -> Map.put(processes, target, String.to_integer(supply))
      true ->
        { val, updated_processes } = find_value(processes, supply)
        Map.put(updated_processes, target, val)
    end
  end

  defp process_and(target, supply, processes) do
    { c1, c2 } = String.split(supply, " AND ", trim: true) |> List.to_tuple()
    { v1, current_map1 } = parse_value(processes, c1)
    { v2, current_map2 } = parse_value(current_map1, c2)
    Map.put(current_map2, target, Bitwise.band(v1, v2))
  end

  defp process_or(target, supply, processes) do
    { c1, c2 } = String.split(supply, " OR ", trim: true) |> List.to_tuple()
    { v1, current_map1 } = parse_value(processes, c1)
    { v2, current_map2 } = parse_value(current_map1, c2)
    Map.put(current_map2, target, Bitwise.bor(v1, v2))
  end

  defp process_not(target, supply, processes) do
    c1 = String.split(supply, "NOT ", trim: true) |> List.last()
    { v1, current_map1 } = parse_value(processes, c1)
    Map.put(current_map1, target, 65536 + Bitwise.bnot(v1))
  end

  defp process_lshift(target, supply, processes) do
    { c1, c2 } = String.split(supply, " LSHIFT ", trim: true) |> List.to_tuple()
    { v1, current_map1 } = parse_value(processes, c1)
    Map.put(current_map1, target, Bitwise.bsl(v1, String.to_integer(c2)))
  end

  defp process_rshift(target, supply, processes) do
    { c1, c2 } = String.split(supply, " RSHIFT ", trim: true) |> List.to_tuple()
    { v1, current_map1 } = parse_value(processes, c1)
    Map.put(current_map1, target, Bitwise.bsr(v1, String.to_integer(c2)))
  end

  defp parse_value(processes, val) do
    cond do
      Map.has_key?(processes, val) -> find_value(processes, val)
      true -> { String.to_integer(val), processes }
    end
  end
end
