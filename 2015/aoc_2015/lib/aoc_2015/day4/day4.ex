defmodule Aoc2015.Day4 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> mine5()
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> mine6()
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
    |> List.first()
  end

  defp mine5(key), do: mine5(key, "", 0)
  defp mine5(_key, "00000" <> _rest, nonce), do: nonce
  defp mine5(key, _hash, nonce) do
    mine5(key, :crypto.hash(:md5, key <> Integer.to_string(nonce + 1)) |> Base.encode16, nonce + 1)
  end

  defp mine6(key), do: mine6(key, "", 0)
  defp mine6(_key, "000000" <> _rest, nonce), do: nonce
  defp mine6(key, _hash, nonce) do
    mine6(key, :crypto.hash(:md5, key <> Integer.to_string(nonce + 1)) |> Base.encode16, nonce + 1)
  end
end
