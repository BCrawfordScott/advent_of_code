defmodule Aoc2015.Day1 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> step()
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> step_count()
  end

  defp parse_input(path) do
    path
    |> Input.to_array("")
  end

  defp step(instructions), do: step(instructions, 0)
  defp step([], floor), do: floor
  defp step(["(" | tail], floor), do: step(tail, floor + 1)
  defp step([")" | tail], floor), do: step(tail, floor - 1)

  defp step_count(instructions), do: step_count(instructions, 0, 0)
  defp step_count(_tail, -1, count), do: count
  defp step_count([], _floor, _count), do: "Whoops! Shouldn't have made it to the end"
  defp step_count(["(" | tail], floor, count), do: step_count(tail, floor + 1, count + 1)
  defp step_count([")" | tail], floor, count), do: step_count(tail, floor - 1, count + 1)
end
