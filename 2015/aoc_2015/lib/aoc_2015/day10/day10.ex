defmodule Aoc2015.Day10 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  @num_count_pattern ~r/(\d)\1*/

  def part1, do: part1(@input_file)
  # def part1, do: part1(@test_file)
  def part1(path) do
    path
    |> parse_input()
    |> List.first()
    |> look_and_say(40)
    |> String.length()
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> List.first()
    |> look_and_say(50)
    |> String.length()
  end

  defp look_and_say(string, 0), do: string
  defp look_and_say(string, number) do
    Regex.scan(@num_count_pattern, string)
    |> Enum.reduce("", fn [str | [char | []]], acc ->
      acc <> Integer.to_string(String.length(str)) <> char
    end)
    |> look_and_say(number - 1)
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end
end
