defmodule Aoc2015.DayTemplate do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    "Pending..."
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    "Pending..."
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end
end
