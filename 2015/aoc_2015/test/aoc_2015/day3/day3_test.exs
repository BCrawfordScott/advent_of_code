defmodule Aoc2015.Day3Test do
  use ExUnit.Case
  doctest Aoc2015.Day3

  import Aoc2015.Day3, only: [
    part1: 1,
    part2: 1,
  ]

  @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  test "has an input file" do
    assert(File.exists?(File.cwd! <> "/lib/aoc_2015/day3/input.txt"))
  end

  test "part 1 delivers the right output" do
    assert(part1(@test_file) == 2)
  end

  test "part 2 delivers the right output" do
    assert(part2(@test_file) == 11)
  end
end
