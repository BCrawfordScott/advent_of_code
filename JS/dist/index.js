"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const twenty15 = __importStar(require("./2015"));
function TwentyFifteen() {
    return __awaiter(this, void 0, void 0, function* () {
        const d1p1 = yield twenty15.Day1.part1();
        const d1p2 = yield twenty15.Day1.part2();
        const d2p1 = yield twenty15.Day2.part1();
        const d2p2 = yield twenty15.Day2.part2();
        const d3p1 = yield twenty15.Day3.part1();
        const d3p2 = yield twenty15.Day3.part2();
        // Uncomment to run - 10 - 15 sec of processing
        // const d4p1 = await twenty15.Day4.part1();
        // const d4p2 = await twenty15.Day4.part2();
        const d4p1 = 117946;
        const d4p2 = 3938038;
        const d5p1 = yield twenty15.Day5.part1();
        const d5p2 = yield twenty15.Day5.part2();
        const d6p1 = yield twenty15.Day6.part1();
        const d6p2 = yield twenty15.Day6.part2();
        console.log('2015 Day 1 part 1: ', d1p1);
        console.log('2015 Day 1 part 2: ', d1p2);
        console.log('2015 Day 2 part 1: ', d2p1);
        console.log('2015 Day 2 part 2: ', d2p2);
        console.log('2015 Day 3 part 1: ', d3p1);
        console.log('2015 Day 3 part 2: ', d3p2);
        console.log('2015 Day 4 part 1: ', d4p1);
        console.log('2015 Day 4 part 2: ', d4p2);
        console.log('2015 Day 5 part 1: ', d5p1);
        console.log('2015 Day 5 part 2: ', d5p2);
        console.log('2015 Day 6 part 1: ', d6p1);
        console.log('2015 Day 6 part 2: ', d6p2);
    });
}
TwentyFifteen();
