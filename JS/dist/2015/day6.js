"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const INPUT = (0, helpers_1.toArray)('2015/day6.txt', '\n');
const COMMAND_PATTERN = /on|off|toggle/;
const POSITION_PATTERN = /\d{1,3}/g;
const applyBrightness = (lights, command, position) => {
    const currentValue = lights.get(position) || 0;
    switch (command) {
        case 'toggle':
            lights.set(position, currentValue + 2);
            return lights;
        case 'on':
            lights.set(position, currentValue + 1);
            return lights;
        case 'off':
            lights.set(position, currentValue === 0 ? 0 : currentValue - 1);
            return lights;
        default:
            return lights;
    }
};
const applyCommand = (lights, command, position) => {
    switch (command) {
        case 'toggle':
            lights.has(position) ? lights.delete(position) : lights.add(position);
            return lights;
        case 'on':
            lights.add(position);
            return lights;
        case 'off':
            lights.delete(position);
            return lights;
        default:
            return lights;
    }
};
const applyInstructions = (lights, instruction) => {
    const command = instruction.match(COMMAND_PATTERN);
    const stringPositions = instruction.match(POSITION_PATTERN);
    const positions = stringPositions === null || stringPositions === void 0 ? void 0 : stringPositions.map((pos) => parseInt(pos));
    if (positions) {
        const [x1, y1, x2, y2] = positions;
        for (let x = x1; x <= x2; x++) {
            for (let y = y1; y <= y2; y++) {
                const currentPosition = [x, y].join(',');
                applyCommand(lights, command ? command[0] : '', currentPosition);
            }
        }
    }
    return lights;
};
const applyBrightnessInstructions = (lights, instruction) => {
    const command = instruction.match(COMMAND_PATTERN);
    const stringPositions = instruction.match(POSITION_PATTERN);
    const positions = stringPositions === null || stringPositions === void 0 ? void 0 : stringPositions.map((pos) => parseInt(pos));
    if (positions) {
        const [x1, y1, x2, y2] = positions;
        for (let x = x1; x <= x2; x++) {
            for (let y = y1; y <= y2; y++) {
                const currentPosition = [x, y].join(',');
                applyBrightness(lights, command ? command[0] : '', currentPosition);
            }
        }
    }
    return lights;
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield INPUT;
    const lights = new Set();
    const finishedLights = input.reduce(applyInstructions, lights);
    return finishedLights.size;
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield INPUT;
    const lights = new Map();
    const finishedLights = input.reduce(applyBrightnessInstructions, lights);
    return [...finishedLights.values()].reduce((sum, next) => sum + next);
});
exports.part2 = part2;
