"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const crypto_js_1 = __importDefault(require("crypto-js"));
const INPUT = 'ckczppom';
const part1 = () => {
    let found = false;
    let nonce = 0;
    while (!found) {
        const hash = crypto_js_1.default.MD5(INPUT + nonce);
        if (hash.toString().match(/^00000.*/)) {
            found = true;
        }
        else {
            ++nonce;
        }
    }
    return nonce;
};
exports.part1 = part1;
const part2 = () => {
    let found = false;
    let nonce = 0;
    while (!found) {
        const hash = crypto_js_1.default.MD5(INPUT + nonce);
        if (hash.toString().match(/^000000.*/)) {
            found = true;
        }
        else {
            ++nonce;
        }
    }
    return nonce;
};
exports.part2 = part2;
