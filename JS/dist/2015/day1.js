"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('2015/day1.txt', '');
    return input.reduce((total, paren) => {
        if (paren === '(')
            return total + 1;
        if (paren === ')')
            return total - 1;
        return total;
    }, 0);
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield (0, helpers_1.toArray)('2015/day1.txt', '');
    const totals = [...input];
    const runningTotals = totals.reduce((running, paren, idx) => {
        const val = paren === '(' ? 1 : -1;
        if (idx === 0)
            return [val];
        running.push(val + running[idx - 1]);
        return running;
    }, []);
    return runningTotals.indexOf(-1) + 1;
});
exports.part2 = part2;
