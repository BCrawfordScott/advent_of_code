"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const INPUT = (0, helpers_1.toArray)('2015/day2.txt', '\n');
// const testInput: string[] = ['2x3x4', '1x1x10'];
const toDimensions = (dString) => {
    const [ls, ws, hs] = dString.split('x');
    return {
        l: parseInt(ls),
        w: parseInt(ws),
        h: parseInt(hs),
    };
};
const calcSquareFeet = ({ l, w, h }) => {
    const sides = [l * w, w * h, h * l];
    const smallestSide = (0, helpers_1.min)(sides);
    return sides.reduce((total, side) => total + (side * 2), 0) + smallestSide;
};
const calcRibbon = ({ l, w, h }) => {
    const longest = (0, helpers_1.max)([l, w, h]);
    const bow = l * w * h;
    const ribbonLength = ((l * 2) + (w * 2) + (h * 2) - (longest * 2));
    return bow + ribbonLength;
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield INPUT;
    return input.reduce((total, dString) => {
        return total + calcSquareFeet(toDimensions(dString));
    }, 0);
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield INPUT;
    return input.reduce((total, dString) => {
        return total + calcRibbon(toDimensions(dString));
    }, 0);
});
exports.part2 = part2;
