"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const INPUT = (0, helpers_1.toArray)('2015/day5.txt', '\n');
const p1r1 = /[aeiou].*[aeiou].*[aeiou]/;
const p1r2 = /aa|bb|cc|dd|ee|ff|gg|hh|ii|jj|kk|ll|mm|nn|oo|pp|qq|rr|ss|tt|uu|vv|ww|xx|yy|zz/;
const p1r3 = /ab|cd|pq|xy/;
const isNice = (string) => {
    return !!string.match(p1r1) && !!string.match(p1r2) && !string.match(p1r3);
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield INPUT;
    return input.reduce((total, string) => {
        return isNice(string) ? total + 1 : total;
    }, 0);
});
exports.part1 = part1;
const p2r1 = /\w{1}.{1}(\1)/;
const p2r2 = /(\w{2}).*?(\1)/;
const isNice2 = (string) => {
    return !!string.match(p2r1) && !!string.match(p2r2);
};
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield INPUT;
    return input.reduce((total, string) => {
        return isNice2(string) ? total + 1 : total;
    }, 0);
});
exports.part2 = part2;
