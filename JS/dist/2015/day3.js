"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
// @ts-ignore - Input is guaranteed to be Dirs with this filepath
const INPUT = (0, helpers_1.toArray)('2015/day3.txt', '');
const START = '0,0';
const DIR_MAP = {
    '>': [1, 0],
    '^': [0, 1],
    'v': [0, -1],
    '<': [-1, 0],
};
const applyDirection = ([x, y], dir) => {
    const [dx, dy] = DIR_MAP[dir];
    return [x + dx, y + dy];
};
const part1 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield INPUT;
    const vistied = new Set([START]);
    input.reduce((location, dir) => {
        const nextLocation = applyDirection((0, helpers_1.parseCoor)(location), dir);
        const stringCoor = (0, helpers_1.processCoor)(nextLocation);
        vistied.add(stringCoor);
        return stringCoor;
    }, START);
    return vistied.size;
});
exports.part1 = part1;
const part2 = () => __awaiter(void 0, void 0, void 0, function* () {
    const input = yield INPUT;
    const visited = new Set([START]);
    const starts = { santa: START, robo: START };
    input.reduce(({ santa, robo }, dir, idx) => {
        if ((idx % 2) === 0) {
            const nextLocation = applyDirection((0, helpers_1.parseCoor)(santa), dir);
            const stringCoor = (0, helpers_1.processCoor)(nextLocation);
            visited.add(stringCoor);
            return { santa: stringCoor, robo };
        }
        else {
            const nextLocation = applyDirection((0, helpers_1.parseCoor)(robo), dir);
            const stringCoor = (0, helpers_1.processCoor)(nextLocation);
            visited.add(stringCoor);
            return { santa, robo: stringCoor };
        }
    }, starts);
    return visited.size;
});
exports.part2 = part2;
