import * as twenty15 from './2015';

async function TwentyFifteen(): Promise<void> {
  const d1p1 = await twenty15.Day1.part1();
  const d1p2 = await twenty15.Day1.part2();
  const d2p1 = await twenty15.Day2.part1();
  const d2p2 = await twenty15.Day2.part2();
  const d3p1 = await twenty15.Day3.part1();
  const d3p2 = await twenty15.Day3.part2();
  // Uncomment to run - 10 - 15 sec of processing
  // const d4p1 = await twenty15.Day4.part1();
  // const d4p2 = await twenty15.Day4.part2();
  const d4p1 = 117946;
  const d4p2 = 3938038;
  const d5p1 = await twenty15.Day5.part1();
  const d5p2 = await twenty15.Day5.part2();
  const d6p1 = await twenty15.Day6.part1();
  const d6p2 = await twenty15.Day6.part2();
  
  console.log('2015 Day 1 part 1: ', d1p1);
  console.log('2015 Day 1 part 2: ', d1p2);
  console.log('2015 Day 2 part 1: ', d2p1);
  console.log('2015 Day 2 part 2: ', d2p2);
  console.log('2015 Day 3 part 1: ', d3p1);
  console.log('2015 Day 3 part 2: ', d3p2);
  console.log('2015 Day 4 part 1: ', d4p1);
  console.log('2015 Day 4 part 2: ', d4p2);
  console.log('2015 Day 5 part 1: ', d5p1);
  console.log('2015 Day 5 part 2: ', d5p2);
  console.log('2015 Day 6 part 1: ', d6p1);
  console.log('2015 Day 6 part 2: ', d6p2);
}

TwentyFifteen();