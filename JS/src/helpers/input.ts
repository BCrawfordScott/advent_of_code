import * as dotenv from 'dotenv';
import { readFile } from 'node:fs/promises';

dotenv.config();

export type Dir = '>' | '^' | 'v' | '<';
type InputPromise = Promise<Dir[] | string[]>

const { ABSOLUTE_PATH } = process.env;

/** PRIVATE FUNCTIONS */

async function read(path: string): Promise<Buffer> {
  return await readFile(ABSOLUTE_PATH + path);
}

/** PUBLIC FUNCTIONS */

export async function toArray(path: string, splitter: string): InputPromise {
  const buffer = await read(path);
  return buffer.toString()
               .split(splitter);
}
