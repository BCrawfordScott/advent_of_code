export const min = (array: number[]): number => array.reduce((least: number, current: number) => current < least ? current : least);

export const max = (array: number[]): number => array.reduce((least: number, current: number) => current > least ? current : least);
