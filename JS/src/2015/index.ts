export * as Day1 from './day1';
export * as Day2 from './day2';
export * as Day3 from './day3';
export * as Day4 from './day4';
export * as Day5 from './day5';
export * as Day6 from './day6';
