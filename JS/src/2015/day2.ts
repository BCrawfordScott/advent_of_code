import { max, min, toArray } from '../helpers';

type Dimensions = {
  l: number,
  w: number,
  h: number,
};

const INPUT: Promise<string[]> = toArray('2015/day2.txt', '\n');
// const testInput: string[] = ['2x3x4', '1x1x10'];

const toDimensions = (dString: string): Dimensions => {
  const [ls, ws, hs] = dString.split('x');
  return {
    l: parseInt(ls),
    w: parseInt(ws),
    h: parseInt(hs),
  }
};

const calcSquareFeet = ({ l, w, h }: Dimensions): number => {
  const sides = [l * w, w * h, h * l];
  const smallestSide = min(sides);

  return sides.reduce((total: number, side: number) => total + (side * 2), 0) + smallestSide;
};

const calcRibbon = ({ l, w, h }: Dimensions): number => {
  const longest = max([l, w, h]);
  const bow = l * w * h;
  const ribbonLength = ((l * 2)  + (w * 2) + (h * 2) - (longest * 2));

  return bow + ribbonLength;
}

const part1 = async (): Promise<number> => {
  const input = await INPUT;

  return input.reduce((total: number, dString: string): number =>{
    return total + calcSquareFeet(toDimensions(dString));
  }, 0);
};

const part2 = async (): Promise<number> => {
  const input = await INPUT;

  return input.reduce((total: number, dString: string): number =>{
    return total + calcRibbon(toDimensions(dString));
  }, 0);
};

export {
  part1,
  part2,
}