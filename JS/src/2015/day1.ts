import { toArray } from '../helpers';

const part1 = async (): Promise<number> => {
  const input = await toArray('2015/day1.txt', '');
  return input.reduce((total: number, paren: string): number => {
    if (paren === '(') return total + 1;
    if (paren === ')') return total - 1;
    return total;
  }, 0);
};

const part2 = async (): Promise<number> => {
  const input = await toArray('2015/day1.txt', '');
  const totals = [...input];
  const runningTotals = totals.reduce((running: number[], paren: string, idx: number) => {
    const val = paren === '(' ? 1 : -1;
    if (idx === 0) return [val];
    running.push(val + running[idx - 1]);
    return running;
  }, []);

  return runningTotals.indexOf(-1) + 1;
}

export {
  part1,
  part2,
};
