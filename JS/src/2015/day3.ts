import { Dir, parseCoor, processCoor, toArray } from '../helpers';

// @ts-ignore - Input is guaranteed to be Dirs with this filepath
const INPUT: Promise<Dir[]> = toArray('2015/day3.txt', '');
const START = '0,0';

const DIR_MAP = {
  '>': [1, 0],
  '^': [0, 1],
  'v': [0, -1],
  '<': [-1, 0],
}

const applyDirection = ([x, y]: number[], dir: Dir): number[] => {
  const [dx, dy] = DIR_MAP[dir];
  return [x + dx, y + dy];
}

const part1 = async (): Promise<number> => {
  const input: Dir[] = await INPUT;
  const vistied = new Set([START]);

  input.reduce((location: string, dir: Dir): string => {
    const nextLocation = applyDirection(parseCoor(location) , dir);
    const stringCoor = processCoor(nextLocation);
    vistied.add(stringCoor);

    return stringCoor;
  }, START)

  return vistied.size;
}

type Starts = {
  santa: string,
  robo: string,
}

const part2 = async (): Promise<number> => {
  const input: Dir[] = await INPUT;
  const visited = new Set([START]);
  const starts = { santa: START, robo: START }

  input.reduce(({santa, robo}: Starts, dir: Dir, idx: number): Starts => {
    if ((idx % 2) === 0) {
      const nextLocation = applyDirection(parseCoor(santa) , dir);
      const stringCoor = processCoor(nextLocation);
      visited.add(stringCoor);

      return { santa: stringCoor, robo };
    } else {
      const nextLocation = applyDirection(parseCoor(robo), dir);
      const stringCoor = processCoor(nextLocation);
      visited.add(stringCoor);

      return { santa, robo: stringCoor };
    }


  }, starts)

  return visited.size;
}

export {
  part1,
  part2,
};
