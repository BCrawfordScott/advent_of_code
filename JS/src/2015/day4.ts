import CryptoJS from 'crypto-js';

const INPUT = 'ckczppom';

const part1 = (): number => {
  let found = false;
  let nonce = 0;
  while (!found) {
    const hash = CryptoJS.MD5(INPUT + nonce)
    if (hash.toString().match(/^00000.*/)) {
      found = true;
    } else {
      ++nonce;
    }
  }

  return nonce;
}
const part2 = (): number => {
  let found = false;
  let nonce = 0;
  while (!found) {
    const hash = CryptoJS.MD5(INPUT + nonce)
    if (hash.toString().match(/^000000.*/)) {
      found = true;
    } else {
      ++nonce;
    }
  }

  return nonce;
}

export {
  part1,
  part2,
};
