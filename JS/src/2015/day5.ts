import { toArray } from '../helpers';

const INPUT: Promise<string[]> = toArray('2015/day5.txt', '\n');
const p1r1 = /[aeiou].*[aeiou].*[aeiou]/;
const p1r2 = /aa|bb|cc|dd|ee|ff|gg|hh|ii|jj|kk|ll|mm|nn|oo|pp|qq|rr|ss|tt|uu|vv|ww|xx|yy|zz/;
const p1r3 = /ab|cd|pq|xy/;

const isNice = (string: string): boolean => {
  return !!string.match(p1r1) && !! string.match(p1r2) && !string.match(p1r3);
};

const part1 = async (): Promise<number> => {
  const input = await INPUT;

  return input.reduce((total: number, string: string) => {
    return isNice(string) ? total + 1 : total;
  }, 0)
};

const p2r1 = /\w{1}.{1}(\1)/
const p2r2 = /(\w{2}).*?(\1)/;

const isNice2 = (string: string): boolean => {
  return !!string.match(p2r1) && !!string.match(p2r2);
}

const part2 = async (): Promise<number> => {
  const input = await INPUT;

  return input.reduce((total: number, string: string) => {
    return isNice2(string) ? total + 1 : total;
  }, 0)
}

export {
  part1,
  part2,
};
