import { toArray } from '../helpers';

const INPUT: Promise<string[]> = toArray('2015/day6.txt', '\n');
const COMMAND_PATTERN = /on|off|toggle/;
const POSITION_PATTERN = /\d{1,3}/g;

const applyBrightness = (lights: Map<string, number>, command: string, position: string): Map<string, number> => {
  const currentValue = lights.get(position) || 0;

  switch (command) {
    case 'toggle':
      lights.set(position, currentValue + 2);
      return lights;
    case 'on':
      lights.set(position, currentValue + 1);
      return lights
    case 'off':
      lights.set(position, currentValue === 0 ? 0 : currentValue - 1);
      return lights
    default:
      return lights;
  }
}

const applyCommand = (lights: Set<string>, command: string, position: string): Set<string> => {
  switch (command) {
    case 'toggle':
      lights.has(position) ? lights.delete(position) : lights.add(position);
      return lights
    case 'on':
      lights.add(position);
      return lights
    case 'off':
      lights.delete(position);
      return lights
    default:
      return lights;
  }
}

const applyInstructions = (lights: Set<string>, instruction: string): Set<string> => {
  const command = instruction.match(COMMAND_PATTERN);
  const stringPositions: string[] | null = instruction.match(POSITION_PATTERN)
  const positions = stringPositions?.map((pos: string) => parseInt(pos));

  if (positions) {
    const [x1, y1, x2, y2] = positions;
    for (let x = x1; x <= x2; x++) {
      for (let y = y1; y <= y2; y++) {
        const currentPosition = [x, y].join(',');
        applyCommand(lights, command ? command[0] : '', currentPosition);
      }
    }
  }

  return lights;
}

const applyBrightnessInstructions = (lights: Map<string, number>, instruction: string): Map<string, number> => {
  const command = instruction.match(COMMAND_PATTERN);
  const stringPositions: string[] | null = instruction.match(POSITION_PATTERN)
  const positions = stringPositions?.map((pos: string) => parseInt(pos));

  if (positions) {
    const [x1, y1, x2, y2] = positions;
    for (let x = x1; x <= x2; x++) {
      for (let y = y1; y <= y2; y++) {
        const currentPosition = [x, y].join(',');
        applyBrightness(lights, command ? command[0] : '', currentPosition);
      }
    }
  }

  return lights;
}

const part1 = async (): Promise<number> => {
  const input = await INPUT;
  const lights: Set<string> = new Set();
  const finishedLights: Set<string> = input.reduce(applyInstructions, lights);

  return finishedLights.size;
}

const part2= async (): Promise<number> => {
  const input = await INPUT;
  const lights: Map<string, number> = new Map();
  const finishedLights: Map<string, number> = input.reduce(applyBrightnessInstructions, lights);

  return [...finishedLights.values()].reduce((sum: number, next: number): number => sum + next);
}

export {
  part1,
  part2,
};
