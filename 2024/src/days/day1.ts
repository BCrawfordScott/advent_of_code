import { toArray } from "../helpers";

const part1 = async (): Promise<number> => {
  const input = await toArray("day1.txt", "\n");
  const groups = input.map((line: string) => line.match(/(?<num1>\d+)\s\s\s(?<num2>\d+)/)?.groups as { num1: string, num2: string });
  const [firsts, seconds] = groups.reduce((acc: [string[], string[]], curr: { num1: string, num2: string }) => {
    acc[0].push(curr.num1);
    acc[1].push(curr.num2);
    return acc;
  }, [[], []]);
  const [sortedFirsts, sortedSeconds] = [firsts.sort(), seconds.sort()];

  let total = 0;

  for (let i = 0; i < firsts.length; i++) {
    total = total + Math.abs(parseInt(sortedFirsts[i]) - parseInt(sortedSeconds[i])); 
  }

  return total;
};

const part2 = async (): Promise<number> => {
  const input = await toArray("day1.txt", "\n");
  const groups = input.map((line: string) => line.match(/(?<num1>\d+)\s\s\s(?<num2>\d+)/)?.groups as { num1: string, num2: string });
  const [firsts, seconds] = groups.reduce((acc: [string[], string[]], curr: { num1: string, num2: string }) => {
    acc[0].push(curr.num1);
    acc[1].push(curr.num2);
    return acc;
  }, [[], []]);

  const counts = seconds.reduce((acc: Map<string, number>, curr: string) => {
    acc.set(curr, (acc.get(curr) || 0) + 1);
    return acc;
  }, new Map<string, number>())

  return firsts.reduce((acc: number, curr: string) => {
    return acc = acc + (parseInt(curr) * (counts.get(curr.toString()) || 0));
  }, 0);
};

export {
  part1,
  part2,
};
