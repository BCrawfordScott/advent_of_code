import { toArray } from "../helpers";

const part1 = async (): Promise<number> => {
  // const input = await toArray("day2test.txt", "\n");
  const input = await toArray("day2.txt", "\n");
  return input.reduce((acc: number, curr: string) => {
    const safe = isSafe(curr.split(" ").map((n) => parseInt(n)));

    return safe ? acc + 1 : acc;
  }, 0);
};

const part2 = async (): Promise<number> => {
  // const input = await toArray("day2test.txt", "\n");
  const input = await toArray("day2.txt", "\n");
  return input.reduce((acc: number, curr: string) => {
    const safe = isDampSafe(curr.split(" ").map((n) => parseInt(n)));
    console.log(curr, safe)
    return safe ? acc + 1 : acc;
  }, 0);
};

function isSafe(nums: number[]): boolean {
  // const nums = line.split(" ").map((n) => parseInt(n));

  let prev = nums[0];
  let ascSafe = true;
  let descSafe = true;
  for (let i = 1; i < nums.length; i++) {
    const num = nums[i];
    if (Math.abs(prev - num) > 3) return false;
    if (num >= prev) descSafe = false;
    if (num <= prev) ascSafe = false;
    if (!ascSafe && !descSafe) return false;

    prev = num;
  }

  return ascSafe || descSafe;
}

function isDampSafe(nums: number[]): boolean {
  // const nums = line.split(" ").map((n) => parseInt(n));

  let prev = nums[0];
  let ascSafe = true;
  let descSafe = true;
  for (let i = 1; i < nums.length; i++) {
    const num = nums[i];
    if (Math.abs(prev - num) > 3 || prev === num) return isSafe(nums.splice(i, 1));
    if (num > prev) descSafe = false;
    if (num < prev) ascSafe = false;
    if (!ascSafe && !descSafe) return false;

    prev = num;
  }

  return ascSafe || descSafe;

  // let prev = nums[0];
  // let asc = false;
  // let desc = false;
  // let dampened = false
  // for (let i = 1; i < nums.length; i++) {
  //   const num = nums[i];
  //   // console.log(prev, num, asc, desc, dampened);
  //   const directed = asc || desc;
  //   if (dampened) {
  //     if (Math.abs(prev - num) > 3) return false;
  //     if (num >= prev) {
  //       if (directed && desc) return false;
  //       if (!asc) asc = true;
  //     }
  //     if (num <= prev) {
  //       if (directed && asc) return false;
  //       if (!desc) desc = true;
  //     }

  //     prev = num;
  //   } else {
  //     if (
  //       (Math.abs(prev - num) > 3) ||
  //       num === prev ||
  //       (num >= prev) && desc ||
  //       (num <= prev) && asc
  //     ) {
  //       dampened = true
  //       if (i === 0) {
  //         prev = num, 
  //         i = 1;
  //       }
  //     } else {
  //       prev = num;
  //     };

  //   }

    
  // }

  return true;
}

export {
  part1,
  part2,
};