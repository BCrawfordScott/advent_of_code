export const parseCoor = (coor: string): number[] => coor.split(',').map(c => parseInt(c));

export const processCoor = (coorArr: number[]): string => coorArr.join(',');