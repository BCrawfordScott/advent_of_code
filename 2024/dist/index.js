"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const twenty24 = __importStar(require("./days"));
async function TwentyTwentyFour() {
    const d1p1 = await twenty24.Day1.part1();
    const d1p2 = await twenty24.Day1.part2();
    const d2p1 = await twenty24.Day2.part1();
    const d2p2 = await twenty24.Day2.part2();
    const d3p1 = await twenty24.Day3.part1();
    const d3p2 = await twenty24.Day3.part2();
    const d4p1 = await twenty24.Day4.part1();
    const d4p2 = await twenty24.Day4.part2();
    const d5p1 = await twenty24.Day5.part1();
    const d5p2 = await twenty24.Day5.part2();
    const d6p1 = await twenty24.Day6.part1();
    const d6p2 = await twenty24.Day6.part2();
    const d7p1 = await twenty24.Day7.part1();
    const d7p2 = await twenty24.Day7.part2();
    const d8p1 = await twenty24.Day8.part1();
    const d8p2 = await twenty24.Day8.part2();
    const d9p1 = await twenty24.Day9.part1();
    const d9p2 = await twenty24.Day9.part2();
    const d10p1 = await twenty24.Day10.part1();
    const d10p2 = await twenty24.Day10.part2();
    const d11p1 = await twenty24.Day11.part1();
    const d11p2 = await twenty24.Day11.part2();
    const d12p1 = await twenty24.Day12.part1();
    const d12p2 = await twenty24.Day12.part2();
    const d13p1 = await twenty24.Day13.part1();
    const d13p2 = await twenty24.Day13.part2();
    const d14p1 = await twenty24.Day14.part1();
    const d14p2 = await twenty24.Day14.part2();
    const d15p1 = await twenty24.Day15.part1();
    const d15p2 = await twenty24.Day15.part2();
    const d16p1 = await twenty24.Day16.part1();
    const d16p2 = await twenty24.Day16.part2();
    const d17p1 = await twenty24.Day17.part1();
    const d17p2 = await twenty24.Day17.part2();
    const d18p1 = await twenty24.Day18.part1();
    const d18p2 = await twenty24.Day18.part2();
    const d19p1 = await twenty24.Day19.part1();
    const d19p2 = await twenty24.Day19.part2();
    const d20p1 = await twenty24.Day20.part1();
    const d20p2 = await twenty24.Day20.part2();
    const d21p1 = await twenty24.Day21.part1();
    const d21p2 = await twenty24.Day21.part2();
    const d22p1 = await twenty24.Day22.part1();
    const d22p2 = await twenty24.Day22.part2();
    const d23p1 = await twenty24.Day23.part1();
    const d23p2 = await twenty24.Day23.part2();
    const d24p1 = await twenty24.Day24.part1();
    const d24p2 = await twenty24.Day24.part2();
    const d25p1 = await twenty24.Day25.part1();
    const d25p2 = await twenty24.Day25.part2();
    if (d1p1 !== undefined)
        console.log('2023 Day 1 part 1: ', d1p1);
    if (d1p2 !== undefined)
        console.log('2023 Day 1 part 2: ', d1p2);
    if (d2p1 !== undefined)
        console.log('2023 Day 2 part 1: ', d2p1);
    if (d2p2 !== undefined)
        console.log('2023 Day 2 part 2: ', d2p2);
    if (d3p1 !== undefined)
        console.log('2023 Day 3 part 1: ', d3p1);
    if (d3p2 !== undefined)
        console.log('2023 Day 3 part 2: ', d3p2);
    if (d4p1 !== undefined)
        console.log('2023 Day 4 part 1: ', d4p1);
    if (d4p2 !== undefined)
        console.log('2023 Day 4 part 2: ', d4p2);
    if (d5p1 !== undefined)
        console.log('2023 Day 5 part 1: ', d5p1);
    if (d5p2 !== undefined)
        console.log('2023 Day 5 part 2: ', d5p2);
    if (d6p1 !== undefined)
        console.log('2023 Day 6 part 1: ', d6p1);
    if (d6p2 !== undefined)
        console.log('2023 Day 6 part 2: ', d6p2);
    if (d7p1 !== undefined)
        console.log('2023 Day 7 part 1: ', d7p1);
    if (d7p2 !== undefined)
        console.log('2023 Day 7 part 2: ', d7p2);
    if (d8p1 !== undefined)
        console.log('2023 Day 8 part 1: ', d8p1);
    if (d8p2 !== undefined)
        console.log('2023 Day 8 part 2: ', d8p2);
    if (d9p1 !== undefined)
        console.log('2023 Day 9 part 1: ', d9p1);
    if (d9p2 !== undefined)
        console.log('2023 Day 9 part 2: ', d9p2);
    if (d10p1 !== undefined)
        console.log('2023 Day 10 part 1: ', d10p1);
    if (d10p2 !== undefined)
        console.log('2023 Day 10 part 2: ', d10p2);
    if (d11p1 !== undefined)
        console.log('2023 Day 11 part 1: ', d11p1);
    if (d11p2 !== undefined)
        console.log('2023 Day 11 part 2: ', d11p2);
    if (d12p1 !== undefined)
        console.log('2023 Day 12 part 1: ', d12p1);
    if (d12p2 !== undefined)
        console.log('2023 Day 12 part 2: ', d12p2);
    if (d13p1 !== undefined)
        console.log('2023 Day 13 part 1: ', d13p1);
    if (d13p2 !== undefined)
        console.log('2023 Day 13 part 2: ', d13p2);
    if (d14p1 !== undefined)
        console.log('2023 Day 14 part 1: ', d14p1);
    if (d14p2 !== undefined)
        console.log('2023 Day 14 part 2: ', d14p2);
    if (d15p1 !== undefined)
        console.log('2023 Day 15 part 1: ', d15p1);
    if (d15p2 !== undefined)
        console.log('2023 Day 15 part 2: ', d15p2);
    if (d16p1 !== undefined)
        console.log('2023 Day 16 part 1: ', d16p1);
    if (d16p2 !== undefined)
        console.log('2023 Day 16 part 2: ', d16p2);
    if (d17p1 !== undefined)
        console.log('2023 Day 17 part 1: ', d17p1);
    if (d17p2 !== undefined)
        console.log('2023 Day 17 part 2: ', d17p2);
    if (d18p1 !== undefined)
        console.log('2023 Day 18 part 1: ', d18p1);
    if (d18p2 !== undefined)
        console.log('2023 Day 18 part 2: ', d18p2);
    if (d19p1 !== undefined)
        console.log('2023 Day 19 part 1: ', d19p1);
    if (d19p2 !== undefined)
        console.log('2023 Day 19 part 2: ', d19p2);
    if (d20p1 !== undefined)
        console.log('2023 Day 20 part 1: ', d20p1);
    if (d20p2 !== undefined)
        console.log('2023 Day 20 part 2: ', d20p2);
    if (d21p1 !== undefined)
        console.log('2023 Day 21 part 1: ', d21p1);
    if (d21p2 !== undefined)
        console.log('2023 Day 21 part 2: ', d21p2);
    if (d22p1 !== undefined)
        console.log('2023 Day 22 part 1: ', d22p1);
    if (d22p2 !== undefined)
        console.log('2023 Day 22 part 2: ', d22p2);
    if (d23p1 !== undefined)
        console.log('2023 Day 23 part 1: ', d23p1);
    if (d23p2 !== undefined)
        console.log('2023 Day 23 part 2: ', d23p2);
    if (d24p1 !== undefined)
        console.log('2023 Day 24 part 1: ', d24p1);
    if (d24p2 !== undefined)
        console.log('2023 Day 24 part 2: ', d24p2);
    if (d25p1 !== undefined)
        console.log('2023 Day 25 part 1: ', d25p1);
    if (d25p2 !== undefined)
        console.log('2023 Day 25 part 2: ', d25p2);
    console.log(twenty24.DEFAULT);
}
TwentyTwentyFour();
