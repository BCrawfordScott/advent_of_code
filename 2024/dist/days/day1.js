"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const part1 = async () => {
    const input = await (0, helpers_1.toArray)("day1.txt", "\n");
    const groups = input.map((line) => line.match(/(?<num1>\d+)\s\s\s(?<num2>\d+)/)?.groups);
    const [firsts, seconds] = groups.reduce((acc, curr) => {
        acc[0].push(curr.num1);
        acc[1].push(curr.num2);
        return acc;
    }, [[], []]);
    const [sortedFirsts, sortedSeconds] = [firsts.sort(), seconds.sort()];
    let total = 0;
    for (let i = 0; i < firsts.length; i++) {
        total = total + Math.abs(parseInt(sortedFirsts[i]) - parseInt(sortedSeconds[i]));
    }
    return total;
};
exports.part1 = part1;
const part2 = async () => {
    const input = await (0, helpers_1.toArray)("day1.txt", "\n");
    const groups = input.map((line) => line.match(/(?<num1>\d+)\s\s\s(?<num2>\d+)/)?.groups);
    const [firsts, seconds] = groups.reduce((acc, curr) => {
        acc[0].push(curr.num1);
        acc[1].push(curr.num2);
        return acc;
    }, [[], []]);
    const counts = seconds.reduce((acc, curr) => {
        acc.set(curr, (acc.get(curr) || 0) + 1);
        return acc;
    }, new Map());
    return firsts.reduce((acc, curr) => {
        return acc = acc + (parseInt(curr) * (counts.get(curr.toString()) || 0));
    }, 0);
};
exports.part2 = part2;
