"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const part1 = async () => {
    // const input = await toArray("day2test.txt", "\n");
    const input = await (0, helpers_1.toArray)("day2.txt", "\n");
    return input.reduce((acc, curr) => {
        const safe = isSafe(curr.split(" ").map((n) => parseInt(n)));
        return safe ? acc + 1 : acc;
    }, 0);
};
exports.part1 = part1;
const part2 = async () => {
    // const input = await toArray("day2test.txt", "\n");
    const input = await (0, helpers_1.toArray)("day2.txt", "\n");
    return input.reduce((acc, curr) => {
        const safe = isDampSafe(curr.split(" ").map((n) => parseInt(n)));
        console.log(curr, safe);
        return safe ? acc + 1 : acc;
    }, 0);
};
exports.part2 = part2;
function isSafe(nums) {
    // const nums = line.split(" ").map((n) => parseInt(n));
    let prev = nums[0];
    let ascSafe = true;
    let descSafe = true;
    for (let i = 1; i < nums.length; i++) {
        const num = nums[i];
        if (Math.abs(prev - num) > 3)
            return false;
        if (num >= prev)
            descSafe = false;
        if (num <= prev)
            ascSafe = false;
        if (!ascSafe && !descSafe)
            return false;
        prev = num;
    }
    return ascSafe || descSafe;
}
function isDampSafe(nums) {
    // const nums = line.split(" ").map((n) => parseInt(n));
    let prev = nums[0];
    let ascSafe = true;
    let descSafe = true;
    for (let i = 1; i < nums.length; i++) {
        const num = nums[i];
        if (Math.abs(prev - num) > 3 || prev === num)
            return isSafe(nums.splice(i, 1));
        if (num > prev)
            descSafe = false;
        if (num < prev)
            ascSafe = false;
        if (!ascSafe && !descSafe)
            return false;
        prev = num;
    }
    return ascSafe || descSafe;
    // let prev = nums[0];
    // let asc = false;
    // let desc = false;
    // let dampened = false
    // for (let i = 1; i < nums.length; i++) {
    //   const num = nums[i];
    //   // console.log(prev, num, asc, desc, dampened);
    //   const directed = asc || desc;
    //   if (dampened) {
    //     if (Math.abs(prev - num) > 3) return false;
    //     if (num >= prev) {
    //       if (directed && desc) return false;
    //       if (!asc) asc = true;
    //     }
    //     if (num <= prev) {
    //       if (directed && asc) return false;
    //       if (!desc) desc = true;
    //     }
    //     prev = num;
    //   } else {
    //     if (
    //       (Math.abs(prev - num) > 3) ||
    //       num === prev ||
    //       (num >= prev) && desc ||
    //       (num <= prev) && asc
    //     ) {
    //       dampened = true
    //       if (i === 0) {
    //         prev = num, 
    //         i = 1;
    //       }
    //     } else {
    //       prev = num;
    //     };
    //   }
    // }
    return true;
}
