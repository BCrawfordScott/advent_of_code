defmodule Input do
    def read(filename) do
        {:ok, input} = File.read(filename)
        input
    end

    def to_array(input) do
        input
        |> String.replace("\r", "")
        |> String.split("\n", trim: true)
    end

    def to_ints(filename) do
        read(filename)
        |> to_array
        |> Enum.map(&(String.to_integer(&1)))
    end

    def process_pword(string) do
        [rules, pword] = String.split(string, ":")
        [counts, letter] = String.split(rules, " ")
        [min, max] = String.split(counts, "-")
        |> Enum.map(&(String.to_integer(&1)))
        {min, max, letter, String.trim(pword)}
    end

    def parsable_pwords(filename) do
        read(filename)
        |> to_array
        |> Enum.map(&(process_pword(&1)))
    end
end

defmodule Toboggan do
    def count_trees(arr) do
        count_trees(arr, {3, 1})
    end

    def count_trees(arr, {sx, sy}) do
        count_trees(arr, {sx, sy}, {sx, sy, 0})
    end

    def count_trees(arr, {sx, sy}, {x, y, count}) do
        if y >= Enum.count(arr) do
            count
        else
            tree = Enum.at(arr, y)
                |> find_tree(x)

            count_trees(arr, {sx, sy}, {x + sx, y + sy, count + tree})
        end 
    end


    def find_tree(string, x) do
        positions = String.split(string, "", trim: true)
        idx = rem(x, Enum.count(positions))

        case Enum.at(positions, idx) do
            "." -> 0
            "#" -> 1
        end
    end

    def count_slopes(forest, slopes) do
        Enum.map(slopes, fn slope -> 
            count_trees(forest, slope)
        end)
    end
end

Input.read('./input.txt')
|> Input.to_array
|> Toboggan.count_trees
|> IO.puts

Input.read('./input.txt')
|> Input.to_array
|> Toboggan.count_slopes([{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}])
|> Enum.reduce(fn trees, acc -> trees * acc end)
|> IO.puts