Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Jigsaw do
    
    def solve1(input_path) do
        process_input(input_path)
        |> corners
        |> Enum.map(&(String.to_integer(&1)))
        |> List.foldl(1, &(&1 * &2))
        |> IO.inspect
    end
    
    def corners(tile_map) do
        neighbored_tiles = tile_map
        |> Map.keys
        |> map_neighbors(tile_map)

        Map.keys(neighbored_tiles)
        |> Enum.reject(fn key -> {_id, _tile, _sides, count} = Map.get(neighbored_tiles, key); count != 2 end)
    end

    def map_neighbors([], neighbor_map), do: neighbor_map
    def map_neighbors([head | tail], neighbor_map) do
        map_neighbors(tail, count_neighbors(head, tail, neighbor_map))
    end

    def count_neighbors(tile, tile_list, neighbor_map) do
        {t_id, t_tile, t_sides, t_count} = Map.get(neighbor_map, tile)
        
        {new_n_map, n_count} = Enum.reduce(tile_list, {neighbor_map, 0}, fn n_tile, acc -> 
            {n_map, count} = acc
            {n_id, n_tile, n_sides, n_count} = Map.get(neighbor_map, n_tile)
            
            flip_count = t_sides
            |> List.foldl(0, fn t_side, acc -> acc + Enum.count(n_sides, fn n_side -> n_side == t_side end) end)
            
            rotate_count = t_sides
            |> List.foldl(0, fn t_side, acc ->  acc + Enum.count(n_sides, fn n_side -> n_side == String.reverse(t_side) end) end)

            total_count = flip_count + rotate_count
            
            {Map.put(n_map, n_id, {n_id, n_tile, n_sides, n_count + total_count}), total_count + count}
        end)
        
        Map.put(new_n_map, t_id, {t_id, t_tile, t_sides, t_count + n_count})
    end
    
    def process_input(input_path) do
        Input.images_array(input_path)
        |> Enum.reduce(%{}, fn image, acc -> process_image(image, acc) end)
    end
    
    def process_image(image, image_map) do
        [tile_id, tile] = image
        |> String.replace("\r", "")
        |> String.split(":\n", trim: true)

        [id] = Regex.run(~r/\d{4}$/, tile_id)
        {sides, tile} =  process_tile(tile)
        Map.put(image_map, id, {id, tile, sides, 0})
    end

    def process_tile(tile) do
        char_rows = String.split(tile, "\n", trim: true)
        |> Enum.map(fn string -> String.split(string, "", trim: true) end)

        
        side1 = Enum.join(List.first(char_rows))
        side2 = List.foldl(char_rows, "", fn row, acc -> acc <> Enum.at(row, length(row) - 1) end)
        side3 = List.foldr(List.last(char_rows), "", fn char, acc -> acc <> char end)
        side4 = List.foldr(char_rows, "", fn row, acc -> acc <> Enum.at(row, 0) end)

        {[side1, side2, side3, side4], tile}
    end
end

test_path = Path.dirname(__ENV__.file) <> "/test.txt"
Jigsaw.solve1(test_path)
input_path = Path.dirname(__ENV__.file) <> "/input.txt"
Jigsaw.solve1(input_path)
