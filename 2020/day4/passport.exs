defmodule Input do
    def read(filename) do
        {:ok, input} = File.read(filename)
        input
    end

    def to_array(input) do
        input
        |> String.replace("\r", "")
        |> String.split("\n", trim: true)
    end

    def to_ints(filename) do
        read(filename)
        |> to_array
        |> Enum.map(&(String.to_integer(&1)))
    end

    def process_pword(string) do
        [rules, pword] = String.split(string, ":")
        [counts, letter] = String.split(rules, " ")
        [min, max] = String.split(counts, "-")
        |> Enum.map(&(String.to_integer(&1)))
        {min, max, letter, String.trim(pword)}
    end

    def parsable_pwords(filename) do
        read(filename)
        |> to_array
        |> Enum.map(&(process_pword(&1)))
    end

    def divide_passports(filename) do
        read(filename)
        |> String.replace("\r", "")
        |> String.split("\n\n")
        |> Enum.map(fn string -> String.replace(string, "\n", " ") end)
    end
end

defmodule Passport do
    
    @required_keys [
        ~r/(byr:)+/, 
        ~r/(iyr:)+/, 
        ~r/(eyr:)+/, 
        ~r/(hgt:)+/, 
        ~r/(hcl:)+/, 
        ~r/(ecl:)+/, 
        ~r/(pid:)+/
    ]

    @strict_keys [
        ~r/byr:(19[2-9]\d)|(200[0-2])/,
        ~r/iyr:20((1\d)|20)/,
        ~r/eyr:20((2\d)|30)/,
        ~r/hgt:((1[5-8]\d|19[0-3])(?=cm)|(59|6\d|7[0-6])(?=in))/,
        ~r/hcl:#([a-f]|\d){6}/,
        ~r/ecl:(amb|blu|brn|gry|grn|hzl|oth)/,
        ~r/pid:\d{9}(\s|$)/
    ]

    def validate_passport(pass) do
        case Enum.all?(@required_keys, fn pattern ->
            String.match?(pass, pattern)
        end) do
            true -> 1
            false -> 0
        end
    end

    def strict_validation(pass) do
        case Enum.all?(@strict_keys, fn pattern ->
            String.match?(pass, pattern)
        end) do
            true -> 1
            false -> 0
        end
    end

    def count_strict_passports(passports), do: count_strict_passports(passports, 0)
    def count_strict_passports([], count), do: count
    def count_strict_passports([curr | rest], count) do
        count_strict_passports(rest, count + strict_validation(curr))
    end

    def count_valid_passports(passports), do: count_valid_passports(passports, 0)
    def count_valid_passports([], count), do: count
    def count_valid_passports([curr | rest], count) do
        count_valid_passports(rest, count + validate_passport(curr))
    end
end

Input.divide_passports("./input.txt")
|> Passport.count_valid_passports
|> IO.puts

Input.divide_passports("./input.txt")
# |> IO.inspect
|> Passport.count_strict_passports
|> IO.puts

# IO.puts(String.match?("hgt168", ~r/hgt:((1[5-8]\d|19[0-3])(?=cm)|(59|6\d|7[0-6])(?=in))/))