Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Seat do

    def to_assignments(seats) do
        seats
        |> to_binaries
        |> to_seat_ids
    end

    def to_binaries(seats) do
        Enum.map(seats, &(to_binary_tuple(&1)))
    end

    def to_binary_tuple(string) do
        String.replace(string, ["F", "B", "L", "R"], fn
            "F" -> "0"
            "L" -> "0"
            "B" -> "1"
            "R" -> "1"
        end)
        |> String.split_at(7)
    end

    def calc_seat_id({b_row, b_column}) do
        row = String.to_integer(b_row, 2)
        column = String.to_integer(b_column, 2)

        row * 8 + column
    end

    def to_seat_ids(binaries) do
        binaries
        |> Enum.map(&(calc_seat_id(&1)))
    end

    def highest_id(assignments) do
        Enum.max(assignments)
    end

    def find_seat(assignments) do
        assignments
        |> Enum.sort
        |> check_seats
    end

    def check_seats([ head | tail ]), do: check_seats(tail, head)
    def check_seats(seat_ids, acc) do
        seat_ids
        |> Enum.reduce_while(acc, fn id, acc ->
            case id == acc + 1 do
                true -> { :cont, id }
                false -> { :halt, acc + 1 }
            end
        end)
    end

end

input_path = Path.dirname(__ENV__.file) <> "./input.txt"

Input.to_array(input_path)
|> Seat.to_assignments
|> Seat.highest_id
|> IO.puts

Input.to_array(input_path)
|> Seat.to_assignments
|> Seat.find_seat
|> IO.inspect
