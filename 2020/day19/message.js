const fs = require('fs')

function countValid(inputPath) {
    [ruleMap, messages] = parseInput(inputPath)
    pattern = new RegExp("^" + buildPattern(ruleMap, "0") + "$")

    count = messages.reduce((acc, message) => { return message.match(pattern) ? acc + 1 : acc}, 0);
    console.log(count);
}

function countValid2(inputPath) {
    [ruleMap, messages] = parseInput(inputPath)

    ruleMap["8"] = ["42", "|", "42", "8"];
    ruleMap["11"] = ["42", "31", "|", "42", "11", "31"];
    pattern = new RegExp("^" + buildPattern(ruleMap, "0") + "$")

    count = messages.reduce((acc, message) => { return message.match(pattern) ? acc + 1 : acc }, 0);
    console.log(count);
}

function parseInput(inputPath) {
    [rules, messages] = fs.readFileSync(inputPath).toString().split("\r\n\r\n");

    ruleMap = mapRules(rules);

    return [ruleMap, messages.split("\r\n")]
}

function mapRules(rules) {
    indRules = rules.split("\r\n");
    const map = new Map();

    indRules.forEach(rule => {
        [key, pattern] = rule.split(": ")
        map[key] = pattern.split(" ")
    })

    return map;
}

function buildPattern(ruleMap, key, depth=20) {
    const rule = ruleMap[key];

    if (depth <= 0) return "";
    if (rule[0][1] === "a" || rule[0][1] === "b") return rule[0][1];
    patString = "(";
    rule.forEach(el => {
        if (el === "|") {
            patString += el;
        } else {
            patString += buildPattern(ruleMap, el, depth - 1);
        }
    })

    return patString + ")";
}

countValid2('./input.txt')