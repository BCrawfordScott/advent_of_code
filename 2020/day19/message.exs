Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Message do
    
    def count_valid_messages(input_path) do
        {rule_map, messages} = parse_input(input_path)
        string_pattern = "^" <> build_pattern(rule_map, "0") <> "$"
        final_pattern = String.replace(string_pattern, "(|)", "|")

        {:ok, pattern} = Regex.compile(final_pattern)
        IO.inspect(pattern)
        messages
        |> Enum.count(fn message -> String.match?(message, pattern) end)
        |> IO.inspect
    end

    def count_valid_part2(input_path) do
        {rule_map, messages} = parse_input(input_path)
        pt2_rules_map = rule_map
        |> Map.put("8", ["42", "|", "42", "8"])
        |> Map.put("11", ["42", "31", "|", "42", "11", "31"])
        string_pattern = "^" <> build_pattern(pt2_rules_map, "0") <> "$"

        {:ok, pattern} = Regex.compile(string_pattern)
        IO.inspect(pattern)
        messages
        |> Enum.count(fn message -> String.match?(message, pattern) end)
        |> IO.inspect
    end
    
    def parse_input(input_path) do
        [rules, messages] = Input.messages_array(input_path) # Read the file as a string and split on "\n\n"
        rule_map = map_rules(rules)
        {rule_map, String.split(messages, "\n", trim: true)}
    end

    def build_pattern(rule_map, key, depth \\ 14)
    def build_pattern(rule_map, key, depth) do
        rule = rule_map[key]

        cond do
            depth <= 0 -> ""
            String.match?(Enum.at(rule, 0), ~r/\D/) -> Enum.at(String.split(Enum.at(rule, 0), "", trim: true), 1) 
            true -> pattern = Enum.reduce(rule, "(", fn el, acc -> 
                    case el do
                        "|" -> acc <> el
                        next_key -> acc <> build_pattern(rule_map, next_key, depth - 1)    
                    end
                end)

                case pattern <> ")" do
                    "()" -> ""
                    "(|)" -> "|"
                    other -> other
                end    
        end
    end

    def map_rules(rules) do
        String.split(rules, "\n", trim: true)
        |> Enum.reduce(%{}, fn rule, acc -> 
            [key, pattern] = String.split(rule, ": ", trim: true)
            Map.put(acc, key, String.split(pattern, " ", trim: true))
        end)
    end
end

# test_path = Path.dirname(__ENV__.file) <> "/test.txt"
# Message.count_valid_messages(test_path)
input_path = Path.dirname(__ENV__.file) <> "/input.txt"
# Message.count_valid_messages(input_path)

Message.count_valid_part2(input_path)