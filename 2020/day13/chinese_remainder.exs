defmodule Chinese do
    def remainder(mod_x_remainders) do
        max = Enum.reduce(mod_x_remainders, fn {mod, _r}, acc -> mod * acc end)
        mod_x_remainders
        |> Enum.reduce(fn {m, r} -> Enum.take_every(r..max, m) |> MapSet.new end)
        |> Enum.reduce(fn set, acc -> MapSet.intersection(set, acc) end)
        |> MapSet.to_list
    end
end