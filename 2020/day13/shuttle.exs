Code.require_file("../../input.ex","#{__ENV__.file}")

### Borrowed from https://rosettacode.org/wiki/Modular_inverse#Elixir

defmodule Modular do
    def extended_gcd(a, b) do
        {last_remainder, last_x} = extended_gcd(abs(a), abs(b), 1, 0, 0, 1)
        {last_remainder, last_x * (if a < 0, do: -1, else: 1)}
    end
    
    defp extended_gcd(last_remainder, 0, last_x, _, _, _), do: {last_remainder, last_x}
    defp extended_gcd(last_remainder, remainder, last_x, x, last_y, y) do
        quotient   = div(last_remainder, remainder)
        remainder2 = rem(last_remainder, remainder)
        extended_gcd(remainder, remainder2, x, last_x - quotient*x, y, last_y - quotient*y)
    end
    
    def inverse(e, et) do
        {g, x} = extended_gcd(e, et)
        if g != 1, do: raise "The maths are broken!"
        rem(x+et, et)
    end
end

defmodule Shuttle do
    def earliest_bus(input_path) do
        parse_input(input_path)
        |> find_wait_times
        |> IO.inspect
    end

    def gauss_solve(input_path) do
        {_my_time, shuttles} = parse_input(input_path)
        shuttles_with_offsets(shuttles)
        |> gauss
        |> IO.inspect
    end


    def shuttles_with_offsets(shuttles) do
        String.split(shuttles, ",", trim: true)
        |> Enum.with_index
        |> Enum.reject(fn {shu, _idx} -> shu == "x" end)
        |> Enum.map(fn {shu, idx} -> num_shu = String.to_integer(shu); {num_shu, num_shu - idx} end)
    end
    
    
    def find_wait_times({timestamp, shuttles}) do
        String.split(shuttles, ",", trim: true)
        |> Enum.reduce([], fn shuttle, acc -> 
            case shuttle do
                "x" -> acc
                string_shu -> 
                    shu = String.to_integer(string_shu) 
                    mod = shu - rem(timestamp, shu);
                    acc ++ [{mod, shu * mod}]
            end
        end)
        |> Enum.min_by(fn {wait, _val} -> wait end)
    end
    
    def parse_input(input_path) do
        {[timestamp], [shuttles]} = Enum.split(Input.to_array(input_path), 1)

        {String.to_integer(timestamp), shuttles}
    end


    def gauss(mod_x_remainders) do
        max = Enum.reduce(mod_x_remainders, 1, fn {mod, _r}, acc -> mod * acc end)
        
        result = Enum.reduce(mod_x_remainders, 0, fn {m, r}, acc ->
            b = div(max, m) 
            acc + (r * b * Modular.inverse(b, m))
        end)

        rem(result, max)
    end

end

test_path = Path.dirname(__ENV__.file) <> "/test.txt"
# Shuttle.earliest_bus(test_path)
Shuttle.gauss_solve(test_path)
input_path = Path.dirname(__ENV__.file) <> "/input.txt"
# Shuttle.earliest_bus(input_path)
Shuttle.gauss_solve(input_path)


    # Brute force:

    # def earliest_aligned(input_path) do
    #     {_my_time, shuttles} = parse_input(input_path)
    #     shuttles_with_offsets(shuttles)
    #     |> find_alignment
    #     |> IO.inspect
    # end

    # def c_remainder_solve(input_path) do
    #     {_my_time, shuttles} = parse_input(input_path)
    #     shuttles_with_offsets(shuttles)
    #     |> remainder
    #     |> IO.inspect
    # end
    
    # def find_alignment(shuttles, count \\ 1)
    # def find_alignment(shuttles, count) do
    #     [{freq, _idx} | tail] = shuttles
    #     timestamp = freq * count
    #     case Enum.all?(tail, fn {shu, offset} -> rem(timestamp + offset, shu) == 0 end) do
    #         true -> timestamp
    #         false -> find_alignment(shuttles, count + 1)
    #     end
    # end

    # def remainder(mod_x_remainders) do
    #     max = Enum.reduce(mod_x_remainders, fn {mod, _r}, acc -> mod * acc end)
    #     mod_x_remainders
    #     |> Enum.reduce(fn {m, r} -> Enum.take_every(r..max, m) |> MapSet.new end)
    #     |> Enum.reduce(fn set, acc -> MapSet.intersection(set, acc) end)
    #     |> MapSet.to_list
    # end