Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Luggage do
    @parent ~r/.*(?= bags contain)/
    @children ~r/(?<=contain ).*/
    @empty ~r/no/
    @child ~r/(?<=\d ).*(?= bag)/
    @bag_count ~r/\d/

    def map_rules(input) do
        input
        |> Enum.map(&(map_rule(&1)))
    end
    
    def map_rule(string) do
        parent = Regex.run(@parent, string) 
        |> List.first
        |> String.to_atom
        children = Regex.run(@children, string)
        |> List.first
        |> make_children_tuples

        {parent, children}
    end

    def make_children_tuples(string) do
        case String.match?(string, @empty) do
            true -> []
            false -> String.split(string, ", ", trim: true)
            |> Enum.map(fn string -> 
                count = Regex.run(@bag_count, string) |> List.first |> String.to_integer
                child = Regex.run(@child, string) |> List.first |> String.to_atom

                {child, count}
            end)
        end
    end

    def can_carry?(rule, rules, color) do
        {_parent, children} = rule 
        here = List.keymember?(children, color, 0) 
        child_rules = Enum.map(children, fn { child, _count} -> List.keyfind(rules, child, 0) end)
        there = Enum.any?(child_rules, &(can_carry?(&1, rules, color)))
        here or there
 
    end

    def count_bags(rules, color) do
        rules
        |> Enum.map(&(can_carry?(&1, rules, color)))
        |> Enum.reject(&(&1 == false))
        |> length
    end   

    def calc_inner_bags(rules, color) do
        total = rules
        |> List.keyfind(color, 0)
        |> inner_bag_count(rules)

        total - 1
    end

    def inner_bag_count({_parent, []}, _rules), do: 1
    def inner_bag_count({_parent, children}, rules) do
        Enum.reduce(children, 1, fn {child, count}, acc -> 
            next_rule = List.keyfind(rules, child, 0)
            total = inner_bag_count(next_rule, rules) * count
            total + acc
        end)
    end
end

input_path = Path.dirname(__ENV__.file) <> "/input.txt"
tester_path = Path.dirname(__ENV__.file) <> "/tester.txt"

input_path
|> Input.to_array
|> Luggage.map_rules
|> Luggage.count_bags(:"shiny gold")
|> IO.inspect

input_path
|> Input.to_array
|> Luggage.map_rules
|> Luggage.calc_inner_bags(:"shiny gold")
|> IO.inspect