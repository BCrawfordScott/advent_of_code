defmodule Input do
    def read(filename) do
        {:ok, input} = File.read(filename)
        input
    end

    def to_array(input) do
        input
        |> String.replace("\r", "")
        |> String.split("\n", trim: true)
    end

    def to_ints(filename) do
        read(filename)
        |> to_array
        |> Enum.map(&(String.to_integer(&1)))
    end

    def process_pword(string) do
        [rules, pword] = String.split(string, ":")
        [counts, letter] = String.split(rules, " ")
        [min, max] = String.split(counts, "-")
        |> Enum.map(&(String.to_integer(&1)))
        {min, max, letter, String.trim(pword)}
    end

    def parsable_pwords(filename) do
        read(filename)
        |> to_array
        |> Enum.map(&(process_pword(&1)))
    end
end

defmodule Validate do
    def char_count(char, string) do
        String.split(string, "")
        |> Enum.count(&(&1 == char))
    end

    def between(num, min, max) do
        num >= min && num <= max
    end

    def currently_valid({i1, i2, letter, string}) do
        chars = String.split(string, "")
        first = (Enum.at(chars, i1) == letter) 
        second = (Enum.at(chars, i2) == letter)

        (not first and second) or (first and not second)
    end

    def is_valid({min, max, letter, string}) do
        char_count(letter, string)
        |> between(min, max)
    end

    def num_valid(arr) do
        Enum.map(arr, &(is_valid(&1)))
        |> Enum.count(&(&1))
    end

    def num_currently_valid(arr) do
        Enum.map(arr, &(currently_valid(&1)))
        |> Enum.count(&(&1))
    end
end

Input.parsable_pwords('./input.txt')
|> Validate.num_valid
|> IO.puts

Input.parsable_pwords('./input.txt')
|> Validate.num_currently_valid
|> IO.puts

# Validate.num_valid([{1, 2, "S", "String"}])
# |> IO.puts

# Validate.char_count("S", "String")
# |> IO.puts