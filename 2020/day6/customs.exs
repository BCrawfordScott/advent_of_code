Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Customs do
    def analyze(group) do
        # strip the \n and turn into an array of individual chars
        # make uniq
        # count the length

        group
        |> prep_group
        |> unique_claims
        |> count_decs
    end

    def analyze_strict(group) do
        group
        |> prep_group_strict
        |> group_commons
        |> count_decs
    end

    def group_commons(group) do
        group
        |> Enum.reduce(fn person, acc -> 
            similarities(acc, person)
        end)
    end

    def similarities(claim1, claim2) do
        claim1
        |> Enum.reduce([], fn el, acc -> 
            case Enum.find_value(claim2, &(&1 == el)) do
                true -> [el | acc]
                bad -> acc
            end
        end)
    end

    def prep_group(group) do
        group
        |> String.replace("\n", "")
        |> String.split("", trim: true)
    end

    def prep_group_strict(group) do
        group
        |> String.split("\n", trim: true)
        |> Enum.map(&(String.split(&1, "", trim: true)))
    end

    def unique_claims(pgroup) do
        pgroup
        |> Enum.uniq
    end

    def count_decs(ugroup) do
        Enum.count(ugroup)
    end

    def sum_declarations(groups) do
        # map the groups to totals
        # reduce to a sum
        groups
        |> Enum.reduce(0, fn group, acc ->
            acc + analyze(group)
        end)
    end
    def sum_strict(groups) do
        # map the groups to totals
        # reduce to a sum
        groups
        |> Enum.reduce(0, fn group, acc ->
            acc + analyze_strict(group)
        end)
    end
end

input_path = Path.dirname(__ENV__.file) <> "/input.txt"

input_path
|> Input.customs_array
|> Customs.sum_declarations
# |> List.first
# |> Customs.prep_group
# |> Customs.unique_claims
# |> Customs.count_decs
|> IO.inspect

input_path
|> Input.customs_array
|> Customs.sum_strict
# |> List.first
# |> Customs.prep_group_strict
|> IO.inspect