
defmodule TwoSum do
    def find(arr, find) do
        arr
        |> Enum.reduce_while({:none}, fn x, acc ->
            y = find - x
            case Enum.find_index(arr, &(&1 == y)) do
                nil -> {:cont, acc}
                _ -> {:halt, {x, y}}
            end
        end)
    end

    def find_three(arr, find) do
        arr
        |> Enum.reduce_while({:none}, fn x, acc ->
            diff = find - x
            case TwoSum.find(arr, diff) do
                {:none} -> {:cont, acc}
                {y, z} -> {:halt, {x, y, z}}
            end
        end)
    end

    def tuple_product(tuple) do
        tuple
        |> Tuple.to_list
        |> Enum.reduce(&(&1 * &2))
    end
end

defmodule Input do
    def read(filename) do
        {:ok, input} = File.read(filename)
        input
    end

    def to_array(input) do
        input
        |> String.replace("\r", "")
        |> String.split("\n", trim: true)
    end

    def to_ints(filename) do
        read(filename)
        |> to_array
        |> Enum.map(&(String.to_integer(&1)))
    end
end




Input.to_ints('./input.txt')
|> TwoSum.find(2020)
|> TwoSum.tuple_product
|> IO.puts

Input.to_ints('./input.txt')
|> TwoSum.find_three(2020)
|> TwoSum.tuple_product
|> IO.puts