Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Rain do

    def find_distance(input_path) do
        Input.to_array(input_path)
        |> follow_instructions
        |> manhattan({0, 0})
        |> IO.inspect
    end

    def follow_instructions(instructions, bearing \\ {{0, 0}, {10, 1}})
    def follow_instructions([], {pos, _wpoint}), do: pos
    def follow_instructions([head | tail], bearing), do: follow_instructions(tail, apply_instruction(head, bearing))

    def apply_instruction(inst, {pos, {x, y}}) do
        case parse_instruction(inst) do
            {"N", val} -> {pos, {x, y + val}}
            {"E", val} -> {pos, {x + val, y}}
            {"S", val} -> {pos, {x, y - val}}
            {"W", val} -> {pos, {x - val, y}}
            {"L", val} -> {pos, rotate(360 - val, {x, y})}
            {"R", val} -> {pos, rotate(val, {x, y})}
            {"F", val} -> {move_ship(val, {pos, {x, y}}), {x, y}}
        end
    end

    def move_ship(val, {{px, py}, {wx, wy}}) do
        dx = val * wx
        dy = val * wy

        {px + dx, py + dy}
    end

    def rotate(degrees, {x, y}) do
        case degrees do
            90 -> {y, x * -1}
            180 -> {x * -1, y * -1}
            270 -> {y * -1, x}
        end
    end
    
    def parse_instruction(inst) do
        {cmd, val} = String.split_at(inst, 1)

        {cmd, String.to_integer(val)}
    end
    
    def manhattan(pos1, pos2) do
        {x1, y1} = pos1
        {x2, y2} = pos2  

        abs(x1 - x2) + abs(y1 - y2)
    end
end


test_path = Path.dirname(__ENV__.file) <> "/test.txt"
Rain.find_distance(test_path)
input_path = Path.dirname(__ENV__.file) <> "/input.txt"
Rain.find_distance(input_path)
