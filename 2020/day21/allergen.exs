Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Allergen do
    def dangerous_ingredients(input_path) do
        input_list = Input.to_array(input_path) # Read and split the input string on "\n"
        {allergen_map, _all_foods} = map_allergens(input_list)

        allergy_ingredients = Map.to_list(allergen_map)
        |> sort_ingredients
        |> identify_ingredients

        allergy_ingredients
        |> Map.keys
        |> Enum.sort
        |> Enum.map(fn key -> Map.get(allergy_ingredients, key) end)
        |> Enum.join(",")
        |> IO.puts
    end

    def identify_ingredients(list, allergy_map \\ %{})
    def identify_ingredients([], allergy_map), do: allergy_map
    def identify_ingredients([head | tail], allergy_map) do
       {key, ingredient_set} = head    
       ingredient = MapSet.to_list(ingredient_set) |> List.first 

       new_tail = Enum.map(tail, fn {key, ing_set} -> {key, MapSet.delete(ing_set, ingredient)} end)

       identify_ingredients(sort_ingredients(new_tail), Map.put(allergy_map, key, ingredient))
    end

    def sort_ingredients(allergen_list) do
        Enum.sort(allergen_list, fn {_a1, in_set1}, {_a2, in_set2} -> MapSet.size(in_set1) < MapSet.size(in_set2) end)
    end

    def allergy_free(input_path) do
        input_list = Input.to_array(input_path) # Read and split the input string on "\n"
        {allergen_map, all_foods} = map_allergens(input_list)

        allergen_list = Map.to_list(allergen_map)
        no_allergens = List.foldl(allergen_list, all_foods, fn {_allergen, food_set}, acc -> MapSet.difference(acc, food_set) end)

        processed_input = Enum.map(input_list, fn input_row -> process_food(input_row) end)
        MapSet.to_list(no_allergens)
        |> Enum.map(fn food -> 
            Enum.count(processed_input, fn {_allergy_list, food_set} -> MapSet.member?(food_set, food) end)
        end)
        |> List.foldl(0, &(&1 + &2))
        |> IO.inspect  
    end
    
    def map_allergens(food_list) do
        List.foldl(food_list, {%{}, %MapSet{}}, fn food, {allergen_map, foods} ->
            {allergens, curr_foods} = process_food(food)

            all_foods = MapSet.union(foods, curr_foods)

            all_allergens = Enum.reduce(allergens, allergen_map, fn allergen, map -> 
                if Map.has_key?(map, allergen) do
                    Map.put(map, allergen, MapSet.intersection(curr_foods, Map.get(map, allergen)))
                else
                    Map.put(map, allergen, curr_foods)
                end
            end)

            {all_allergens, all_foods}
        end)
    end

    def process_food(food) do
        [foods, allergens] = String.split(food, " (contains ", trim: true)
        allergen_list = allergens
        |> String.replace(")", "")
        |> String.split(", ", trim: true)

        {allergen_list, MapSet.new(String.split(foods, " ", trim: true))}
    end
end

test_path = Path.dirname(__ENV__.file) <> "/test.txt"
# Allergen.allergy_free(test_path)
Allergen.dangerous_ingredients(test_path)
input_path = Path.dirname(__ENV__.file) <> "/input.txt"
# Allergen.allergy_free(input_path)
Allergen.dangerous_ingredients(input_path)