Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Dock do
    def sum_masked_binaries(input_path) do
        {_mask, cache} = process_masks(input_path)

        Enum.reduce(Map.values(cache), &(&1 + &2))
        |> IO.inspect
    end
  
    def process_masks(input_path) do
        Input.to_array(input_path)  # convert input into a List of strings split on "\n"
        |> Enum.reduce({nil, %{}}, fn string, {mask, memory} -> 
            case is_mask?(string) do
                true -> {get_mask_binary(string), memory}
                false -> 
                    {binary_loc, int} = prep_mem_binary(string)
                    masked_binary = Enum.reverse(mask_binary(Enum.reverse(mask), Enum.reverse(binary_loc)))
                    new_memory = binary_addresses(masked_binary, memory, {"", int})

                    {mask, new_memory}
            end
        end)
    end

    def binary_addresses([], cache, {address, val}), do: Map.put(cache, address, val)
    def binary_addresses([head | tail], cache, {address, val}) do
        case head do
            "X" -> 
                cache1 = binary_addresses(tail, cache, {address <> "1", val})
                binary_addresses(tail, Map.merge(cache, cache1), {address <> "0", val})
                
            b -> binary_addresses(tail, cache, {address <> b, val})
        end
    end
    
    def is_mask?(cmd) do
        String.match?(cmd, ~r/^mas/)
    end

    def get_mask_binary(mask) do
        Regex.run(~r/(?<= = )\w*/, mask)
        |> List.first
        |> String.split("", trim: true) 
    end

    def prep_mem_binary(mem) do
        [int] = Regex.run(~r/(?<= = )\w*/, mem)
        [loc] = Regex.run(~r/(?<=mem\[)\w*/, mem)

        binary_loc = String.to_integer(loc)
        |> Integer.digits(2)
        |> Enum.map(&(Integer.to_string(&1)))

        {binary_loc, String.to_integer(int)}
    end
    
    def mask_binary([], _mem), do: []
    def mask_binary([maskd | mask_tail], []), do: [mask_digit(maskd, nil) | mask_binary(mask_tail, [])]
    def mask_binary([maskd | mask_tail], [memd | mem_tail]) do
        [mask_digit(maskd, memd) | mask_binary(mask_tail, mem_tail)]
    end
    
    def mask_digit(maskb, memb) do
        case maskb do  
            "0" -> if memb == nil, do: "0", else: memb
            mask -> mask
        end
    end
end

test_path = Path.dirname(__ENV__.file) <> "/test.txt"
# Dock.sum_masked_binaries(test_path)
input_path = Path.dirname(__ENV__.file) <> "/input.txt"
Dock.sum_masked_binaries(input_path)