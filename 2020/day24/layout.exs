Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Layout do

    @dir_map %{
        "ne" => {1, -1},
        "nw" => {0, -1},
        "w" => {-1, 0},
        "e" => {+1, 0},
        "se" => {0, 1},
        "sw" => {-1, 1}
    }

    def neighbor_deltas(), do: Map.values(@dir_map)

    def tiles_after(input_path, count) do
        map_tiles(input_path)
        |> apply_tiles
        |> MapSet.to_list
        |> cycle(count)
        |> length
        |> IO.inspect
    end
    
    def cycle(tiles, 0), do: tiles
    def cycle(tiles, count) do
        Enum.reduce(tiles, %{}, fn tile, acc -> record_neighbors(tile, acc) end)
        |> determine_black(tiles)
        |> cycle(count - 1)
    end
    
    def determine_black(neighbor_counts, prev_black) do
        still_black = Enum.reject(prev_black, fn tile -> neighbor_counts[tile] == 0 or neighbor_counts[tile] > 2 end)
        newly_black = Enum.reduce(still_black, Map.keys(neighbor_counts), fn tile, acc -> List.delete(acc, tile) end)
        |> Enum.reject(fn tile -> neighbor_counts[tile] != 2 end)

        still_black ++ newly_black
    end
    
    def record_neighbors({x, y}, neighbor_counts) do
        Enum.reduce(neighbor_deltas(), neighbor_counts, fn {dx, dy}, acc -> 
            neighbor = {x + dx, y + dy}
            Map.put(acc, neighbor, Map.get(acc, neighbor, 0) + 1)
        end)
    end

    def total_black_tiles(input_path) do
        map_tiles(input_path)
        |> apply_tiles
        |> MapSet.size
        |> IO.inspect
    end

    def map_tiles(input_path) do
        parse_input(input_path)
        |> Enum.map(fn dirs -> dirs_to_tile(dirs) end)
    end 
    
    def parse_input(input_path) do
        Input.to_array(input_path) # Read the fiel to a string and split on "\n"
        |> Enum.map(&(String.split(&1, "", trim: true)))
    end

    def dirs_to_tile(list, pos \\ {0, 0}, dir \\ "")
    def dirs_to_tile([], pos, _dir), do: pos
    def dirs_to_tile([head | tail], pos, dir) when head == "e" or head == "w" do
        {cx, cy} = pos
        {dx, dy} = @dir_map[dir <> head]

        dirs_to_tile(tail, {cx + dx, cy + dy}, "") 
    end
    def dirs_to_tile([head | tail], pos, dir), do: dirs_to_tile(tail, pos, dir <> head)

    def apply_tiles(tiles, black_tiles \\ %MapSet{})
    def apply_tiles([], black_tiles), do: black_tiles
    def apply_tiles([head | tail], black_tiles) do
        if MapSet.member?(black_tiles, head), do: apply_tiles(tail, MapSet.delete(black_tiles, head)), else: apply_tiles(tail, MapSet.put(black_tiles, head))
    end
end

test_path = Path.dirname(__ENV__.file) <> "/test.txt"
# Layout.total_black_tiles(test_path)
Layout.tiles_after(test_path, 100)

input_path = Path.dirname(__ENV__.file) <> "/input.txt"
# Layout.total_black_tiles(input_path)
Layout.tiles_after(input_path, 100)