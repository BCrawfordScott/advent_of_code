Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Combat do
    
    def run(input_path, rec \\ false)
    def run(input_path, rec) do
        [deck1, deck2] = parse_decks(input_path)

        result = if rec, do: rec_combat(deck1, deck2), else: play(deck1, deck2)
        result |> score |> IO.inspect
    end
    
    def score(result) when is_list(result), do: calc_score(result)
    def score({_player, deck}), do: calc_score(deck)

    def calc_score(deck) do
        deck
        |> Enum.reverse
        |> Enum.with_index
        |> List.foldl(0, fn {num, idx}, acc -> acc + (num * (idx + 1)) end)
    end
    
    def play(deck1, []), do: deck1
    def play([], deck2), do: deck2
    def play([head1 | tail1], [head2 | tail2]) do
        case head1 > head2 do
            true -> play(tail1 ++ [head1, head2], tail2)
            false -> play(tail1, tail2  ++ [head2, head1])
        end
    end

    def rec_combat(deck1, deck2, mem \\ %MapSet{})
    def rec_combat([], deck2, _mem), do: {:player2, deck2}
    def rec_combat(deck1, [], _mem), do: {:player1, deck1}
    def rec_combat(deck1, deck2, mem) do
        if MapSet.member?(mem, {deck1, deck2}) do
            {:player1, deck1}
        else
            [head1 | tail1] = deck1 
            [head2 | tail2] = deck2

            if head1 <= length(tail1) and head2 <= length(tail2) do
                case rec_combat(Enum.slice(tail1, 0, head1), Enum.slice(tail2, 0, head2)) do
                    {:player1, _deck} -> rec_combat(tail1 ++ [head1, head2], tail2, MapSet.put(mem, {deck1, deck2}))
                    {:player2, _deck} -> rec_combat(tail1, tail2 ++ [head2, head1], MapSet.put(mem, {deck1, deck2}))
                end
            else
                if head1 > head2 do
                    rec_combat(tail1 ++ [head1, head2], tail2, MapSet.put(mem, {deck1, deck2}))
                else
                    rec_combat(tail1, tail2 ++ [head2, head1], MapSet.put(mem, {deck1, deck2}))
                end
            end
        end
    end
    
    def parse_decks(input_path) do
        Input.combat_array(input_path) # Parse the input to a string and split on "\n\n"
        |> Enum.map(fn deck -> String.split(deck, "\n", trim: true) end)
        |> Enum.map(fn [_head | tail] -> tail end)
        |> Enum.map(fn deck -> Enum.map(deck, &(String.to_integer(&1))) end)
    end
end

test_path = Path.dirname(__ENV__.file) <> "/test.txt"
Combat.run(test_path)
Combat.run(test_path, true)

input_path = Path.dirname(__ENV__.file) <> "/input.txt"
Combat.run(input_path)
Combat.run(input_path, true)
