Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Handheld do
    def process_command(cmd) do
        [cmd, int] = String.split(cmd, " ", trim: true)
        {cmd, String.to_integer(int)}
    end

    def compute_offset(cmd) do
        case process_command(cmd) do
            {"nop", _} -> {"nop", 0, 1}
            {"acc", int} -> {"acc", int, 1}
            {"jmp", int} -> {"jmp", 0, int}
        end
    end  

    def locate_loop(program, idx \\ 0, acc \\ 0, set \\ %MapSet{})
    def locate_loop(program, idx, acc, set) do
        command = Enum.at(program, idx)
        if command do
            case MapSet.member?(set, idx) do
                true -> {:loop, acc}
                false -> 
                    {_cmd, aug, offset} =  compute_offset(command)

                    locate_loop(program, idx + offset, acc + aug, MapSet.put(set, idx))
            end
        else
            {:no_loop, acc}
        end
    end

    def test_loop(program, idx \\ 0, acc \\ 0, set \\ %MapSet{})
    def test_loop(program, idx, acc, set) do
        command = Enum.at(program, idx)
        if command do
            case MapSet.member?(set, idx) do
                true -> {:loop, acc}
                false -> 
                    {cmd, aug, offset} = compute_offset(command)

                    case cmd do 
                        "acc" -> test_loop(program, idx + offset, acc + aug, MapSet.put(set, idx))
                        "jmp" -> case locate_loop(program, idx + 1, acc, MapSet.put(set, idx)) do
                                {:loop, _acc} -> test_loop(program, idx + offset, acc + aug, MapSet.put(set, idx))
                                solution -> solution
                            end
                        "nop" -> case locate_loop(program, idx + aug, acc, MapSet.put(set, idx)) do
                                {:loop, _acc} -> test_loop(program, idx + offset, acc + aug, MapSet.put(set, idx))
                                solution -> solution
                            end
                    end
            end
        else
            {:no_loop, acc}
        end
    end
end

input_path = Path.dirname(__ENV__.file) <> "/input.txt"

input_path
|> Input.to_array
|> Handheld.locate_loop
|> IO.inspect

input_path
|> Input.to_array
|> Handheld.test_loop
|> IO.inspect