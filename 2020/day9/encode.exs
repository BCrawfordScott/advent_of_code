Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Encode do
    def two_sum(ints, target) do
        Enum.reduce_while(ints, %{}, fn int, acc -> 
            case Map.get(acc, int) do
                nil -> {:cont, Map.put(acc, target - int, int)}
                val -> if not (val == int), do: {:halt, [int, val]}, else: {:cont, acc}
            end
        end)
    end

    def has_two_sum?(preamble, target) do
        case two_sum(preamble, target) do
            [_v1, _v2] -> true
            _map -> false
        end
    end

    def find_error(input, p_length \\ 25)
    def find_error(input, p_length) do
            preamble = Enum.take(input, p_length)
            target = Enum.at(input, p_length)

            case has_two_sum?(preamble, target) do
                true -> [_head | rest] = input; find_error(rest, p_length)
                false -> target
            end
    end

    def target_sub_sum([head | tail], target) do
        Enum.reduce_while(tail, head, fn int, acc -> 
            new_acc = acc + int
    
            if new_acc == target do
                {:halt, [head, int]}
            else
                if new_acc > target do
                    {:halt, target_sub_sum(tail, target)}
                else
                    {:cont, new_acc}
                end
            end
        end)
    end

    def find_range(input, [v1, v2]) do
        si = Enum.find_index(input, fn el -> el == v1 end)
        ei = Enum.find_index(input, fn el -> el == v2 end)

        Enum.slice(input, si..ei)
    end

    def calc_range(input, indices) do
        range = find_range(input, indices)

        Enum.min(range) + Enum.max(range)
    end

    def calc_error_range(input), do: calc_error_range(input, find_error(input))
    def calc_error_range(input, target) do
        input
        |> calc_range(target_sub_sum(input, target))
    end
end

input_path = Path.dirname(__ENV__.file) <> "/input.txt"

input_path
|> Input.to_ints
|> Encode.find_error
|> IO.inspect

input_path
|> Input.to_ints
|> Encode.calc_error_range
|> IO.puts