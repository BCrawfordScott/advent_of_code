Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule ConwayCube do
    @neighbor_deltas [
        {1, 1, 1, 1}, {1, 1, 1, -1}, {1, 1, 1, 0},
        {1, 1, -1, 1}, {1, 1, -1, -1}, {1, 1, -1, 0},
        {1, 1, 0, 1}, {1, 1, 0, -1}, {1, 1, 0, 0},
        {1, 0, 1, 1}, {1, 0, 1, -1}, {1, 0, 1, 0},
        {1, 0, -1, 1}, {1, 0, -1, -1}, {1, 0, -1, 0},
        {1, 0, 0, 1}, {1, 0, 0, -1}, {1, 0, 0, 0}, 
        {0, 1, 1, 1}, {0, 1, 1, -1}, {0, 1, 1, 0}, 
        {0, 1, -1, 1}, {0, 1, -1, -1}, {0, 1, -1, 0}, 
        {0, 1, 0, 1}, {0, 1, 0, -1}, {0, 1, 0, 0}, 
        {-1, 0, 1, 1}, {-1, 0, 1, -1}, {-1, 0, 1, 0},
        {-1, 0, -1, 1}, {-1, 0, -1, -1}, {-1, 0, -1, 0},
        {-1, 0, 0, 1}, {-1, 0, 0, -1}, {-1, 0, 0, 0},
        {0, -1, 1, 1}, {0, -1, 1, -1}, {0, -1, 1, 0},
        {0, -1, -1, 1}, {0, -1, -1, -1}, {0, -1, -1, 0},
        {0, -1, 0, 1}, {0, -1, 0, -1}, {0, -1, 0, 0},
        {-1, 1, 1, 1}, {-1, 1, 1, -1}, {-1, 1, 1, 0},
        {-1, 1, -1, 1}, {-1, 1, -1, -1}, {-1, 1, -1, 0},
        {-1, 1, 0, 1}, {-1, 1, 0, -1}, {-1, 1, 0, 0},
        {1, -1, 1, 1}, {1, -1, 1, -1}, {1, -1, 1, 0},
        {1, -1, -1, 1}, {1, -1, -1, -1}, {1, -1, -1, 0},
        {1, -1, 0, 1}, {1, -1, 0, -1}, {1, -1, 0, 0},
        {-1, -1, 1, 1}, {-1, -1, 1, -1}, {-1, -1, 1, 0},
        {-1, -1, -1, 1}, {-1, -1, -1, -1}, {-1, -1, -1, 0},
        {-1, -1, 0, 1}, {-1, -1, 0, -1}, {-1, -1, 0, 0},
        {0, 0, 1, 1}, {0, 0, 1, -1}, {0, 0, 1, 0},
        {0, 0, -1, 1}, {0, 0, -1, -1}, {0, 0, -1, 0},
        {0, 0, 0, 1}, {0, 0, 0, -1}
    ]

    def simulate_cycles(input_path, cycles) do
        initial_cubes(input_path)
        |> cycle(cycles)
        |> length
        |>IO.inspect
    end

    def cycle(cubes, 0), do: cubes
    def cycle(cubes, cycle) do
        Enum.reduce(cubes, %{}, fn cube, acc -> record_neighbors(cube, acc) end)
        |> determine_active(cubes)
        |> cycle(cycle - 1)
    end

    def determine_active(neighbor_counts, prev_active) do
        still_active = Enum.reject(prev_active, fn cube -> neighbor_counts[cube] < 2 or neighbor_counts[cube] > 3 end)

        newly_active = Enum.reduce(still_active, Map.keys(neighbor_counts), fn cube, acc -> List.delete(acc, cube) end)
        |> Enum.reject(fn cube -> neighbor_counts[cube] != 3 end)
         
        still_active ++ newly_active
    end

    def record_neighbors({x, y, z, w}, record) do
        Enum.reduce(@neighbor_deltas, record, fn {dx, dy, dz, dw}, acc -> 
            neighbor = {x + dx, y + dy, z + dz, w + dw}
            Map.put(acc, neighbor, Map.get(acc, neighbor, 0) + 1)
        end)
    end

    def initial_cubes(input_path) do
        input = Input.to_array(input_path) # Read the input and split on "\n"
        width = length(String.split(List.first(input), "", trim: true))
        cube_list = String.split(Enum.join(input, ""), "", trim: true)

        List.foldr(Enum.with_index(cube_list), [], fn {char, idx}, acc -> 
            case char do
                "#" -> [{div(idx, width), rem(idx, width), 0, 0} | acc ]
                _period -> acc
            end
        end)
    end
end

test_start = Time.utc_now()
test_path = Path.dirname(__ENV__.file) <> "/test.txt"
ConwayCube.simulate_cycles(test_path, 6)
Time.diff( test_start, Time.utc_now, :microsecond)
|> IO.inspect

input_start = Time.utc_now()
input_path = Path.dirname(__ENV__.file) <> "/input.txt"
ConwayCube.simulate_cycles(input_path, 6)    
Time.diff(Time.utc_now, input_start,  :microsecond)
|> IO.inspect