require 'byebug'

input = File.readlines("./input.txt").map { |line| line.to_i }.sort
tester = File.readlines("./test.txt").map { |line| line.to_i }.sort

def solve(input)
    prepped_input = [0] + input.dup << input.last + 3
    # puts prepped_input
    numways(prepped_input)
end

def numways(input, idx=0, memo=Hash.new)
    return memo[idx] if memo[idx]
    return 1 if idx == input.length - 1

    total = 0
    # debugger
    if input[idx + 1] && input[idx + 1] - input[idx] <= 3
        total += numways(input, idx + 1, memo) 
    end
    if input[idx + 2] && input[idx + 2] - input[idx] <= 3
        total += numways(input, idx + 2, memo) 
    end
    if input[idx + 3] && input[idx + 3] - input[idx] <= 3
        total += numways(input, idx + 3, memo) 
    end
    # puts total
    memo[idx] = total
    memo[idx]
end

puts solve(tester)