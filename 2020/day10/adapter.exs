Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Adapter do

   def sort(input, func \\ &(&1 < &2))
   def sort(input, func) do
      Enum.sort(input, func)
   end

   def count_diffs(list) do
      first = List.first(list)
      count_diffs(list, %{first => first})
   end

   def count_diffs([_head | []], counts), do: Map.put(counts, 3, Map.get(counts, 3, 0) + 1)
   def count_diffs([head | tail], counts) do
      diff = List.first(tail) - head
      val = Map.get(counts, diff, 0)

      count_diffs(tail, Map.put(counts, diff, val + 1))
   end



   def calc_diffs(map) do
      Map.get(map, 1, 0) * Map.get(map, 3, 0)
   end

   def jolt_diffs(input) do
      input
      |> sort
      |> count_diffs 
      |> calc_diffs
   end

   def combinations(list), do: combinations([0 | list], 0, %{})
   def combinations(list, idx, memo) do
      mem = Map.get(memo, idx)
      if mem do
         {mem, memo}
      else
         if idx == (length(list) - 1) do
            {1, Map.put(memo, idx, 1)}
         else
            [first, second, third, fourth] = [
               Enum.at(list, idx), 
               Enum.at(list, idx + 1), 
               Enum.at(list, idx + 2), 
               Enum.at(list, idx + 3)
            ]

            {total1, memo1} = if second != nil and second - first <= 3, do: combinations(list, idx + 1, memo), else: {0, %{}}
            {total2, memo2} = if third != nil and third - first <= 3, do: combinations(list, idx + 2, memo1), else: {0, %{}}
            {total3, memo3} = if fourth != nil and fourth - first <= 3, do: combinations(list, idx + 3, memo2), else: {0, %{}}

            total = total1 + total2 + total3
            Map.put(memo3, idx, total)  

            {total, Map.merge(memo1, Map.merge(memo2, memo3))}
         end   
      end
   end
   # def combinations([_ult], idx, memo), do: {1, Map.put(memo, idx, 1)}
   # def combinations([pen, ult], idx, memo) do 
   #    saved = Map.get(memo, idx)
   #    if  saved do 
   #       {saved, memo}
   #    else
   #       IO.inspect([pen, ult])
   #       total = 0

   #       if ult - pen <= 3 do
   #          {calc, memo} = combinations([ult], idx + 1, memo)
   #          # memo = Map.merge(memo, n_memo)
   #          total = total + calc

   #          memo = Map.put(memo, idx, total)
   #          {memo[idx], memo}
   #       else
   #          memo = Map.put(memo, idx, total)
   #          {memo[idx], memo}
   #       end
   #    end
   # end
   # def combinations([ant, pen, ult], idx, memo) do
   #    saved = Map.get(memo, idx)
   #    if  saved do 
   #       {saved, memo}
   #    else
   #       IO.inspect([ant, pen, ult])
   #       total = 0
         
   #       if pen - ant <= 3 do
   #          {calc, memo} = combinations([pen, ult], idx + 1, memo)
   #          # memo = Map.merge(memo, n_memo)
   #          total = total + calc

   #          if ult - ant <= 3 do
   #             {calc, memo} = combinations([ult], idx + 2, memo)
   #             # memo = Map.merge(memo, n_memo)
   #             total = total + calc

   #             memo = Map.put(memo, idx, total)
   #             {memo[idx], memo}
   #          else
   #             memo = Map.put(memo, idx, total)
   #             {memo[idx], memo}
   #          end
   #       else
   #          memo = Map.put(memo, idx, total)
   #          {memo[idx], memo}
   #       end
   #    end
   # end

   # # [1, 4, 5, 6, 7, 10, 11, 12, 15, 16, 19]

   # def combinations(list, idx, memo) do
   #    # IO.inspect(list)
   #    saved = Map.get(memo, idx)
   #    if  saved do 
   #       {saved, memo}
   #    else
   #       [first, second, third, fourth] = Enum.take(list, 4)
   #       IO.inspect([first, second, third, fourth])
   #       total = 0
         
   #       if second - first <= 3 do
   #          {calc, memo} = combinations(Enum.drop(list, 1), idx + 1, memo)
   #          # memo = Map.merge(memo, n_memo)
   #          total = total + calc

   #          if third - first <= 3 do
   #             {calc, memo} = combinations(Enum.drop(list, 2), idx + 2, memo)
   #             # memo = Map.merge(memo, n_memo)
   #             total = total + calc

   #             if fourth - first <= 3 do
   #                {calc, memo} = combinations(Enum.drop(list, 3), idx + 3, memo)
   #                # memo = Map.merge(memo, n_memo)
   #                total = total + calc

   #                memo = Map.put(memo, idx, total)
   #                {memo[idx], memo}
   #             else
   #                memo = Map.put(memo, idx, total)
   #                {memo[idx], memo}
   #             end
   #          else
   #             memo = Map.put(memo, idx, total)
   #             {memo[idx], memo}
   #          end
   #       else
   #          memo = Map.put(memo, idx, total)
   #          {memo[idx], memo}
   #       end
   #    end
   # end 

   # def diff([first, second, third]) do
   #    [second - first, third - second]
   # end

   def calc_combinations(input) do
   input
   |> sort #(&(&1 >= &2))
   # |> to_diffs
   |> combinations
   end

   def to_diffs([_last]), do: [3]
   def to_diffs([head | tail]) do
      # IO.inspect(tail)
      [Enum.at(tail, 0) - head | to_diffs(tail) ]
   end




end

input_path = Path.dirname(__ENV__.file) <> "/input.txt"
test_path = Path.dirname(__ENV__.file) <> "/test.txt"

input_path
|> Input.to_ints
# |> Adapter.sort
# |> Adapter.to_diffs
# # |> IO.inspect
|> Adapter.jolt_diffs
|> IO.inspect

input_path
|> Input.to_ints
|> Adapter.sort
# |> Adapter.to_diffs
# |> IO.inspect
|> Adapter.calc_combinations
|> IO.inspect

# [3, 1, 1, 1, 3, 1, 1, 3, 1, 3, 3]