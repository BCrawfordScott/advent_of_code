Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Recite do

    def find_num_on(input_path, target) do
        Input.read(input_path) # Read the input and return it as a single string
        |> String.split(",", trim: true)
        |> Enum.map(&(String.to_integer(&1)))
        |> Recite.play(target)
        |> IO.inspect
    end
    
    def play(starter, target) do
        history = Enum.reduce(Enum.with_index(starter), %{}, fn {num, idx}, acc -> 
            Map.put(acc, num, idx + 1)
        end)
  
        Enum.reduce((length(starter) + 1)..target, {0, history}, fn 
            ^target, {num, _history} -> num
            turn, {curr_num, history} -> 
                case Map.get(history, curr_num) do
                    nil -> {0, Map.put(history, curr_num, turn)}
                    last_turn -> {turn - last_turn, Map.put(history, curr_num, turn)}
                end
        end) 
    end
end

test_path = Path.dirname(__ENV__.file) <> "/test.txt"
Recite.find_num_on(test_path, 2020)
input_path = Path.dirname(__ENV__.file) <> "/input.txt"
Recite.find_num_on(input_path, 30000000)