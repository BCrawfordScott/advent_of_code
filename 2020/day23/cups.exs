Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Cups do 
    def play(input_path, million \\ false)
    def play(input_path, million) do
        cups = parse_input(input_path)
        current_cup = List.first(cups)
        if (million) do 
            cup_map = map_input_1000000(cups) 

            finished = move(cup_map, current_cup, 10000000, million)
            first = Map.get(finished, 1)
            second = Map.get(finished, first)
            
            IO.inspect(first * second)
        else 
            cup_map = map_input(cups)

            move(cup_map, current_cup, 100, million)
            |> join_from_1 # produces a string of all the numbers after "1" followed by "1"
            |> IO.puts
        end
    end

    def move(cup_map, _current_cup, 0, _million), do: cup_map
    def move(cup_map, current_cup, round, million) do
        first = Map.get(cup_map, current_cup)
        second = Map.get(cup_map, first)
        third = Map.get(cup_map, second)
        rest = Map.get(cup_map, third)
        
        temp_cup_map = Map.put(cup_map, current_cup, rest)

        max_offset = if million, do: 1000000, else: 9
        new_head = Enum.reduce_while((max_offset - 1)..1, current_cup, fn offset, acc ->
            test = rem(offset + acc, max_offset)
            key = if test == 0, do: max_offset, else: test
            if Enum.member?([first, second, third], key), do: {:cont, current_cup}, else: {:halt, key}
        end)
        new_tail = Map.get(temp_cup_map, new_head)
        new_cup_map = Map.merge(temp_cup_map, %{new_head => first, third => new_tail})

        move(new_cup_map, Map.get(new_cup_map, current_cup), round - 1, million)
    end

    def parse_input(input_path) do
        {:ok, input} = File.read(input_path)
        String.split(input, "", trim: true)
        |> Enum.map(fn num -> String.to_integer(num) end)
    end

    def map_input_1000000([first | tail]), do: map_input(tail ++ Enum.to_list(10..1000000), Map.put(%{}, first, List.first(tail)), first)
    def map_input([first | tail]), do: map_input(tail, Map.put(%{}, first, List.first(tail)), first)
    def map_input([head | []], map, first), do: Map.put(map, head, first)
    def map_input([head | tail], map, first), do: map_input(tail, Map.put(map, head, List.first(tail)), first)

    def join_from_1(map, str \\ "", key \\ :start)
    def join_from_1(_map, str, 1), do: str
    def join_from_1(map, str, key) do
        case key do
            :start -> val = Map.get(map, 1); join_from_1(map, str <> Integer.to_string(val), val)
            ckey -> val = Map.get(map, ckey); join_from_1(map, str <> Integer.to_string(val), val)
        end
    end
end

# test_path = Path.dirname(__ENV__.file) <> "/test.txt"
# Cups.play(test_path)
# Cups.play(test_path, true)
 
input_path = Path.dirname(__ENV__.file) <> "/input.txt"
# Cups.play(input_path)
start = Time.utc_now()
Cups.play(input_path, true)
IO.inspect(Time.diff(Time.utc_now(),start))