Application.put_env(:elixir, :ansi_enabled, true)
Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Seating do

    @neighbor_deltas [
        {1, 1}, 
        {1, 0}, 
        {0, 1}, 
        {-1, 0},
        {0, -1},
        {-1, 1},
        {1, -1},
        {-1, -1}
    ]

    def equilibrium(input_path) do
        input = Input.to_array(input_path)

        width = length(String.split(List.first(input), "", trim: true))
        
        seat_list = String.split(Enum.join(input, ""), "", trim: true)

        seat_map = Enum.reduce(Enum.with_index(seat_list), %{}, fn 
            {char, idx}, acc -> Map.put(acc, {div(idx, width), rem(idx, width)}, char) 
        end)

        # render(seat_map, width)
        evolve(seat_map, width)

    end
    
    def evolve(seat_map, length) do
        range = 0..length(Map.keys(seat_map)) -  1
        evolved_map = Enum.reduce(range, %{}, fn idx, acc -> 
            char = evolve_seat(seat_map, length, idx)
            Map.put(acc, {div(idx, length), rem(idx, length)}, char)
        end)
        
        render(evolved_map, length)
        if Map.equal?(seat_map, evolved_map), do: evolved_map, else: evolve(evolved_map, length)
    end

    def find_neighbors(map, length, idx) do
        start_key = {div(idx, length), rem(idx, length)}

        @neighbor_deltas
        |> Enum.map(fn delta -> 
            get_valid_neighbor(map, start_key, delta)
        end)

        # [
        #     Map.get(map, {x - 1, y - 1}, nil),
        #     Map.get(map, {x - 1, y}, nil),
        #     Map.get(map, {x - 1, y + 1}, nil),
        #     Map.get(map, {x, y - 1}, nil),
        #     Map.get(map, {x, y + 1}, nil),
        #     Map.get(map, {x + 1, y - 1}, nil),
        #     Map.get(map, {x + 1, y }, nil),
        #     Map.get(map, {x + 1, y + 1}, nil)
        # ]
    end

    def get_valid_neighbor(map, key, delta) do
        {x, y} = key
        {dx, dy} = delta
        search = {x + dx, y + dy}

        case Map.get(map, search) do
            "." -> get_valid_neighbor(map, search, delta)
            seat -> seat
        end
    end

    def evolve_seat(map, length, idx) do
        seat = Map.get(map, {div(idx, length), rem(idx, length)})
        neighbors = find_neighbors(map, length, idx)
        # if idx == 9, do: IO.inspect(neighbors)
        cond do
            seat == "." -> "."
            seat == "#" and Enum.count(neighbors, fn char -> char == "#" end) >= 5 -> "L"
            seat == "L" and Enum.all?(neighbors, fn char -> char != "#"  end) -> "#"
            true -> seat
        end
    end

    def count_seats(seat_map) do
        Enum.count(Map.values(seat_map), &(&1 == "#"))
    end

    def render(map, length) do
        IO.puts("")
        Map.to_list(map)
        |> Enum.sort(fn el1, el2 -> 
            {{x1, y1}, _val1} = el1
            {{x2, y2}, _val2} = el2

            (x1 * length + y1) < (x2 * length) + y2
        end)
        |> Enum.map(fn {_idx, val} -> val end)
        |> Enum.chunk_every(length)
        |> Enum.each(&(IO.puts(Enum.join(&1, ""))))

        Process.sleep(350)
        IO.ANSI.clear()
    end

    def solve1(input_path) do
        equilibrium(input_path)
        |> count_seats
        |> IO.inspect
    end
end

test_path = Path.dirname(__ENV__.file) <> "/test.txt"
Seating.solve1(test_path)
input_path = Path.dirname(__ENV__.file) <> "/input.txt"
Seating.solve1(input_path)
