Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Operation do  
    def solve_homework(input_path) do
        organize_input(input_path)
        |> Enum.map(fn line -> process(line) end)
        |> List.foldl(0, fn {calc, _i}, acc -> calc + acc end)
        |> IO.inspect
    end
    
    def organize_input(input_path) do
        Input.to_array(input_path) # Given a file path, parse the text file and split on "\n"
        |> Enum.map(fn string -> 
            String.split(String.replace(string, " ", ""), "", trim: true)
        end)
    end

    def process(line, acc \\ {:none, 0}, operator \\ :none)
    def process(line, {acc, idx}, _operator) when idx >= length(line), do: {acc, idx}
    def process(line, {acc, idx}, operator) do
        case Enum.at(line, idx) do
            "+" -> process(line, {acc, idx + 1}, "+")
            "*" -> {sub_acc, idx} = process(line, {:none, idx + 1}, :none); {acc * sub_acc, idx}
            ")" -> {acc, idx + 1}
            "(" -> {sub_acc, next_idx} = process(line, {:none, idx + 1}, :none)
                case operator do
                    "+" -> process(line, {acc + sub_acc, next_idx}, operator)
                    "*" -> process(line,{acc * sub_acc, next_idx}, operator)
                    :none -> process(line, {sub_acc, next_idx}, :none)
                end
            val -> case operator do
                    "+" -> process(line, {acc + String.to_integer(val), idx + 1}, operator)
                    "*" -> {sub_acc, idx} = process(line, {:none, idx + 1}, operator); {acc * sub_acc, idx}
                    :none -> process(line,{ String.to_integer(Enum.at(line, idx)), idx + 1}, :none)
                end
        end
    end
end

test_path = Path.dirname(__ENV__.file) <> "/test.txt"
Operation.solve_homework(test_path)

input_path = Path.dirname(__ENV__.file) <> "/input.txt"
Operation.solve_homework(input_path)
