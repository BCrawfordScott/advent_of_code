Code.require_file("../../input.ex","#{__ENV__.file}")

defmodule Combo do
    def break(input_path) do
        [card_pk, door_pk] = Enum.map(Input.to_array(input_path), &(String.to_integer(&1))) #Split the input on "\n" and map to integers
        transform_key(door_pk, find_loop(card_pk))
        |> IO.inspect
    end

    def find_loop(key_target, loop_size \\ 0, current_val \\ 1)
    def find_loop(key_target, loop_size, current_val) when current_val == key_target, do: loop_size
    def find_loop(key_target, loop_size, current_val), do: find_loop(key_target, loop_size + 1, transform(current_val, 7))

    def transform_key(subject_num, loop_size, current_val \\ 1)
    def transform_key(_subject_num, 0, current_val), do: current_val
    def transform_key(subject_num, loop_size, current_val), do: transform_key(subject_num, loop_size - 1, transform(current_val, subject_num))
    
    def transform(val, subject), do: rem(val * subject, 20201227)
end


test_path = Path.dirname(__ENV__.file) <> "/test.txt"
Combo.break(test_path)
input_path = Path.dirname(__ENV__.file) <> "/input.txt"
Combo.break(input_path)