"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isNumber = void 0;
function isNumber(c) {
    return !!c.match(/\d/);
}
exports.isNumber = isNumber;
