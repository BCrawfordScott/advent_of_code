"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const part1 = async () => {
    const input = await (0, helpers_1.toArray)("day4.txt", "\n");
    // const input = await toArray("day4test.txt", "\n");
    return input.reduce((acc, curr, idx) => {
        return acc + calcCardValue(curr);
    }, 0);
};
exports.part1 = part1;
const part2 = async () => { };
exports.part2 = part2;
function calcCardValue(card) {
    const [nums, winningNums] = card.slice(8, card.length).split("|").map(n => n.trim());
    const winningSet = new Set(cleanNums(winningNums));
    const numsSet = new Set(cleanNums(nums));
    const matches = setIntersection(numsSet, winningSet).size;
    if (matches <= 1)
        return matches;
    return Math.pow(2, matches - 1);
}
function cleanNums(nums) {
    return nums.replaceAll("  ", " ").split(" ").map(n => parseInt(n));
}
function setIntersection(set1, set2) {
    const result = new Set();
    for (let i of set1) {
        if (set2.has(i))
            result.add(i);
    }
    return result;
}
