"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const part1 = async () => {
    const input = await (0, helpers_1.toArray)("day3.txt", "\n");
    return input.reduce((acc, curr, idx) => {
        const numMatches = Array.from(curr.matchAll(/\d+/g));
        const lineVal = numMatches.reduce((acc, curr) => {
            const num = curr[0];
            const numStart = curr.index;
            let touchesSymbol = false;
            if (numStart !== undefined) {
                for (let j = idx - 1; j <= idx + 1; j++) {
                    for (let i = numStart - 1; i <= numStart + num.length; i++) {
                        if (!touchesSymbol) {
                            if (input[j]?.[i]?.match(/[^A-Za-z0-9_\.]/))
                                touchesSymbol = true;
                        }
                    }
                }
            }
            return touchesSymbol ? acc + parseInt(num) : acc;
        }, 0);
        return acc + lineVal;
    }, 0);
};
exports.part1 = part1;
const part2 = async () => {
    const input = await (0, helpers_1.toArray)("day3.txt", "\n");
    const gearMap = new Map();
    input.forEach((curr, idx) => {
        const numMatches = Array.from(curr.matchAll(/\d+/g));
        numMatches.reduce((acc, curr) => {
            const num = curr[0];
            const numStart = curr.index;
            if (numStart !== undefined) {
                for (let j = idx - 1; j <= idx + 1; j++) {
                    for (let i = numStart - 1; i <= numStart + num.length; i++) {
                        if (input[j]?.[i]?.match(/\*/)) {
                            const key = (0, helpers_1.processCoor)([j, i]);
                            if (acc.has(key)) {
                                acc.get(key)?.add(num);
                            }
                            else {
                                acc.set(key, new Set([num]));
                            }
                        }
                    }
                }
            }
            return acc;
        }, gearMap);
    });
    let acc = 0;
    gearMap.forEach((curr) => {
        if (curr.size > 1) {
            const prod = (0, helpers_1.product)(Array.from(curr).map((num) => parseInt(num)));
            acc = acc + prod;
        }
    }, 0);
    return acc;
};
exports.part2 = part2;
