"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Day25 = exports.Day24 = exports.Day23 = exports.Day22 = exports.Day21 = exports.Day20 = exports.Day19 = exports.Day18 = exports.Day17 = exports.Day16 = exports.Day15 = exports.Day14 = exports.Day13 = exports.Day12 = exports.Day11 = exports.Day10 = exports.Day9 = exports.Day8 = exports.Day7 = exports.Day6 = exports.Day5 = exports.Day4 = exports.Day3 = exports.Day2 = exports.Day1 = exports.DEFAULT = void 0;
exports.DEFAULT = 'Hello 2023';
exports.Day1 = __importStar(require("./day1"));
exports.Day2 = __importStar(require("./day2"));
exports.Day3 = __importStar(require("./day3"));
exports.Day4 = __importStar(require("./day4"));
exports.Day5 = __importStar(require("./day5"));
exports.Day6 = __importStar(require("./day6"));
exports.Day7 = __importStar(require("./day7"));
exports.Day8 = __importStar(require("./day8"));
exports.Day9 = __importStar(require("./day9"));
exports.Day10 = __importStar(require("./day10"));
exports.Day11 = __importStar(require("./day11"));
exports.Day12 = __importStar(require("./day12"));
exports.Day13 = __importStar(require("./day13"));
exports.Day14 = __importStar(require("./day14"));
exports.Day15 = __importStar(require("./day15"));
exports.Day16 = __importStar(require("./day16"));
exports.Day17 = __importStar(require("./day17"));
exports.Day18 = __importStar(require("./day18"));
exports.Day19 = __importStar(require("./day19"));
exports.Day20 = __importStar(require("./day20"));
exports.Day21 = __importStar(require("./day21"));
exports.Day22 = __importStar(require("./day22"));
exports.Day23 = __importStar(require("./day23"));
exports.Day24 = __importStar(require("./day24"));
exports.Day25 = __importStar(require("./day25"));
