"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.part2 = exports.part1 = void 0;
const helpers_1 = require("../helpers");
const part1 = async () => {
    const input = await (0, helpers_1.toArray)('day2.txt', "\n");
    return input.reduce((acc, curr, idx) => {
        return isValid(toMaxes(curr)) ? acc + (idx + 1) : acc;
    }, 0);
};
exports.part1 = part1;
const part2 = async () => {
    const input = await (0, helpers_1.toArray)('day2.txt', "\n");
    return input.reduce((acc, curr) => {
        return acc + toPower(toMaxes(curr));
    }, 0);
};
exports.part2 = part2;
function toMaxes(line) {
    const result = { green: 0, red: 0, blue: 0 };
    const greens = Array.from(line.matchAll(/(?<num>\d+) green/g)).map((arr) => parseInt(arr.groups?.num || "0"));
    const blues = Array.from(line.matchAll(/(?<num>\d+) blue/g)).map((arr) => parseInt(arr.groups?.num || "0"));
    const reds = Array.from(line.matchAll(/(?<num>\d+) red/g)).map((arr) => parseInt(arr.groups?.num || "0"));
    result.green = (0, helpers_1.max)(greens);
    result.blue = (0, helpers_1.max)(blues);
    result.red = (0, helpers_1.max)(reds);
    return result;
}
function isValid(maxes) {
    return maxes.red <= 12 && maxes.green <= 13 && maxes.blue <= 14;
}
function toPower(gameSet) {
    return (0, helpers_1.product)(Object.values(gameSet));
}
