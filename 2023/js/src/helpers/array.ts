export const min = (array: number[]): number => array.reduce((least: number, current: number) => current < least ? current : least);

export const max = (array: number[]): number => array.reduce((least: number, current: number) => current > least ? current : least);

export const sum = (array: number[]): number => array.reduce((acc: number, curr: number) => acc + curr, 0);

export const product = (array: number[]): number => (array.length === 0) ? 0 : array.reduce((acc: number, curr: number) => acc * curr);
