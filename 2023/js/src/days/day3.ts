import { processCoor, product, toArray } from "../helpers";

const part1 = async (): Promise<number> => {
  const input = await toArray("day3.txt", "\n");
  return input.reduce((acc: number, curr: string, idx: number) => {
    const numMatches = Array.from(curr.matchAll(/\d+/g));
    const lineVal = numMatches.reduce((acc: number, curr: RegExpMatchArray) => {
      const num = curr[0];
      const numStart = curr.index;
      let touchesSymbol: boolean = false;
      if (numStart !== undefined) {
        for (let j = idx - 1; j <= idx + 1; j++) {
          for (let i = numStart - 1; i <= numStart + num.length; i++) {
            if (!touchesSymbol) {
              if (input[j]?.[i]?.match(/[^A-Za-z0-9_\.]/)) touchesSymbol = true;
            }
          }
        }
      }
      return touchesSymbol ? acc + parseInt(num) : acc;
    }, 0)
    return acc + lineVal;
  }, 0);
};

const part2 = async (): Promise<number> => {
  const input = await toArray("day3.txt", "\n");
  const gearMap = new Map<string, Set<string>>();
  input.forEach((curr: string, idx: number) => {
    const numMatches = Array.from(curr.matchAll(/\d+/g));
    numMatches.reduce((acc: Map<string, Set<string>>, curr: RegExpMatchArray) => {
      const num = curr[0];
      const numStart = curr.index;
      if (numStart !== undefined) {
        for (let j = idx - 1; j <= idx + 1; j++) {
          for (let i = numStart - 1; i <= numStart + num.length; i++) {
            if (input[j]?.[i]?.match(/\*/)) {
              const key = processCoor([j, i]);
              if (acc.has(key)) {
                acc.get(key)?.add(num)
              } else {
                acc.set(key, new Set([num]));
              }
            }
          }
        }
      }
      return acc;
    }, gearMap)
  });

  let acc = 0;
  gearMap.forEach((curr: Set<string>) => {
    if (curr.size > 1) {
      const prod = product(Array.from(curr).map((num) => parseInt(num)))
      acc = acc + prod;
    }
  }, 0)

  return acc;
};

export {
  part1,
  part2,
};