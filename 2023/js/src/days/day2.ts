import { max, product, toArray } from "../helpers";

type GameSet = Record<"green" | "blue" | "red", number>

const part1 = async (): Promise<number> => {
  const input = await toArray('day2.txt', "\n");

  return input.reduce((acc: number, curr: string, idx: number) => {
    return isValid(toMaxes(curr)) ? acc + (idx + 1) : acc;
  }, 0);
};

const part2 = async (): Promise<number> => {
  const input = await toArray('day2.txt', "\n");

  return input.reduce((acc: number, curr: string) => {
    return acc + toPower(toMaxes(curr))
  }, 0);
};

function toMaxes(line: string): GameSet {
  const result: GameSet = { green: 0, red: 0, blue: 0 };
  const greens = Array.from(line.matchAll(/(?<num>\d+) green/g)).map((arr) => parseInt(arr.groups?.num || "0"))
  const blues = Array.from(line.matchAll(/(?<num>\d+) blue/g)).map((arr) => parseInt(arr.groups?.num || "0"))
  const reds = Array.from(line.matchAll(/(?<num>\d+) red/g)).map((arr) => parseInt(arr.groups?.num || "0"))

  result.green = max(greens);
  result.blue = max(blues);
  result.red = max(reds);

  return result;
}

function isValid(maxes: GameSet): boolean {
  return maxes.red <= 12 && maxes.green <= 13 && maxes.blue <= 14;
}

function toPower(gameSet: GameSet): number {
  return product(Object.values(gameSet));
}

export {
  part1,
  part2,
};