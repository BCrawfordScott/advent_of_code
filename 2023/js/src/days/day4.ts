import { toArray } from "../helpers";

const part1 = async (): Promise<number> => {
  const input = await toArray("day4.txt", "\n");
  return input.reduce((acc: number, curr: string, idx: number) => {
    return acc + calcCardValue(curr)
  }, 0);
};

const part2 = async (): Promise<void> => {
  const input = await toArray("day4.txt", "\n");
  const totals = new Array<number>(input.length).fill(1);
};

function calcCardValue(card: string): number {
  const [nums, winningNums] = card.slice(8, card.length).split("|").map(n => n.trim());
  const winningSet = new Set(cleanNums(winningNums));
  const numsSet = new Set(cleanNums(nums));
  const matches = setIntersection(numsSet, winningSet).size;

  if (matches <= 1) return matches;

  return Math.pow(2, matches - 1);
}

function cleanNums(nums: string): number[] {
  return nums.replaceAll("  ", " ").split(" ").map(n => parseInt(n))
}

function setIntersection(set1: Set<number>, set2: Set<number>): Set<number> {
  const result = new Set<number>();

  for (let i of set1) {
    if (set2.has(i)) result.add(i);
  }

  return result;
}

export {
  part1,
  part2,
};