import { toArray } from "../helpers";

const part1 = async (): Promise<number> => {
  const input: String[] = await toArray('day1.txt', '\n');
  return input.reduce((acc: number, line: String) => {
    return acc + findCalibrationNum(line);
  }, 0)
};

const part2 = async (): Promise<number> => {
  const input: String[] = await toArray('day1.txt', '\n');
  return input.reduce((acc: number, line: String) => {
    const num = findFullCalibrationNum(line);
    return acc + findFullCalibrationNum(line);
  }, 0)
};

function findCalibrationNum(line: String): number {
  let first: string|null = null;
  let i = 0;
  let last: string|null = null;
  let j = line.length - 1;

  while (first === null && i < line.length) {
    const c = line[i];
    if (isNumber(c)) first = c;
    ++i;
  }

  while (last === null && j >=0) {
    const c = line[j];
    if (isNumber(c)) last = c;
    --j;
  }

  return parseInt((first || '0') + (last || '0'));
}

function findFullCalibrationNum(line: String): number {
  let first: string|null = null;
  let i = 0;
  let last: string|null = null;
  let j = line.length - 1;

  while (first === null && i < line.length) {
    const c = line[i];
    if (isNumber(c)) {
      first = c;
    } else {
      const numWord = isNumberWord(line.slice(i, line.length), true)
      if (numWord) first = numMap(numWord)
    }
    ++i;
  }

  while (last === null && j >=0) {
    const c = line[j];
    if (isNumber(c)) {
      last = c;
    } else {
      const numWord = isNumberWord(line.slice(0, j + 1), false)
      if (numWord) last = numMap(numWord)
    }
    --j;
  }

  return parseInt((first || '0') + (last || '0'));
}

function isNumber(c: String): boolean {
  return !!c.match(/\d/);
}

function isNumberWord(sub: String, beginning: boolean): string|undefined {
  const numPattern: String = "(one|two|three|four|five|six|seven|eight|nine)";
  const pattern: RegExp = beginning ? new RegExp("^" + numPattern) : new RegExp(numPattern + "$");

  return sub.match(pattern)?.[0];
} 

function numMap(strNum: string): string {
  switch (strNum) {
    case "one": return "1";
    case "two": return "2";
    case "three": return "3";
    case "four": return "4";
    case "five": return "5";
    case "six": return "6";
    case "seven": return "7";
    case "eight": return "8";
    case "nine": return "9";
    default: return "0";
  }
}

export {
  part1,
  part2,
};
