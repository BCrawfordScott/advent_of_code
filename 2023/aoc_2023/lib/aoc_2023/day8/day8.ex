defmodule Aoc2023.Day8 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> count_steps({"AAA", ~r/Z{3}/})
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> count_group_steps()
  end

  defp parse_input(path) do
    {rl_strings,  string_map} = path |> Input.to_array("\n\n") |> List.to_tuple()

    rls = rl_strings |> String.split("", trim: true)
    graph = Regex.scan(~r/(\w{3})\s=\s\((\w{3}), (\w{3})\)/, string_map)
            |> Stream.map(&List.to_tuple/1)
            |> Enum.reduce(%{}, fn {_, node, l, r}, acc ->
              Map.put(acc, node, {l, r})
            end)

    {rls, graph}
  end

  defp count_steps(instructions, {start, target}) do
    step(start, target, 0, instructions)
  end

  defp step(curr, target, steps, {rl, graph}) do
    if Regex.match?(target, curr) do
      steps
    else
      {l, r} = Map.get(graph, curr)

      case Enum.at(rl, rem(steps, length(rl))) do
        "L" -> step(l, target, steps + 1, {rl, graph})
        "R" -> step(r, target, steps + 1, {rl, graph})
      end
    end
  end

  defp count_group_steps({rl, graph}) do
    start_nodes = graph
    |> Map.keys()
    |> Enum.filter(&String.match?(&1, ~r/\w{2}A/))

    start_nodes
    |> Enum.map(&count_steps({rl, graph}, {&1, ~r/\w{2}Z/}))
    |> Enum.reduce(fn step_count, acc -> Math.lcm(acc, step_count) end)
  end
end
