defmodule Aoc2023.Day1 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> get_total_calibration
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> get_accurate_total_calibration()
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp get_total_calibration(values) do
    values
    |> Enum.reduce(0, fn value, acc ->
      get_calibration(value) + acc
    end)
  end

  defp get_calibration(value), do: get_calibration(String.split(value, ""), false, false)
  defp get_calibration([], first, last) do
    String.to_integer(first <> last)
  end
  defp get_calibration([val | rest], false, false) do
    case Regex.match?(~r/\d/, val) do
      true -> get_calibration(rest, val, val)
      false -> get_calibration(rest, false, false)
    end
  end
  defp get_calibration([val | rest], first, any) do
    case Regex.match?(~r/\d/, val) do
      true -> get_calibration(rest, first, val)
      false -> get_calibration(rest, first, any)
    end
  end

  defp get_accurate_total_calibration(values) do
    values
    |> Enum.reduce(0, fn value, acc ->
      get_accurate_calibration(value) + acc
    end)
  end

  defp get_accurate_calibration(value), do: get_accurate_calibration(String.split(value, ""), false, false)
  defp get_accurate_calibration([], first, last), do: String.to_integer(first <> last)
  defp get_accurate_calibration([a | [b | [c | [d | [e | tail]]]]], false, false) do
    cond do
      Regex.match?(~r/\d/, a) -> get_accurate_calibration([b | [c | [d | [e | tail]]]], a, a)
      a <> b <> c <> d <> e == "seven" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], "7", "7")
      a <> b <> c <> d <> e == "eight" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], "8", "8")
      a <> b <> c <> d <> e == "three" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], "3", "3")
      a <> b <> c <> d == "four" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], "4", "4")
      a <> b <> c <> d == "five" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], "5", "5")
      a <> b <> c <> d == "nine" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], "9", "9")
      a <> b <> c == "one" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], "1", "1")
      a <> b <> c == "two" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], "2", "2")
      a <> b <> c == "six" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], "6", "6")
      true -> get_accurate_calibration([b | [c | [d | [e | tail]]]], false, false)
    end
  end
  defp get_accurate_calibration([a | [b | [c | [d | tail]]]], false, false) do
    cond do
      Regex.match?(~r/\d/, a) -> get_accurate_calibration([b | [c | [d | tail]]], a, a)
      a <> b <> c <> d == "four" -> get_accurate_calibration([b | [c | [d | tail]]], "4", "4")
      a <> b <> c <> d == "five" -> get_accurate_calibration([b | [c | [d | tail]]], "5", "5")
      a <> b <> c <> d == "nine" -> get_accurate_calibration([b | [c | [d | tail]]], "9", "9")
      a <> b <> c == "one" -> get_accurate_calibration([b | [c | [d | tail]]], "1", "1")
      a <> b <> c == "two" -> get_accurate_calibration([b | [c | [d | tail]]], "2", "2")
      a <> b <> c == "six" -> get_accurate_calibration([b | [c | [d | tail]]], "6", "6")
      true -> get_accurate_calibration([b | [c | [d | tail]]], false, false)
    end
  end
  defp get_accurate_calibration([a | [b | [c | tail]]], false, false) do
    cond do
      Regex.match?(~r/\d/, a) -> get_accurate_calibration([b | [c | tail]], a, a)
      a <> b <> c == "one" -> get_accurate_calibration([b | [c | tail]], "1", "1")
      a <> b <> c == "two" -> get_accurate_calibration([b | [c | tail]], "2", "2")
      a <> b <> c == "six" -> get_accurate_calibration([b | [c | tail]], "6", "6")
      true -> get_accurate_calibration([b | [c | tail]], false, false)
    end
  end
  defp get_accurate_calibration([a | tail], false, false) do
    cond do
      Regex.match?(~r/\d/, a) -> get_accurate_calibration(tail, a, a)
      true -> get_accurate_calibration(tail, false, false)
    end
  end

  defp get_accurate_calibration([a | [b | [c | [d | [e | tail]]]]], first, any) do
    cond do
      Regex.match?(~r/\d/, a) -> get_accurate_calibration([b | [c | [d | [e | tail]]]], first, a)
      a <> b <> c <> d <> e == "seven" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], first, "7")
      a <> b <> c <> d <> e == "eight" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], first, "8")
      a <> b <> c <> d <> e == "three" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], first, "3")
      a <> b <> c <> d == "four" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], first, "4")
      a <> b <> c <> d == "five" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], first, "5")
      a <> b <> c <> d == "nine" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], first, "9")
      a <> b <> c == "one" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], first, "1")
      a <> b <> c == "two" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], first, "2")
      a <> b <> c == "six" -> get_accurate_calibration([b | [c | [d | [e | tail]]]], first, "6")
      true -> get_accurate_calibration([b | [c | [d | [e | tail]]]], first, any)
    end
  end
  defp get_accurate_calibration([a | [b | [c | [d | tail]]]], first, any) do
    cond do
      Regex.match?(~r/\d/, a) -> get_accurate_calibration([b | [c | [d | tail]]], first, a)
      a <> b <> c <> d == "four" -> get_accurate_calibration([b | [c | [d | tail]]], first, "4")
      a <> b <> c <> d == "five" -> get_accurate_calibration([b | [c | [d | tail]]], first, "5")
      a <> b <> c <> d == "nine" -> get_accurate_calibration([b | [c | [d | tail]]], first, "9")
      a <> b <> c == "one" -> get_accurate_calibration([b | [c | [d | tail]]], first, "1")
      a <> b <> c == "two" -> get_accurate_calibration([b | [c | [d | tail]]], first, "2")
      a <> b <> c == "six" -> get_accurate_calibration([b | [c | [d | tail]]], first, "6")
      true -> get_accurate_calibration([b | [c | [d | tail]]], first, any)
    end
  end
  defp get_accurate_calibration([a | [b | [c | tail]]], first, any) do
    cond do
      Regex.match?(~r/\d/, a) -> get_accurate_calibration([b | [c | tail]], first, a)
      a <> b <> c == "one" -> get_accurate_calibration([b | [c | tail]], first, "1")
      a <> b <> c == "two" -> get_accurate_calibration([b | [c | tail]], first, "2")
      a <> b <> c == "six" -> get_accurate_calibration([b | [c | tail]], first, "6")
      true -> get_accurate_calibration([b | [c | tail]], first, any)
    end
  end
  defp get_accurate_calibration([a | tail], first, any) do
    cond do
      Regex.match?(~r/\d/, a) -> get_accurate_calibration(tail, first, a)
      true -> get_accurate_calibration(tail, first, any)
    end
  end
end
