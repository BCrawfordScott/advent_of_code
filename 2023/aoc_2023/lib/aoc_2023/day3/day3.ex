defmodule Aoc2023.Day3 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  @start_deltas [
    {-1, -1},
    {0, -1},
    {1, -1}
  ]
  @end_deltas [
    {-1, 1},
    {0, 1},
    {1, 1}
  ]
  @standard_deltas [
    {-1, 0},
    {1, 0}
  ]

  def part1, do: part1(@input_file)
  def part1(path) do
    input = path |> parse_input()
    map = build_map(input)
    sum_parts(input, map)
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    input = path |> parse_input()
    map = build_map(input)
    sum_gear_ratios(input, map)
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp sum_parts(input, num_map) do
    input
    |> Enum.with_index()
    |> Enum.reduce(0, fn {row, x}, acc1 ->
      Regex.scan(~r/[^\.\w]/, row, return: :index)
      |> Enum.map(&List.first/1)
      |> Enum.reduce(acc1, fn {y, _len}, acc2 ->
        val = Map.get(num_map, {x, y}, []) |> Enum.sum()
        acc2 + val
      end)
    end)
  end

  defp sum_gear_ratios(input, num_map) do
    input
    |> Enum.with_index()
    |> Enum.reduce(0, fn {row, x}, acc1 ->
      Regex.scan(~r/\*/, row, return: :index)
      |> Enum.map(&List.first/1)
      |> Enum.reduce(acc1, fn {y, _len}, acc2 ->
        val = Map.get(num_map, {x, y}, [])
        case length(val) do
          2 -> acc2 + Enum.product(val)
          _len -> acc2
        end
      end)
    end)
  end

  defp build_map(input) do
    input
    |> Enum.with_index()
    |> Enum.reduce(Map.new, fn {row, x}, acc1 ->
      Regex.scan(~r/\d+/, row, return: :index)
      |> Enum.map(&List.first/1)
      |> Enum.reduce(acc1, fn {start_idx, len}, acc2 ->
        end_idx = start_idx + len - 1
        range = start_idx..end_idx
        num = String.slice(row, range) |> String.to_integer()
        border_pos = start_diffs({x, start_idx}) ++ end_diffs({x, end_idx}) ++ Enum.flat_map(range, fn y -> standard_diffs({x, y}) end)
        border_pos |> Enum.reduce(acc2, fn
          pos, acc3 -> Map.put(acc3, pos, [num | Map.get(acc3, pos, [])])
        end)
      end)
    end)
  end

  defp start_diffs(idxs) do
    @start_deltas
    |> Enum.map(fn sd -> apply_delta(sd, idxs) end)
  end

  defp end_diffs(idxs) do
    @end_deltas
    |> Enum.map(fn ed -> apply_delta(ed, idxs) end)
  end

  defp standard_diffs(idxs) do
    @standard_deltas
    |> Enum.map(fn sd -> apply_delta(sd, idxs) end)
  end

  defp apply_delta({dx, dy}, {sx, sy}), do: {sx + dx, sy + dy}
end
