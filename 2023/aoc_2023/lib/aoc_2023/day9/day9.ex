defmodule Aoc2023.Day9 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> Stream.map(&String.split(&1, " ", trim: true))
    |> Stream.map(fn arr -> Enum.map(arr, &String.to_integer/1) end)
    |> Stream.map(&find_next_value/1)
    |> Enum.sum()
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> Stream.map(&String.split(&1, " ", trim: true))
    |> Stream.map(fn arr -> Enum.map(arr, &String.to_integer/1) end)
    |> Stream.map(&find_prev_value/1)
    |> Enum.sum()
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp find_next_value(int_arr) do
    if Enum.all?(int_arr, fn int -> int == 0 end) do
      0
    else
      {diff_sequence, _} = int_arr |> List.foldr({[], nil}, fn
        int, {acc, nil} -> {acc, int}
        int, {acc, prev} -> {[prev - int | acc], int}
      end)
      List.last(int_arr) + find_next_value(diff_sequence)
    end
  end

  defp find_prev_value(int_arr) do
    if Enum.all?(int_arr, fn int -> int == 0 end) do
      0
    else
      {diff_sequence, _} = int_arr |> List.foldl({[], nil}, fn
        int, {acc, nil} -> {acc, int}
        int, {acc, prev} -> {[int - prev | acc], int}
      end)
      List.first(int_arr) - find_next_value(diff_sequence)
    end
  end
end
