defmodule Aoc2023.Day10 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  @delta_map %{
    # { d_that_got_me_here, char } => next_d
    {{-1, 0}, "|"} => {-1, 0},
    {{+1, 0}, "|"} => {+1, 0},
    {{0, -1}, "-"} => {0, -1},
    {{0, +1}, "-"} => {0, +1},
    {{0, -1}, "L"} => {-1, 0},
    {{+1, 0}, "L"} => {0, +1},
    {{0, +1}, "J"} => {-1, 0},
    {{+1, 0}, "J"} => {0, -1},
    {{0, +1}, "7"} => {+1, 0},
    {{-1, 0}, "7"} => {0, -1},
    {{0, -1}, "F"} => {+1, 0},
    {{-1, 0}, "F"} => {0, +1},
  }

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> build_coor_map()
    |> chart_route()
    |> length()
    |> div(2)
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    input_rows = path |> parse_input()
    loop_tiles =  input_rows |> build_coor_map() |> chart_route() |> MapSet.new()
    count_interior_space(input_rows, loop_tiles)
  end

  defp count_interior_space(input_rows, loop_tiles) do
    input_rows
    |> Stream.with_index()
    |> Enum.reduce(0, fn {row, x}, count1 ->
      {row_count, _int } = row
      |> String.split("", trim: true)
      |> Stream.with_index()
      |> Enum.reduce({count1, false}, fn {char, y}, {count2, int} ->
        on_loop = MapSet.member?(loop_tiles, {x, y})
        cond do
          on_loop && char == "|" -> {count2, !int}
          on_loop && char == "S" -> {count2, !int}
          on_loop && char == "F" -> {count2, !int}
          on_loop && char == "7" -> {count2, !int}
          !on_loop && int -> {count2 + 1, int}
          true -> {count2, int}
        end
      end)

      row_count
    end)
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp build_coor_map(input_lines) do
    input_lines
    |> Stream.map(&String.split(&1, "", trim: true))
    |> Stream.with_index
    |> Enum.reduce({%{}, nil}, fn {line, x}, {map1, start1} ->
      line
      |> Enum.with_index()
      |> List.foldr({map1, start1}, fn {char, y}, {map2, start2} ->
        next_map = Map.put(map2, {x, y}, char)
        if(char == "S", do: {next_map, {x, y}}, else: {next_map, start2})
      end)
    end)
  end

  defp chart_route({pipe_map, {sx, sy}}) do
    {dx, dy} = get_valid_start(pipe_map, {sx, sy})
    x = sx + dx
    y = sy + dy
    build_route({x, y}, [{x, y}, {sx, sy}], {dx, dy}, pipe_map)
  end

  defp build_route({x, y}, route, prev_delta, pipe_map) do
    char = Map.get(pipe_map, {x, y})
    if char == "S" do
       route
    else
      case Map.get(@delta_map, {prev_delta, char}, :error) do
        :error -> raise "Womp womp"
        {dx, dy} -> build_route({x + dx, y + dy}, [{x + dx, y + dy} | route], {dx, dy}, pipe_map)
      end
    end
  end

  defp get_valid_start(pipe_map, {sx, sy}) do
    cond do
      Regex.match?(~r/\||F|7/, Map.get(pipe_map, {sx - 1, sy})) -> {-1, 0}
      Regex.match?(~r/\||L|J/, Map.get(pipe_map, {sx + 1, sy})) -> {1, 0}
      Regex.match?(~r/-|F|L/, Map.get(pipe_map, {sx, sy - 1})) -> {0, -1}
      Regex.match?(~r/-|J|7/, Map.get(pipe_map, {sx, sy + 1})) -> {0, 1}
    end
  end
end
