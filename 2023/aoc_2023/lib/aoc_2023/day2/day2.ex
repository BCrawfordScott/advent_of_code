defmodule Aoc2023.Day2 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> Enum.reduce(0, fn game, acc ->
      ["Game " <> g_num | [ grabs | []]] = String.split(game, ":", trim: true)
      case possible?(grabs) do
        true -> acc + String.to_integer(g_num)
        false -> acc
      end
    end)
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> Enum.reduce(0, fn game, acc ->
      [_game_num | [ grabs | []]] = String.split(game, ":", trim: true)
      acc + power(grabs)
    end)
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp possible?(grabs) do
    {max_red, max_green, max_blue} = color_maxes(grabs)

    max_red <= 12 && max_green <= 13 && max_blue <= 14
  end

  defp power(grabs) do
    {max_red, max_green, max_blue} = color_maxes(grabs)

    max_red * max_green * max_blue
  end

  defp color_maxes(grabs) do
    [:red, :green, :blue]
    |> Enum.map( fn color ->
      Regex.scan(~r/(\d*)\s#{color}/, grabs) |> Stream.map(&List.last/1) |> Stream.map(&String.to_integer/1) |> Enum.max()
    end)
    |> List.to_tuple()
  end
end
