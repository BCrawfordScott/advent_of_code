defmodule Aoc2023.Day5 do
  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  # def part1, do: part1(@test_file)
  def part1(path) do
    path
    |> parse_input()
    |> process_inputs()
    |> map_seeds()
    |> Enum.min()
  end

  def part2, do: part2(@input_file)
  # def part2, do: part2(@test_file)
  def part2(path) do
    path
    |> parse_input()
    |> process_inputs(true)
    |> set_seed_ranges()
    |> map_seeds()
    |> Enum.flat_map(fn ranges -> ranges end)
    |> IO.inspect()
    |> Enum.map(fn tuple -> Tuple.to_list(tuple) |> Enum.min() end)
    |> Enum.min()
  end

  defp parse_input(path) do
    path
    |> Input.to_array("\n\n")
  end

  defp process_inputs(input_groups, part_2 \\ false) do
    [seeds_line | map_lines] = input_groups
    seed_nums = seeds_line |> String.replace("seeds: ", "") |> String.split(" ", trim: true) |> Enum.map(&String.to_integer/1)
    map_functions = if part_2 do
      map_lines |> Enum.map(&to_range_function_tuple/1)
    else
      map_lines |> Enum.map(&to_function_tuple/1)
    end

    {seed_nums, map_functions}
  end

  defp set_seed_ranges({seed_nums, map_functions}) do
    seed_ranges = seed_nums
      |> Enum.chunk_every(2)
      |> Enum.map(&List.to_tuple/1)
      |> Enum.map(fn {s, e} -> {s, s+e-1} end)

    {seed_ranges, map_functions}
  end

  defp to_function_tuple(map_group) do
    [map_name | maps] = map_group |> String.split("\n", trim: true)
    map_funcs = maps |> Enum.map(&to_map_function/1)

    {map_name, map_funcs}
  end

  defp to_map_function(map) do
    map |> String.split(" ") |> Enum.map(&String.to_integer/1) |> List.to_tuple() |> build_func()
  end

  defp build_func({a, b, c}) do
    fn
      val when b <= val and val < (b + c) -> {:halt, val + (a - b)}
      val  -> {:cont, val}
    end
  end

  defp to_range_function_tuple(map_group) do
    [map_name | maps] = map_group |> String.split("\n", trim: true)
    map_funcs = maps |> Enum.map(&to_range_function/1) |> Enum.sort()

    {map_name, map_funcs}
  end

  defp to_range_function(map) do
    map |> String.split(" ") |> Enum.map(&String.to_integer/1) |> List.to_tuple() |> build_range_func_tuple()
  end

  defp build_range_func_tuple({a, b, c}) do
    {
      {b, (b+c)},
      fn
        {rs, re} when re < b or rs >= (b + c) -> {nil, {rs, re}}
        {rs, re} when rs < b -> {{re, (b - 1)}, {b, re}}
        {rs, re} when re < (b + c) -> {{rs + (a - b), re + (a - b)}, nil}
        {rs, re} -> {{rs + (a - b), (b + c - 1) + (a - b)}, {b + c, re}}
      end
    }
  end

  defp map_seeds({seeds, maps}) do
    seeds |> Enum.map(fn
      seed when is_integer(seed) -> map_seed(seed, maps)
      seed -> map_seed_range([seed], maps)
    end)
  end

  defp map_seed(seed, maps) do
    maps
    |> Enum.reduce(seed, fn {_func_name, map}, acc ->
      map |> Enum.reduce_while(acc, fn map_func, acc2 -> map_func.(acc2) end)
    end)
  end

  defp map_seed_range(seed_ranges, maps) do
    maps
    |> IO.inspect(limit: :infinity)
    |> Enum.reduce(seed_ranges, fn {_map_nam, map_list}, curr_ranges ->
      curr_ranges
      |> Enum.flat_map(fn seed_range ->
        {mapped_ranges, final } = map_list |> Enum.reduce_while({[], seed_range}, fn {_range, map_func}, {ranges, curr_range} ->
          case map_func.(curr_range) do
            {nil, next_range} -> {:cont, {ranges, next_range}}
            {last_range, nil} -> {:halt, {[last_range | ranges], nil}}
            {cap_range, next_range} -> {:cont, {[cap_range | ranges], next_range}}
          end
        end)
        if final do
          [final | mapped_ranges]
        else
          mapped_ranges
        end
      end)
    end)
  end
end
