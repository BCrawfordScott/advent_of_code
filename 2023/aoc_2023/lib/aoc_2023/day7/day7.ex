defmodule Aoc2023.Day7 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"
  # @test2_file Path.dirname(__ENV__.file) <> "/test2.txt"

  @card_values %{
    "2" => 1,
    "3" => 2,
    "4" => 3,
    "5" => 4,
    "6" => 5,
    "7" => 6,
    "8" => 7,
    "9" => 8,
    "T" => 9,
    "J" => 10,
    "Q" => 11,
    "K" => 12,
    "A" => 13,
  }

  @card_values_with_jokers %{
    "J" => 1,
    "2" => 2,
    "3" => 3,
    "4" => 4,
    "5" => 5,
    "6" => 6,
    "7" => 7,
    "8" => 8,
    "9" => 9,
    "T" => 10,
    "Q" => 11,
    "K" => 12,
    "A" => 13,
  }

  def part1, do: part1(@input_file)
  # def part1, do: part1(@test_file)
  def part1(path) do
    path
    |> parse_input()
    |> Enum.sort(&by_rank(&1, &2, false, false))
    |> Stream.map(fn hand_bet ->
      {hand, bet} = String.split(hand_bet, " ", trim: true) |> List.to_tuple()
      {hand, String.to_integer(bet)}
    end)
    |> Stream.with_index(1)
    |> Stream.map(fn {{_hand, bet}, rank} -> bet * rank end)
    |> Enum.sum()
  end

  def part2, do: part2(@input_file)
  # def part2, do: part2(@test_file)
  def part2(path) do
    path
    |> parse_input()
    |> Enum.sort(&by_rank(&1, &2, true, true))
    |> Stream.map(fn hand_bet ->
      {hand, bet} = String.split(hand_bet, " ", trim: true) |> List.to_tuple()
      {hand, String.to_integer(bet)}
    end)
    |> Stream.with_index(1)
    |> Stream.map(fn {{_hand, bet}, rank} -> bet * rank end)
    |> Enum.sum()
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp by_rank(hand1, hand2, part2, joker_rules) do
    rank1 = get_rank(hand1, part2)
    rank2 = get_rank(hand2, part2)
    cond do
      rank1 > rank2 -> false
      rank2 > rank1 -> true
      rank1 == rank2 -> by_sub_rules(hand1, hand2, joker_rules)
    end
  end

  defp get_rank(hand, part2) do
    case count_cards(hand, part2) do
      {{ 5, _ }} -> 20
      {{ 4, _ }, _ } -> 19
      {{ 3, _ }, {2, _ }} -> 18
      {{ 3, _ }, _ , _ } -> 17
      {{ 2, _ }, { 2, _ }, _ } -> 16
      {{ 2, _ }, _ , _, _ } -> 15
      _ -> 14
    end
  end

  defp count_cards(hand, part2) do
    hand
    |> jokerfy(part2)
    |> String.split("", trim: true)
    |> Enum.take(5)
    |> Enum.reduce(%{}, fn card, acc ->
      Map.put(acc, card, Map.get(acc, card, 0) + 1)
    end)
    |> Map.to_list()
    |> Enum.map(fn {a, b} -> {b, a} end)
    |> Enum.sort(:desc)
    |> List.to_tuple()
  end

  defp jokerfy(hand, false), do: hand
  defp jokerfy(hand, true) do
    case Regex.match?(~r/J/, hand) do
      false -> hand
      true -> get_best_joker_hand(hand)
    end
  end

  defp get_best_joker_hand(hand) do
    uniq_chars = hand |> String.replace("J", "") |> String.split("", trim: true) |> Enum.uniq()
    uniq_chars
    |> Enum.reduce(hand, fn char, best_hand ->
      joker_hand = String.replace(hand, "J", char)
      case by_rank(joker_hand, best_hand, false, true) do
        true -> best_hand
        false -> joker_hand
      end
    end)
  end

  defp to_card_value(card, joker), do: if(joker, do: Map.get(@card_values_with_jokers, card), else: Map.get(@card_values, card))

  defp by_sub_rules([], [], _joker), do: true
  defp by_sub_rules([c1 | t1], [c2 | t2], joker) do
    val1 = to_card_value(c1, joker)
    val2 = to_card_value(c2, joker)
    cond do
      val1 > val2 -> false
      val1 < val2 -> true
      val1 == val2 -> by_sub_rules(t1, t2, joker)
    end
  end
  defp by_sub_rules(hand1, hand2, joker) do
    cards1 = hand1 |> String.split("", trim: true)
    cards2 = hand2 |> String.split("", trim: true)

    by_sub_rules(cards1, cards2, joker)
  end
end
