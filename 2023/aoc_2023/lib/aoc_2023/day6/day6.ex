defmodule Aoc2023.Day6 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  require Integer

  def part1, do: part1(@input_file)
  # def part1, do: part1(@test_file)
  def part1(path) do
    path
    |> parse_input()
    |> List.to_tuple()
    |> to_record_map()
    |> Enum.map(&to_possible_win_routes/1)
    |> Enum.product()
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> List.to_tuple()
    |> to_unkerned_map()
    |> to_possible_win_routes()
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp to_record_map({times_string, records_string}) do
    times = times_string |> String.replace("Time:", "") |> String.split(" ", trim: true) |> Enum.map(&String.to_integer/1)
    records = records_string |> String.replace("Distance:", "") |> String.split(" ", trim: true) |> Enum.map(&String.to_integer/1)

    Enum.zip(times, records)
  end

  defp to_unkerned_map({times_string, records_string}) do
    time = times_string |> String.replace("Time:", "") |> String.replace(" ", "") |> String.to_integer()
    record = records_string |> String.replace("Distance:", "") |> String.replace(" ", "") |> String.to_integer()

    {time, record}
  end

  defp to_possible_win_routes({time, record}) do
    extra = if(Integer.is_odd(time), do: 0, else: 1)
    possibilities = 2 * (1..(div(time, 2))
    |> Enum.reduce(0, fn hold, winners ->
      if hold * (time - hold) > record do
        winners + 1
      else
        winners
      end
    end))

    if(possibilities == 0, do: 0, else: possibilities - extra)
  end
end
