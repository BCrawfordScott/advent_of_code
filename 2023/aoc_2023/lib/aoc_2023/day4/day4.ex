defmodule Aoc2023.Day4 do

  @input_file Path.dirname(__ENV__.file) <> "/input.txt"
  # @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  def part1, do: part1(@input_file)
  def part1(path) do
    path
    |> parse_input()
    |> Enum.reduce(Map.new, &to_card_match_map/2)
    |> Map.values()
    |> Enum.map(&to_points/1)
    |> Enum.sum()
  end

  def part2, do: part2(@input_file)
  def part2(path) do
    path
    |> parse_input()
    |> Enum.reduce(Map.new, &to_card_match_map/2)
    |> get_total_scratchers()
  end

  defp parse_input(path) do
    path
    |> Input.to_array()
  end

  defp to_card_match_map(card, point_map) do
    ["Card" <> card_num | [numbers |[]]] = String.split(card, ":", trim: true)
    Map.put(point_map, String.trim_leading(card_num) |> String.to_integer, to_matches(numbers))
  end

  defp to_points(matches) do
    case matches do
      0 -> 0
      1 -> 1
      count -> 1..(count - 1) |> Enum.reduce(1, fn _c, acc -> acc * 2 end)
    end
  end

  defp to_matches(numbers), do: MapSet.size(match_numbers(numbers))

  defp match_numbers(numbers) do
    { target_nums, scratch_nums } = String.split(numbers, " | ", trim: true)
    |> Stream.map(fn str_num -> String.split(str_num, " ", trim: true) end)
    |> Stream.map(&MapSet.new/1)
    |> Enum.to_list()
    |> List.to_tuple()

    MapSet.intersection(target_nums, scratch_nums)
  end

  defp get_total_scratchers(match_map) do
    match_map
    |> Map.keys()
    |> Enum.sort()
    |> Enum.reduce(Map.new, fn key, acc1 ->
      next_acc = Map.put_new(acc1, key, 1)
      num_matches = Map.get(match_map, key)
      case num_matches do
        0 -> next_acc
        num -> add_winnings(num, key, next_acc)
      end
    end)
    |> Map.values()
    |> Enum.sum()
  end

  defp add_winnings(num, key, acc) do
    current_key_ticks = Map.get(acc, key)
    (key + 1)..(key + num) |> Enum.reduce(acc, fn n_key, acc2 ->
      Map.put(acc2, n_key, Map.get(acc2, n_key, 1) + current_key_ticks)
    end)
  end
end
