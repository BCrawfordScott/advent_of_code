defmodule Input do
  def read(filename) do
    {:ok, input} = File.read(filename)
    input
   end

  def to_array(filename, delimiter \\ "\n") do
    filename
    |> read
    |> String.replace("\r", "")
    |> String.split(delimiter, trim: true)
  end
end
