defmodule Aoc2023.Day1Test do
  use ExUnit.Case
  doctest Aoc2023.Day1

  import Aoc2023.Day1, only: [
    part1: 1,
    part2: 1,
  ]

  @test_file Path.dirname(__ENV__.file) <> "/test.txt"

  test "has an input file" do
    assert(File.exists?(File.cwd! <> "/lib/aoc_2023/day1/input.txt"))
  end

  test "part 1 delivers the right output" do
    assert(part1(@test_file) == 297)
  end

  test "part 2 delivers the right output" do
    assert(part2(@test_file) == 281)
  end
end
