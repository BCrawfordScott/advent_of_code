const Input = require('../input');

// for each instruction
// move across a list of deltas in an array. Adjust by 1 or -1 based on L or right
// apply the number to the delta and apply to the position

const DELTAS = [
    [0, 1],
    [1, 0],
    [0, -1],
    [-1, 0]
]

function directions(input) {
    return Input.toArray(input, ", ");
}

function calcIdx(turn, idx) {
    if (turn === "L") {
        return (idx + DELTAS.length - 1) % DELTAS.length;
    } else {
        return (idx + DELTAS.length + 1) % DELTAS.length;
    }
}

function manhattan(pos1, pos2) {
    const [x1, y1] = pos1;
    const [x2, y2] = pos2;

    return Math.abs(x1 + x2) + Math.abs(y1 + y2);
}

function applyDirections(dirs) {
    let idx = 0;
    return dirs.reduce((acc, dir) => {
        const [turn, count] = dir.split("");
        idx = calcIdx(turn, idx);
        
        const [dx, dy] = DELTAS[idx];
        const [px, py] = acc;

        return [(dx * parseInt(count)) + px, (dy * parseInt(count)) + py]
    }, [0, 0])
}

function findDistance(input) {
    const dirs = directions(input);
    const final = applyDirections(dirs);

    return manhattan([0, 0], final);
}

console.log(findDistance('./test.txt'))
console.log(findDistance('./input.txt'))