const fs = require('fs');

function read(path) {
    return fs.readFileSync(path, 'utf8');
}

function toArray(path, splitter) {
    return read(path).split(splitter);
}

module.exports = {
    toArray,
}